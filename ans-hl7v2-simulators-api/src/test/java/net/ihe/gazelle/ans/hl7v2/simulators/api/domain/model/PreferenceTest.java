package net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 24/06/2023
 */
class PreferenceTest {

    private static Preference preference;
    @BeforeAll
    static void init(){
        preference = Preference.builder()
                .withId(1)
                .withPreferenceName("casEnabled")
                .withPreferenceValue("true")
                .withDescription("Enabled Cas SSO")
                .withClassName("java.lang.boolean")
                .build();
    }
    @Test
    void shouldGetAttr_OK() {
        assertNotNull(preference);
    }

    @Test
    void simpleEqualsContract() {
        EqualsVerifier
                .simple()
                .forClass(Preference.class)
                .withIgnoredAnnotations()
                .verify();
    }
    @Test
    void testToString() {
        String expected = "Preference{id=1, className='java.lang.boolean', description='Enabled Cas SSO', preferenceName='casEnabled', preferenceValue='true'}";
        assertEquals(expected, preference.toString());
    }

    @Test
    void getter_OK() {
        assertEquals("java.lang.boolean", preference.getClassName());
        assertEquals("casEnabled", preference.getPreferenceName());
        assertEquals(1, preference.getId());
        assertEquals("true", preference.getPreferenceValue());
        assertEquals("Enabled Cas SSO", preference.getDescription());
    }
}