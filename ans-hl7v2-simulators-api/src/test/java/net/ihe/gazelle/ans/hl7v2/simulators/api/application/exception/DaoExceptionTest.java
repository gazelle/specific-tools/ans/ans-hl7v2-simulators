package net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception;

import org.junit.jupiter.api.Test;

import java.io.Serial;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 24/06/2023
 */
class DaoExceptionTest extends DaoException {

    @Serial
    private static final long serialVersionUID = 5214875355448867936L;
    private static final String MESSAGE = "Test Exception";
    private final Throwable throwable = new Throwable(MESSAGE);

    @Test
    void testTipExceptionWithMessage() {
        DaoException daoException = new DaoException(MESSAGE);
        assertEquals(MESSAGE, daoException.getMessage());
    }

    @Test
    void testTipExceptionWithThrowable() {
        DaoException daoException = new DaoException(throwable);
        assertEquals(daoException.getCause(), throwable);
    }

    @Test
    void testTipExceptionSuper() {
        DaoException daoException = new DaoException();
        assertNull(daoException.getMessage());
    }

    @Test
    void testTipExceptionWithMessageAndThrowable() {
        DaoException daoException = new DaoException(MESSAGE, throwable);
        assertEquals(daoException.getCause(), throwable);
        assertEquals(MESSAGE, daoException.getMessage());
    }
}