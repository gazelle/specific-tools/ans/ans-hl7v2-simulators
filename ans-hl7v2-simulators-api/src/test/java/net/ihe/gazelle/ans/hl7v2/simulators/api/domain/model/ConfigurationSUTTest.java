package net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 24/06/2023
 */
class ConfigurationSUTTest {

    private static ConfigurationSUT configurationSUT;
    @BeforeAll
    static void init(){
        configurationSUT = ConfigurationSUT.builder()
                .withId(1)
                .withName("myConf")
                .withHost("myHost")
                .withPort("8080")
                .withApplication("myApp")
                .withFacility("myFacility")
                .withOwner("me")
                .withCompanyOwner("myCompany")
                .withShare(true)
                .withCharset("UTF-8")

                .build();
    }
    @Test
    void shouldGetAttr_OK() {
        assertNotNull(configurationSUT);
    }

    @Test
    void simpleEqualsContract() {
        EqualsVerifier.simple().forClass(ConfigurationSUT.class).verify();
    }

    @Test
    void testToString() {
        String expected = "ConfigurationSUT{id=1, name='myConf', host='myHost', port='8080', applicationSupplier='myApp', facilitySupplier='myFacility', owner='me', companyOwner='myCompany', share='true', charset='UTF-8'}";
        assertEquals(expected, configurationSUT.toString());
    }

    @Test
    void getter_OK() {
        assertEquals("myHost", configurationSUT.getHost());
        assertEquals("myFacility", configurationSUT.getFacility());
        assertEquals(1, configurationSUT.getId());
        assertEquals("myApp", configurationSUT.getApplication());
        assertEquals("myConf", configurationSUT.getName());
        assertEquals("8080", configurationSUT.getPort());
        assertEquals("me", configurationSUT.getOwner());
        assertEquals("myCompany", configurationSUT.getCompanyOwner());
        assertEquals(true, configurationSUT.isShare());
        assertEquals("UTF-8", configurationSUT.getCharset());

    }

}