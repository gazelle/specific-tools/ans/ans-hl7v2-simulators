package net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 19/07/2023
 */
class TemplateHl7v2Test {


    private static TemplateHl7v2 template;

    @BeforeAll
    static void init(){
        template = TemplateHl7v2.builder()
                .withTitle("Titre template")
                .withDescription("Description")
                .withEnabled(false)
                .withFileName("/path/to/file")
                .withTemplateMustache(null)
                .build();
    }

    @Test
    void shouldGetAttr_OK() {
        assertNotNull(template);
    }

    @Test
    void simpleEqualsContract() {
        EqualsVerifier.simple().forClass(TemplateHl7v2.class).withIgnoredFields("description").verify();
    }

    @Test
    void testToString() {





        String expected = "TemplateHl7v2{id=0, title='Titre template', enable=false, filePath='/path/to/file', templateMustache=null}";
        assertEquals(expected, template.toString());
    }

    @Test
    void getter_OK() {
        assertEquals("Description", template.getDescription());
        assertEquals("Titre template", template.getTitle());
        assertFalse(template.isEnable());
        assertEquals("/path/to/file", template.getFileName());
        assertNull(template.getTemplateMustache());
    }

}