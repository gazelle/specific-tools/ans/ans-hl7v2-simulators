package net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception;

import org.junit.jupiter.api.Test;

import java.io.Serial;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 19/07/2023
 */
class DuplicationExceptionTest extends DuplicationException {

    @Serial
    private static final long serialVersionUID = -2286692977739218004L;
    private static final String MESSAGE = "Test Exception";
    private final Throwable throwable = new Throwable(MESSAGE);

    @Test
    void testTipExceptionWithMessage() {
        DuplicationException duplicationException = new DuplicationException(MESSAGE);
        assertEquals(MESSAGE, duplicationException.getMessage());
    }

    @Test
    void testTipExceptionWithThrowable() {
        DuplicationException duplicationException = new DuplicationException(throwable);
        assertEquals(duplicationException.getCause(), throwable);
    }

    @Test
    void testTipExceptionSuper() {
        DuplicationException duplicationException = new DuplicationException();
        assertNull(duplicationException.getMessage());
    }

    @Test
    void testTipExceptionWithMessageAndThrowable() {
        DuplicationException duplicationException = new DuplicationException(MESSAGE, throwable);
        assertEquals(duplicationException.getCause(), throwable);
        assertEquals(MESSAGE, duplicationException.getMessage());
    }
}