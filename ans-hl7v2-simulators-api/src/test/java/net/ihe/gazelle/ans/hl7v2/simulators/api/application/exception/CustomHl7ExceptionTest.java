package net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception;

import org.junit.jupiter.api.Test;

import java.io.Serial;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 19/07/2023
 */
class CustomHl7ExceptionTest extends CustomHl7Exception{

    @Serial
    private static final long serialVersionUID = 8753794085359278128L;
    private static  final String MESSAGE = "Test Exception";
    private final Throwable throwable = new Throwable(MESSAGE);

    @Test
    void testTipExceptionWithMessage() {
        CustomHl7Exception customHl7Exception = new CustomHl7Exception(MESSAGE);
        assertEquals(MESSAGE, customHl7Exception.getMessage());
    }

    @Test
    void testTipExceptionWithThrowable() {
        CustomHl7Exception customHl7Exception = new CustomHl7Exception(throwable);
        assertEquals(customHl7Exception.getCause(), throwable);
    }

    @Test
    void testTipExceptionSuper() {
        CustomHl7Exception customHl7Exception = new CustomHl7Exception();
        assertNull(customHl7Exception.getMessage());
    }

    @Test
    void testTipExceptionWithMessageAndThrowable() {
        CustomHl7Exception customHl7Exception = new CustomHl7Exception(MESSAGE, throwable);
        assertEquals(customHl7Exception.getCause(), throwable);
        assertEquals(MESSAGE, customHl7Exception.getMessage());
    }
}