package net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception;

import org.junit.jupiter.api.Test;

import java.io.Serial;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 19/07/2023
 */
class NotFoundExceptionTest extends NotFoundException {

    @Serial
    private static final long serialVersionUID = -7596077460070431115L;
    private static final String MESSAGE = "Test Exception";
    private final Throwable throwable = new Throwable(MESSAGE);

    @Test
    void testTipExceptionWithMessage() {
        NotFoundException notFoundException = new NotFoundException(MESSAGE);
        assertEquals(MESSAGE, notFoundException.getMessage());
    }

    @Test
    void testTipExceptionWithThrowable() {
        NotFoundException notFoundException = new NotFoundException(throwable);
        assertEquals(notFoundException.getCause(), throwable);
    }

    @Test
    void testTipExceptionSuper() {
        NotFoundException notFoundException = new NotFoundException();
        assertNull(notFoundException.getMessage());
    }

    @Test
    void testTipExceptionWithMessageAndThrowable() {
        NotFoundException notFoundException = new NotFoundException(MESSAGE,throwable);
        assertEquals(notFoundException.getCause(), throwable);
        assertEquals(MESSAGE, notFoundException.getMessage());
    }
}