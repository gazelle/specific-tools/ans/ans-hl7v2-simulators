package net.ihe.gazelle.ans.hl7v2.simulators.api.application.service;

import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.Preference;

import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 23/06/2023
 */
public interface PreferenceService {
    List<Preference> getAllPreference();

    void insertPreference(Preference preference);

    Preference getOnePreference(int id);

    void updatePreference(Preference preference);

    void deletePreference(Preference preference);

    boolean isPreferenceByNameExist(String name);

    Preference getPreferenceByName(String name);

    public String getLocalCreatorPort() ;

    public String getLocalCreatorApplication() ;

    public String getLocalCreatorFacility() ;

    public String getLocalCreatorHost();

    String getLocalUTF8ManagerPort();

    String getLocalUTF8ManagerHost();

    String getLocalUTF8ManagerApplication();

    String getLocalUTF8ManagerFacility();


    String getLocalI885915ManagerPort();

    String getLocalI885915ManagerHost();

    String getLocalI885915ManagerApplication();

    String getLocalI885915ManagerFacility();
}
