package net.ihe.gazelle.ans.hl7v2.simulators.api.application.server;

import ca.uhn.hl7v2.model.Message;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.ConfigurationService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.TemplateHl7v2Service;
/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public interface ManagerServer {

     void startListenMessages(ConfigurationService configurationService, TemplateHl7v2Service templateHl7v2Service,CheckMessageService<Message> checkMessageService)  ;
    void stopListenMessages();
    void switchServerStateMessages(ConfigurationService configurationService, TemplateHl7v2Service templateHl7v2Service, CheckMessageService<Message> checkMessageService)  ;
    boolean isServerRunning();


}
