package net.ihe.gazelle.ans.hl7v2.simulators.api.application.dao;

import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.TemplateHl7v2;

import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 22/07/2023
 */
public interface TemplateDao {
    void insert(TemplateHl7v2 template);

    List<TemplateHl7v2> getAll();

    List<TemplateHl7v2> getEnables();

    TemplateHl7v2 getOne(int id);

    void update(TemplateHl7v2 templateHl7v2);

    boolean isTemplateTitleExist(String title);

    boolean isFileNameExist(String fileName);

    TemplateHl7v2 getByTitle(String title);

    TemplateHl7v2 getByFileName(String fileName);

    void delete(TemplateHl7v2 templateHl7v2);
}
