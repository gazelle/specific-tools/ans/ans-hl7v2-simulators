package net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public class ConfigurationSUT implements Serializable {
    @Serial
    private static final long serialVersionUID = -4740198650719652734L;
    private final int id;
    private final String name;
    private final String host;
    private final String port;
    private final String application;
    private final String facility;
    private final String owner;
    private final String companyOwner;
    private final boolean share;
    private final String charset;

    public ConfigurationSUT(ConfigurationSUTBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.host = builder.host;
        this.port = builder.port;
        this.application = builder.application;
        this.facility = builder.facility;
        this.owner = builder.owner;
        this.companyOwner = builder.companyOwner;
        this.share = builder.share;
        this.charset = builder.charset;
    }

    public static ConfigurationSUTBuilder builder() {
        return new ConfigurationSUTBuilder();
    }

    public int getId() {
        return id;
    }

    public String getHost() {
        return host;
    }

    public String getName() {
        return name;
    }

    public String getPort() {
        return port;
    }

    public String getApplication() {
        return application;
    }

    public String getFacility() {
        return facility;
    }

    public String getOwner() {
        return owner;
    }

    public String getCompanyOwner() {
        return companyOwner;
    }

    public Boolean isShare() {
        return share;
    }

    public String getCharset() {
        return charset;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConfigurationSUT that = (ConfigurationSUT) o;
        return id == that.id && Objects.equals(name, that.name) && Objects.equals(host, that.host) && Objects.equals(port, that.port) && Objects.equals(application, that.application) && Objects.equals(facility, that.facility) && Objects.equals(owner, that.owner) && Objects.equals(companyOwner, that.companyOwner) && Objects.equals(share, that.share) && Objects.equals(charset, that.charset);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, host, port, application, facility, owner, companyOwner, share, charset);
    }

    @Override
    public String toString() {
        return "ConfigurationSUT{" + "id=" + id +
                ", name='" + name + '\'' +
                ", host='" + host + '\'' +
                ", port='" + port + '\'' +
                ", applicationSupplier='" + application + '\'' +
                ", facilitySupplier='" + facility + '\'' +
                ", owner='" + owner + '\'' +
                ", companyOwner='" + companyOwner + '\'' +
                ", share='" + share + '\'' +
                ", charset='" + charset + '\'' +
                '}';
    }

    public static class ConfigurationSUTBuilder {
        private int id;
        private String name;
        private String host;
        private String port;
        private String application;
        private String facility;
        private String owner;
        private String companyOwner;
        private boolean share;
        private String charset;


        public ConfigurationSUTBuilder withId(int id) {
            this.id = id;
            return this;
        }

        public ConfigurationSUTBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public ConfigurationSUTBuilder withHost(String host) {
            this.host = host;
            return this;
        }

        public ConfigurationSUTBuilder withPort(String port) {
            this.port = port;
            return this;
        }

        public ConfigurationSUTBuilder withApplication(String application) {
            this.application = application;
            return this;
        }

        public ConfigurationSUTBuilder withFacility(String facility) {
            this.facility = facility;
            return this;
        }

        public ConfigurationSUTBuilder withOwner(String owner) {
            this.owner = owner;
            return this;
        }

        public ConfigurationSUTBuilder withCompanyOwner(String companyOwner) {
            this.companyOwner = companyOwner;
            return this;
        }

        public ConfigurationSUTBuilder withShare(boolean share) {
            this.share = share;
            return this;
        }

        public ConfigurationSUTBuilder withCharset(String charset) {
            this.charset = charset;
            return this;
        }


        public ConfigurationSUT build() {
            return new ConfigurationSUT(this);
        }
    }


}
