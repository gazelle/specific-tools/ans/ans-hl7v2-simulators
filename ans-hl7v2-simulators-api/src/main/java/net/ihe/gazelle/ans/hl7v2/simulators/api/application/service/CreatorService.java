package net.ihe.gazelle.ans.hl7v2.simulators.api.application.service;

import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.TemplateHl7v2;

import java.util.List;
/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public interface CreatorService<T> {

    void sendMessage(ConfigurationSUT configuration, TemplateHl7v2 templateHl7v2,ConfigurationSUT configurationCreator) throws InterruptedException;

    List<T> getHistoryMessage();

    void resetCreatorServer();
}
