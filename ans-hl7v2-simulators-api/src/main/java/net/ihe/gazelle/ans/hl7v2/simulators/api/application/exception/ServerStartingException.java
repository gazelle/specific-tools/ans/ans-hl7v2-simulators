package net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception;
/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public class ServerStartingException extends RuntimeException{
}
