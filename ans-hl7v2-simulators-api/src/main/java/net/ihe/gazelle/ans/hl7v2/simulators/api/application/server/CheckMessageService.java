package net.ihe.gazelle.ans.hl7v2.simulators.api.application.server;

import ca.uhn.hl7v2.HL7Exception;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 10/07/2023
 */
public interface CheckMessageService<T> {
    boolean checkMessage(T message) throws HL7Exception;

}
