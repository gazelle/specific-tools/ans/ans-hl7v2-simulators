package net.ihe.gazelle.ans.hl7v2.simulators.api.application.service;

import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;

import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public interface ConfigurationService {

    List<ConfigurationSUT> getAvailableConfigurations(String owner, String companyOwner);


    void insertConfiguration(ConfigurationSUT configuration);

    List<ConfigurationSUT> getAllConfiguration();

    ConfigurationSUT getOneConfiguration(int id);

    ConfigurationSUT findSupplier(String applicationSupplier ,String facilitySupplier);

    void updateConfiguration(ConfigurationSUT configuration);

    void deleteConfiguration(ConfigurationSUT configuration);

    boolean isApplicationExisting(String application);

    ConfigurationSUT getConfigurationFromApplication(String application);

    ConfigurationSUT getLocalCreatorConfiguration();


    ConfigurationSUT getLocalUTF8ManagerConfiguration();

    ConfigurationSUT getLocalI885915ManagerConfiguration();
}
