package net.ihe.gazelle.ans.hl7v2.simulators.api.application.server;

import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.TemplateHl7v2;

import java.util.List;
/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public interface CreatorServer<T> {

    void sendMessage(ConfigurationSUT configuration, TemplateHl7v2 templateHl7v2,ConfigurationSUT configurationSUTCreator) throws InterruptedException;

     List<T> getMessageHistory();

    void resetCreatorServer();
}
