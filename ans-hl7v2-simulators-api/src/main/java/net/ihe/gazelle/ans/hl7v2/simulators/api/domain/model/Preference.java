package net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public class Preference implements Serializable {

    @Serial
    private static final long serialVersionUID = 2749722959004530331L;
    private final int id;
    private final String className;
    private final String description;
    private final String preferenceName;
    private final String preferenceValue;

    public Preference(PreferencesBuilder builder) {
        this.id = builder.id;
        this.className = builder.className;
        this.description = builder.description;
        this.preferenceName  = builder.preferenceName;
        this.preferenceValue = builder.preferenceValue;
    }

    public static PreferencesBuilder builder() {
        return new PreferencesBuilder();
    }

    public static class PreferencesBuilder {
        private int id;
        private String className;
        private String description;
        private String preferenceName;
        private String preferenceValue;

        public PreferencesBuilder withId(int id) {
            this.id = id;
            return this;
        }
        public PreferencesBuilder withClassName(String className) {
            this.className = className;
            return this;
        }
        public PreferencesBuilder withDescription(String description) {
            this.description = description;
            return this;
        }
        public PreferencesBuilder withPreferenceName(String preferenceName) {
            this.preferenceName = preferenceName;
            return this;
        }
        public PreferencesBuilder withPreferenceValue(String preferenceValue) {
            this.preferenceValue = preferenceValue;
            return this;
        }

        public Preference build() {
            return new Preference(this);
        }
    }

    public int getId() {
        return id;
    }

    public String getClassName() {
        return className;
    }

    public String getDescription() {
        return description;
    }

    public String getPreferenceName() {
        return preferenceName;
    }

    public String getPreferenceValue() {
        return preferenceValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Preference that = (Preference) o;
        return id == that.id && Objects.equals(className, that.className) && Objects.equals(description, that.description) && Objects.equals(preferenceName, that.preferenceName) && Objects.equals(preferenceValue, that.preferenceValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, className, description, preferenceName, preferenceValue);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Preference{");
        sb.append("id=").append(id);
        sb.append(", className='").append(className).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", preferenceName='").append(preferenceName).append('\'');
        sb.append(", preferenceValue='").append(preferenceValue).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
