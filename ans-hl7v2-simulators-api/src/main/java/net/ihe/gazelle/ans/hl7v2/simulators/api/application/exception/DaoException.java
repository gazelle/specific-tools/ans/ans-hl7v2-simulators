package net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception;

import java.io.Serial;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 23/06/2023
 */
public class DaoException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = -7694009269564785013L;

    public DaoException() {
        super();
    }

    public DaoException(String s) {
        super(s);
    }

    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }

    public DaoException(Throwable cause) {
        super(cause);
    }
}
