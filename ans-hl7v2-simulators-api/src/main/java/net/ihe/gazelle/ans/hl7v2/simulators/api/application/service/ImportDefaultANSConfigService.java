package net.ihe.gazelle.ans.hl7v2.simulators.api.application.service;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 26/01/2024
 */
public interface ImportDefaultANSConfigService {

    void executeImportDefaultStatements(String scriptFilePath);
}
