package net.ihe.gazelle.ans.hl7v2.simulators.api.application.service;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public interface ManagerService {


    void switchServerStateMessages( ConfigurationService configurationService, TemplateHl7v2Service templateHl7v2Service);

    boolean isServerRunning();


}
