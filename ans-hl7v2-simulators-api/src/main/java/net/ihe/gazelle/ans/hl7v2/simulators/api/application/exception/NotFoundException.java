package net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception;

import java.io.Serial;
/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public class NotFoundException extends RuntimeException{
    @Serial
    private static final long serialVersionUID = 8583154773956522153L;

    public NotFoundException() {
        super();
    }

    public NotFoundException(String s) {
        super(s);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundException(Throwable cause) {
        super(cause);
    }
}
