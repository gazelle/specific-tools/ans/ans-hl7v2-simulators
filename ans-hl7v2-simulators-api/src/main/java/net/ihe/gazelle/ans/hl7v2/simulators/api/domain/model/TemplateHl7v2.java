package net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model;

import com.github.mustachejava.Mustache;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 21/07/2023
 */
public class TemplateHl7v2 implements Serializable {

    @Serial
    private static final long serialVersionUID = -474019864519652734L;
    private final int id;
    private final String title;
    private final String description;
    private final boolean enable;
    private final String fileName;
    private final transient Mustache templateMustache;

    public TemplateHl7v2(TemplateHl7v2Builder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.description = builder.description;
        this.enable = builder.enable;
        this.fileName = builder.fileName;
        this.templateMustache = builder.templateMustache;
    }

    public static TemplateHl7v2Builder builder() {
        return new TemplateHl7v2Builder();
    }

    public static class TemplateHl7v2Builder {
        private int id;
        private String title;
        private String description;
        private boolean enable;
        private String fileName;
        private Mustache templateMustache;

        public TemplateHl7v2Builder withId(int id) {
            this.id = id;
            return this;
        }

        public TemplateHl7v2Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public TemplateHl7v2Builder withDescription(String description) {
            this.description = description;
            return this;
        }
        public TemplateHl7v2Builder withEnabled(boolean enable) {
            this.enable = enable;
            return this;
        }

        public TemplateHl7v2Builder withFileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        public TemplateHl7v2Builder withTemplateMustache(Mustache templateMustache) {
            this.templateMustache = templateMustache;
            return this;
        }

        public TemplateHl7v2 build() {
            return new TemplateHl7v2(this);
        }

    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public boolean isEnable() {
        return enable;
    }

    public String getFileName() {
        return fileName;
    }

    public Mustache getTemplateMustache() {
        return templateMustache;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TemplateHl7v2 that = (TemplateHl7v2) o;
        return id == that.id && enable == that.enable && Objects.equals(title, that.title)  && Objects.equals(fileName, that.fileName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, enable, fileName);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TemplateHl7v2{");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", enable=").append(enable);
        sb.append(", filePath='").append(fileName).append('\'');
        sb.append(", templateMustache=").append(templateMustache);
        sb.append('}');
        return sb.toString();
    }
}
