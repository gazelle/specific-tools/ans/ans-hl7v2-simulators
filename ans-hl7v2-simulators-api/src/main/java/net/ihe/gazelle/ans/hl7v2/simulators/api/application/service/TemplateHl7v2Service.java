package net.ihe.gazelle.ans.hl7v2.simulators.api.application.service;

import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.TemplateHl7v2;

import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 21/07/2023
 */
public interface TemplateHl7v2Service {

    List<TemplateHl7v2> getAllTemplatesCDA();

    List<TemplateHl7v2> getEnableTemplates();

    TemplateHl7v2 getTemplateByTitle(String title);

    TemplateHl7v2 getTemplateByFileName(String name);

    TemplateHl7v2 getOneTemplate(int id);

    void createTemplate(TemplateHl7v2 templateHl7v2);

    void updateTemplate(TemplateHl7v2 templateHl7v2);

    void deleteTemplate(TemplateHl7v2 templateHl7v2);


    TemplateHl7v2 getZAMZ01RCVOK();

    TemplateHl7v2 getZAMZ02RCVOK();

    TemplateHl7v2 getZAMZ03LCTOK();

    TemplateHl7v2 getZAMZ03RCVKO();


    boolean istemplatetitleexist(String title);
    boolean istemplatefilenameexist(String fileName);



    boolean isZAMThere();

}
