package net.ihe.gazelle.ans.hl7v2.simulators.api.application.utils;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v26.message.ACK;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 10/07/2023
 */
public interface MessageUtils<T, M> {
    T readMessage(File hl7File);

    ACK generateAck(Message message) throws HL7Exception, IOException;

    String getIdFormatted();

    String getDateFormatted();

    M getMSH(T message);

    String getIdMessage(Message message);

    String getTransaction(Message message);

    String getTypeMessage(Message message);

    List<String> getOxbList(T message);


    List<String> getPRTList(Message message);

    String messageToString(T message);

    List<String> getSplitMessageString(String message);

    String getSegmentName(String segment);

    boolean shouldSendZ01(Message message);


    boolean shouldSendZ02(Message message);

    boolean shouldSendZ03(Message message);

    boolean shouldSendError(T message);

    boolean checkMSSante(Message message);

    boolean checkDMPDest(Message message);

    boolean matchingJDV(String name, String code, String oid,String filePath);
}
