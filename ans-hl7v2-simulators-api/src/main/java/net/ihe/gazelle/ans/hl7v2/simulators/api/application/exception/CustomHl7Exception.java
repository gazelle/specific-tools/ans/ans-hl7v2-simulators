package net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception;

import java.io.Serial;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 23/06/2023
 */
public class CustomHl7Exception extends RuntimeException {

    @Serial
    private static final long serialVersionUID = -7694009269564785013L;

    public CustomHl7Exception() {
        super();
    }

    public CustomHl7Exception(String s) {
        super(s);
    }

    public CustomHl7Exception(String message, Throwable cause) {
        super(message, cause);
    }

    public CustomHl7Exception(Throwable cause) {
        super(cause);
    }
}
