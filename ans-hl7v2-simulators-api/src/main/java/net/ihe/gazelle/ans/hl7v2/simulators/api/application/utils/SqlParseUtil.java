package net.ihe.gazelle.ans.hl7v2.simulators.api.application.utils;

import java.util.List;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 26/01/2024
 */
public interface SqlParseUtil {
    List<String> getAllStatementsFromFile(String scriptFilePath);
}
