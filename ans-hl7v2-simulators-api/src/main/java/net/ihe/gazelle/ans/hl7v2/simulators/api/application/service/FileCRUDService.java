package net.ihe.gazelle.ans.hl7v2.simulators.api.application.service;

public interface FileCRUDService<T> {

    String read(String fileName);

    void upload(T file);

    void delete(String fileName);
}
