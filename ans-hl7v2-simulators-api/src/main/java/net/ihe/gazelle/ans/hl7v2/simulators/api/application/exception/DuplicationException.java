package net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception;

import java.io.Serial;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public class DuplicationException extends  RuntimeException{

    @Serial
    private static final long serialVersionUID = -7982264062434553238L;
    public DuplicationException() {
        super();
    }

    public DuplicationException(String s) {
        super(s);
    }

    public DuplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicationException(Throwable cause) {
        super(cause);
    }
}
