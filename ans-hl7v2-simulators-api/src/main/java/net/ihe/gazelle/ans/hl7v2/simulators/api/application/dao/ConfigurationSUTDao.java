package net.ihe.gazelle.ans.hl7v2.simulators.api.application.dao;

import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;

import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public interface ConfigurationSUTDao {

    void insert(ConfigurationSUT configuration);

    List<ConfigurationSUT> getAll();


    List<ConfigurationSUT> getAvailableConfigurations(String owner, String companyOwner);


    ConfigurationSUT getOne(int id);

    ConfigurationSUT findSupplier(String application ,String facility);

    ConfigurationSUT findApplication(String application);

    void update(ConfigurationSUT configuration);

    void delete(ConfigurationSUT configuration);
}
