package net.ihe.gazelle.ans.hl7v2.simulators.api.application.dao;

import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.Preference;

import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public interface PreferenceDao {

    void insert(Preference preference);

    List<Preference> getAll();

    Preference getOne(int id);

    void update(Preference preference);

    boolean isPreferenceByNameExist(String name);

    Preference getByName(String name);

    void delete(Preference preference);
}
