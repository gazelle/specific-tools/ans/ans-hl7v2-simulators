package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.mapper;

import io.quarkus.test.junit.mockito.InjectMock;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.Preference;
import net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.entity.PreferenceEntity;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 23/06/2023
 */
class PreferenceMapperTest {

    @InjectMock
    PreferenceMapper mapper;
    private static Preference preference;
    private static List<Preference> preferenceList;
    private static PreferenceEntity preferenceEntity;
    private static List<PreferenceEntity> preferenceEntities;

    @BeforeEach
    void init() {
        mapper = PreferenceMapper.instance;
    }

    @BeforeAll
    static void setUp() {
        preference = Preference.builder()
                .withPreferenceName("casEnabled")
                .withClassName("java.lang.boolean")
                .withPreferenceValue("true")
                .withDescription("Enable CAS SSO")
                .build();
        Preference preference2 = Preference.builder()
                .build();
        preferenceList = new ArrayList<>();
        preferenceList.add(preference);
        preferenceList.add(preference2);
        preferenceEntity = PreferenceEntity.builder()
                .withPreferenceName("withCas")
                .withClassName("java.lang.boolean")
                .withPreferenceValue("true")
                .withDescription("Enable CAS SSO")
                .build();
        PreferenceEntity preferenceEntity2 = PreferenceEntity.builder().build();
        preferenceEntities = new ArrayList<>();
        preferenceEntities.add(preferenceEntity);
        preferenceEntities.add(preferenceEntity2);

    }

    @Test
    void preferenceEntityToPreference() {
        PreferenceEntity test = mapper.preferenceToPreferenceEntity(preference);
        assertNotNull(test);
    }

    @Test
    void preferenceToPreferenceEntity() {
        Preference test = mapper.preferenceEntityToPreference(preferenceEntity);
        assertNotNull(test);
    }

    @Test
    void preferenceEntitiesListTopreferenceList() {
        List<Preference> preferences = mapper.preferenceEntitiesListTopreferenceList(preferenceEntities);
        assertEquals(2, preferences.size());
    }

    @Test
    void preferenceListToPreferenceEntitiesList() {
        List<PreferenceEntity> preferences = mapper.preferenceListToPreferenceEntitiesList(preferenceList);
        assertEquals(2, preferences.size());
    }
}