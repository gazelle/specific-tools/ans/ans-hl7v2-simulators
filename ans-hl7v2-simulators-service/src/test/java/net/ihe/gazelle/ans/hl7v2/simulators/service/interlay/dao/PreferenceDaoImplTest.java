package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao;

import io.quarkus.test.junit.QuarkusTest;
import io.zonky.test.db.postgres.junit.EmbeddedPostgresRules;
import io.zonky.test.db.postgres.junit.SingleInstancePostgresRule;
import jakarta.inject.Inject;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.dao.PreferenceDao;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.Preference;
import org.junit.jupiter.api.*;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 23/06/2023
 */
@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PreferenceDaoImplTest {
    @Inject
    PreferenceDao dao;
    private static Preference preference;
    static SingleInstancePostgresRule pg;

    @BeforeAll
    static void init() {
        pg = EmbeddedPostgresRules.singleInstance();
        preference = Preference.builder()
                .withPreferenceName("casEnabled")
                .withClassName("java.lang.boolean")
                .withPreferenceValue("true")
                .withDescription("Enable CAS SSO")
                .build();
    }

    @Test
    @Order(1)
    void createPreferenceTest() {
        dao.insert(preference);
        Preference preference = dao.getOne(50);
        Assertions.assertEquals("java.lang.boolean", preference.getClassName());
    }

    @Test
    @Order(2)
    void findOnePreferenceTest() {
        Assertions.assertEquals(preference.getPreferenceValue(), dao.getOne(50).getPreferenceValue());
    }

    @Test
    @Order(3)
    void updatePreferenceTest() {
        Preference preferenceEntityUpdated = Preference.builder()
                .withId(50)
                .withPreferenceName(preference.getPreferenceName())
                .withClassName(preference.getClassName())
                .withPreferenceValue("false")
                .withDescription(preference.getDescription())
                .build();
        dao.update(preferenceEntityUpdated);
        Assertions.assertEquals("false", dao.getOne(50).getPreferenceValue());
        Assertions.assertEquals("Enable CAS SSO", dao.getOne(50).getDescription());
    }


    @Test
    @Order(4)
    void deletePreferenceTest() {
        Preference toDelete = dao.getOne(50);
        dao.delete(toDelete);
        Assertions.assertNull(dao.getOne(toDelete.getId()));
    }
}