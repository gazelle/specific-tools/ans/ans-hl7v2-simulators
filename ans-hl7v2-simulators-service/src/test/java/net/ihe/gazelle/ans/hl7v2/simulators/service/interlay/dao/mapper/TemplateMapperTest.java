package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.mapper;

import io.quarkus.test.junit.mockito.InjectMock;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.TemplateHl7v2;
import net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.entity.TemplateEntity;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 22/07/2023
 */
class TemplateMapperTest {

    @InjectMock
    TemplateMapper mapper;
    private static TemplateHl7v2 template;
    private static List<TemplateHl7v2> templateList;
    private static TemplateEntity templateEntity;
    private static List<TemplateEntity> templateEntities;

    @BeforeEach
    void init() {
        mapper = TemplateMapper.instance;
    }

    @BeforeAll
    static void setUp() {
        template = TemplateHl7v2.builder()
                .withTitle("Test")
                .withDescription("Great test")
                .withEnabled(false)
                .withFileName("/path/to/file")
                .withTemplateMustache(null)
                .build();
        TemplateHl7v2 template2 = TemplateHl7v2.builder().build();
        templateList = new ArrayList<>();
        templateList.add(template);
        templateList.add(template2);
        templateEntity = TemplateEntity.builder()
                .withTitle("Test")
                .withDescription("Great test")
                .withEnabled(false)
                .withFileName("/path/to/file")
                .withTemplateMustache(null)
                .build();
        templateEntities = new ArrayList<>();
        TemplateEntity templateEntity2 = TemplateEntity.builder().build();
        templateEntities.add(templateEntity);
        templateEntities.add(templateEntity2);
    }

    @Test
    void templateEntityToTemplate() {
        TemplateEntity test = mapper.templateToTemplateEntity(template);
        assertNotNull(test);
    }

    @Test
    void templateToTemplateEntity() {
        TemplateHl7v2 test = mapper.templateEntityToTemplate(templateEntity);
        assertNotNull(test);
    }

    @Test
    void templateEntityListToTemplateList() {
        List<TemplateHl7v2> templateHl7v2List = mapper.templateEntityListToTemplateList(templateEntities);
        assertEquals(2,templateHl7v2List.size());
    }

    @Test
    void templateListToTemplateEntityList() {
        List<TemplateEntity> entities = mapper.templateListToTemplateEntityList(templateList);
        assertEquals(2,entities.size());
    }
}