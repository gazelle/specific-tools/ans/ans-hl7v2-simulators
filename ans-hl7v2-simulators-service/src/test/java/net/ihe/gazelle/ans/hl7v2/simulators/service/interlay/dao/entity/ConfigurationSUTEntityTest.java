package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.entity;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 24/06/2023
 */
class ConfigurationSUTEntityTest {
    private static ConfigurationSUTEntity configurationSUT;
    @BeforeAll
    static void init(){
        configurationSUT = ConfigurationSUTEntity.builder()
                .withId(1)
                .withName("myConf")
                .withHost("myHost")
                .withPort("8080")
                .withApplication("myApp")
                .withFacility("myFacility")
                .withOwner("me")
                .withCompanyOwner("myCompany")
                .withShare(true)
                .withCharset("UTF-8")
                .build();
    }
    @Test
    void shouldGetAttr_OK() {
        assertNotNull(configurationSUT);
    }

    @Test
    void simpleEqualsContract() {
        EqualsVerifier
                .simple()
                .forClass(ConfigurationSUTEntity.class)
                .suppress(Warning.ALL_FIELDS_SHOULD_BE_USED)
                .withIgnoredAnnotations()
                .verify();
    }

    @Test
    void testToString() {
        String expected = "ConfigurationSUTEntity{id=1, name='myConf', host='myHost', port='8080', application='myApp', facility='myFacility', owner='me', companyOwner='myCompany', share='true', charset='UTF-8'}";
        assertEquals(expected, configurationSUT.toString());
    }

    @Test
    void getter_OK() {
        assertEquals("myHost", configurationSUT.getHost());
        assertEquals("myFacility", configurationSUT.getFacility());
        assertEquals(1, configurationSUT.getId());
        assertEquals("myApp", configurationSUT.getApplication());
        assertEquals("myConf", configurationSUT.getName());
        assertEquals("8080", configurationSUT.getPort());
        assertEquals("me", configurationSUT.getOwner());
        assertEquals("myCompany", configurationSUT.getCompanyOwner());
        assertEquals(true, configurationSUT.isShare());
        assertEquals("UTF-8", configurationSUT.getCharset());
    }

}