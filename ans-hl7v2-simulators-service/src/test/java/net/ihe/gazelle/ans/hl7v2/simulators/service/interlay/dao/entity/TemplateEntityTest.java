package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.entity;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 22/07/2023
 */
class TemplateEntityTest {
    private static TemplateEntity template;

    @BeforeAll
    static void init(){
        template = TemplateEntity.builder()
                .withId(1)
                .withTitle("Titre template")
                .withDescription("Description")
                .withEnabled(false)
                .withFileName("/path/to/file")
                .withTemplateMustache(null)
                .build();
    }

    @Test
    void shouldGetAttr_OK() {
        assertNotNull(template);
    }

    @Test
    void simpleEqualsContract() {
        EqualsVerifier
                .simple()
                .forClass(PreferenceEntity.class)
                .suppress(Warning.ALL_FIELDS_SHOULD_BE_USED)
                .withIgnoredAnnotations()
                .verify();
    }

    @Test
    void testToString() {
        String expected = "TemplateEntity{id=1, enable=false, title='Titre template', description='Description', filePath='/path/to/file', templateMustache=null}";
        assertEquals(expected, template.toString());
    }

    @Test
    void getter_OK() {
        assertEquals("Description", template.getDescription());
        assertEquals("Titre template", template.getTitle());
        assertFalse(template.isEnable());
        assertEquals("/path/to/file", template.getFileName());
        assertNull(template.getTemplateMustache());
    }
}