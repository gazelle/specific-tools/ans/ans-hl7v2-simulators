package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.utils;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import org.junit.jupiter.api.Test;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 24/07/2023
 */
@QuarkusTest
class HealthCheckEndpointTest {

    @Test
    void testHelloEndpoint() {
        RestAssured.given()
                .when().get("/rest/health")
                .then()
                .statusCode(200);
    }

}