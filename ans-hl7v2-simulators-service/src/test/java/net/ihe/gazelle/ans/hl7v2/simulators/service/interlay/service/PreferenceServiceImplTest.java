package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.service;

import net.ihe.gazelle.ans.hl7v2.simulators.api.application.dao.PreferenceDao;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.DuplicationException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.NotFoundException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.PreferenceService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.Preference;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 23/06/2023
 */
class PreferenceServiceImplTest {

    PreferenceDao dao = mock(PreferenceDao.class);
    PreferenceService preferenceService = new PreferenceServiceImpl(dao);
    private static Preference preference;

    @BeforeAll
    static void init() {
        preference = Preference.builder()
                .withPreferenceName("casEnabled")
                .withClassName("java.lang.boolean")
                .withPreferenceValue("true")
                .withDescription("Enable CAS SSO")
                .build();
    }

    @Test
    void insertPreference_OK() {
        doNothing().when(dao).insert(isA(Preference.class));
        preferenceService.insertPreference(preference);
        verify(dao, times(1)).insert(preference);
    }

    @Test
    void insert_with_duplication() {
        doNothing().when(dao).insert(isA(Preference.class));
        preferenceService.insertPreference(preference);
        verify(dao, times(1)).insert(preference);
        Preference test = Preference.builder()
                .withPreferenceName("casEnabled")
                .withClassName("java.lang.boolean")
                .withPreferenceValue("true")
                .withDescription("Enable CAS SSO")
                .build();
        preferenceService.insertPreference(test);
        doThrow(new DuplicationException()).when(dao).insert(test);
    }

    @Test
    void getAllPreference_OK() {
        List<Preference> list = new ArrayList<>();
        list.add(preference);
        when(dao.getAll()).thenReturn(list);
        List<Preference> testedList = preferenceService.getAllPreference();
        assertEquals(1, testedList.size());
    }

    @Test
    void getOnePreference_OK() {
        when(dao.getOne(50)).thenReturn(preference);
        Preference sut = preferenceService.getOnePreference(50);
        assertEquals("casEnabled", sut.getPreferenceName());
    }

    @Test
    void updatePreference_OK() {
        when(dao.getOne(preference.getId())).thenReturn(preference);
        preferenceService.updatePreference(preference);
        verify(dao, times(1)).update(preference);
    }

    @Test
    void updatePreference_DuplicationException() {
        when(dao.getOne(preference.getId())).thenReturn(preference);
        preferenceService.insertPreference(preference);
        verify(dao).insert(preference);
        Preference test = Preference.builder()
                .withPreferenceName("casEnabled")
                .withClassName("java.lang.boolean")
                .withPreferenceValue("true")
                .withDescription("Enable CAS SSO")
                .build();
        preferenceService.updatePreference(test);
        verify(dao).update(test);
        doThrow(new DuplicationException()).when(dao).update(test);
    }

    @Test
    void updatePreference_NotFoundException() {
        when(dao.getOne(preference.getId())).thenThrow(new NotFoundException());
        assertThrows(NotFoundException.class, () -> preferenceService.updatePreference(preference));
    }

    @Test
    void deletePreference_OK() {
        when(dao.getOne(preference.getId())).thenReturn(preference);
        preferenceService.deletePreference(preference);
        verify(dao).delete(preference);
    }

    @Test
    void deletePreference_NotFoundException() {
        Preference test = Preference.builder()
                .withId(51)
                .withPreferenceName("casEnabled")
                .withClassName("java.lang.boolean")
                .withPreferenceValue("true")
                .withDescription("Enable CAS SSO")
                .build();
        assertThrows(NotFoundException.class, () -> preferenceService.deletePreference(test));
    }

    @Test
    void getPreferenceByName_OK() {
        when(dao.getByName("cas")).thenReturn(preference);
        preferenceService.getPreferenceByName("cas");
        verify(dao).getByName("cas");
    }

    @Test
    void preferenceCreator_OK(){
        assertEquals("ans-simulators-creator", preferenceService.getLocalCreatorApplication()); ;
        assertEquals("ANS",preferenceService.getLocalCreatorFacility());
        assertEquals("127.0.0.1",preferenceService.getLocalCreatorHost());
        assertEquals("5456", preferenceService.getLocalCreatorPort());

        assertEquals("ans-simulators-utf8-manager",preferenceService.getLocalUTF8ManagerApplication());
        assertEquals("ANS", preferenceService.getLocalUTF8ManagerFacility());
        assertEquals("127.0.0.1",preferenceService.getLocalUTF8ManagerHost());
        assertEquals("5453",preferenceService.getLocalUTF8ManagerPort());



        assertEquals("ans-simulators-i885915-manager",preferenceService.getLocalI885915ManagerApplication());
        assertEquals("ANS", preferenceService.getLocalI885915ManagerFacility());
        assertEquals("127.0.0.1",preferenceService.getLocalI885915ManagerHost());
        assertEquals("5455",preferenceService.getLocalI885915ManagerPort());


    }
}