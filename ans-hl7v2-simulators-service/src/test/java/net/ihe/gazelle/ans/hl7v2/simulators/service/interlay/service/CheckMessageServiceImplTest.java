package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.service;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.server.CheckMessageService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.PreferenceService;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.utils.CustomHapiContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
 class CheckMessageServiceImplTest {

    CheckMessageServiceImpl checkMessageService = new CheckMessageServiceImpl();

    String ORU_OK = "MSH|^˜\\&|Test|TestFacility|Test|TestFacility|{{idMessage}}||ORU^R01^ORU_R01|001|P|2.5|||||FRA|UNICODE UTF-8|||2.0^CISIS_CDA_HL7_V2";
    String MDMTO2LPS = "MSH|^˜\\&|Test|TestFacility|Test|TestFacility|{{idMessage}}||MDM^T02^MDM_T02|001|P|2.5|||||FRA|UNICODE UTF-8|||2.0^CISIS_CDA_HL7_LPS";
    String MDMTO4LPS = "MSH|^˜\\&|Test|TestFacility|Test|TestFacility|{{idMessage}}||MDM^T04^MDM_T02|001|P|2.5|||||FRA|UNICODE UTF-8|||2.0^CISIS_CDA_HL7_LPS";
    String MDMT10LPS = "MSH|^˜\\&|Test|TestFacility|Test|TestFacility|{{idMessage}}||MDM^T10^MDM_T02|001|P|2.5|||||FRA|UNICODE UTF-8|||2.0^CISIS_CDA_HL7_LPS";

    String MDMTO2V2 = "MSH|^˜\\&|Test|TestFacility|Test|TestFacility|{{idMessage}}||MDM^T02^MDM_T02|001|P|2.5|||||FRA|UNICODE UTF-8|||2.0^CISIS_CDA_HL7_V2";
    String MDMTO4V2 = "MSH|^˜\\&|Test|TestFacility|Test|TestFacility|{{idMessage}}||MDM^T04^MDM_T02|001|P|2.5|||||FRA|UNICODE UTF-8|||2.0^CISIS_CDA_HL7_V2";
    String MDMT10V2 = "MSH|^˜\\&|Test|TestFacility|Test|TestFacility|{{idMessage}}||MDM^T10^MDM_T02|001|P|2.5|||||FRA|UNICODE UTF-8|||2.0^CISIS_CDA_HL7_V2";

    String validationResult ="<SummaryResults>\n" +
            "2023-08-22T08:24:02.572575240Z     <ValidationResultsOverview>\n" +
            "2023-08-22T08:24:02.572577715Z         <SDF_DATE>yyyy, MM dd</SDF_DATE>\n" +
            "2023-08-22T08:24:02.572580278Z         <SDF_TIME>hh:mm (aa)</SDF_TIME>\n" +
            "2023-08-22T08:24:02.572582331Z         <Disclaimer>The GazelleHL7v2Validator is an experimental system. IHE-Europe assumes no responsibility whatsoever for its use by other parties, and makes no guarantees, expressed or implied, about its quality, reliability, or any other characteristic. We would appreciate acknowledgement if the service is used. Bug tracking service is available at http://gazelle.ihe.net/jira/browse/HLVAL</Disclaimer>\n" +
            "2023-08-22T08:24:02.572585241Z         <ProfileOID>1.3.6.1.4.1.12559.11.36.8.3.19</ProfileOID>\n" +
            "2023-08-22T08:24:02.572587445Z         <MessageOID>1.3.6.1.4.1.12559.11.36.8.2.12760</MessageOID>\n" +
            "2023-08-22T08:24:02.572589485Z         <ValidationTestResult>PASSED</ValidationTestResult>\n" +
            "2023-08-22T08:24:02.572591514Z         <ValidationServiceVersion>3.8.5</ValidationServiceVersion>\n" +
            "2023-08-22T08:24:02.572594091Z         <ValidationDate>2023, 08 22</ValidationDate>\n" +
            "2023-08-22T08:24:02.572596286Z         <ValidationServiceName>Gazelle HL7 Validator</ValidationServiceName>\n" +
            "2023-08-22T08:24:02.572599039Z         <ValidationTime>10:23 (AM)</ValidationTime>\n" +
            "2023-08-22T08:24:02.572601816Z     </ValidationResultsOverview> </SummaryResults>";



    @Test
    void getTypeMessage_ORU() {
        Message message=null;
        try (CustomHapiContext context = new CustomHapiContext("UNICODE UTF-8")) {
            message = context.getPipeParser().parse(ORU_OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assertions.assertEquals("1.3.6.1.4.1.12559.11.36.8.3.19", checkMessageService.getProfilOIDFromMessage(message));
    }


    @Test
    void getTypeMessage_MDMTO2LPS() {
        Message message=null;
        try (CustomHapiContext context = new CustomHapiContext("UNICODE UTF-8")) {
            message = context.getPipeParser().parse(MDMTO2LPS);
        } catch (Exception e) {
            e.printStackTrace();
        }
       Assertions.assertEquals("1.3.6.1.4.1.12559.11.36.8.3.20", checkMessageService.getProfilOIDFromMessage(message));
    }

    @Test
    void getTypeMessage_MDMTO4LPS() {
        Message message=null;
        try (CustomHapiContext context = new CustomHapiContext("UNICODE UTF-8")) {
            message = context.getPipeParser().parse(MDMTO4LPS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assertions.assertEquals("1.3.6.1.4.1.12559.11.36.8.3.22", checkMessageService.getProfilOIDFromMessage(message));
    }

    @Test
    void getTypeMessage_MDMTO10LPS() {
        Message message=null;
        try (CustomHapiContext context = new CustomHapiContext("UNICODE UTF-8")) {
            message = context.getPipeParser().parse(MDMT10LPS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assertions.assertEquals("1.3.6.1.4.1.12559.11.36.8.3.23", checkMessageService.getProfilOIDFromMessage(message));
    }





    @Test
    void getTypeMessage_MDMTO2V2() {
        Message message=null;
        try (CustomHapiContext context = new CustomHapiContext("UNICODE UTF-8")) {
            message = context.getPipeParser().parse(MDMTO2V2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assertions.assertEquals("1.3.6.1.4.1.12559.11.36.8.3.24", checkMessageService.getProfilOIDFromMessage(message));
    }

    @Test
    void getTypeMessage_MDMTO4V2() {
        Message message=null;
        try (CustomHapiContext context = new CustomHapiContext("UNICODE UTF-8")) {
            message = context.getPipeParser().parse(MDMTO4V2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assertions.assertEquals("1.3.6.1.4.1.12559.11.36.8.3.25", checkMessageService.getProfilOIDFromMessage(message));
    }

    @Test
    void getTypeMessage_MDMT10V2() {
        Message message=null;
        try (CustomHapiContext context = new CustomHapiContext("UNICODE UTF-8")) {
            message = context.getPipeParser().parse(MDMT10V2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assertions.assertEquals("1.3.6.1.4.1.12559.11.36.8.3.21", checkMessageService.getProfilOIDFromMessage(message));
    }

    @Test
    void validationResult_PASSED(){
    Assertions.assertEquals("PASSED",checkMessageService.getValidationResult(validationResult));
    }




}
