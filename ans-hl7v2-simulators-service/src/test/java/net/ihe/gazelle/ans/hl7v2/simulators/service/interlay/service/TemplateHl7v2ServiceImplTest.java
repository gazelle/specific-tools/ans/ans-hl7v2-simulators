package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.service;

import net.ihe.gazelle.ans.hl7v2.simulators.api.application.dao.TemplateDao;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.DuplicationException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.NotFoundException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.TemplateHl7v2Service;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.TemplateHl7v2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 19/07/2023
 */
class TemplateHl7v2ServiceImplTest {

    TemplateDao dao = mock(TemplateDao.class);
    TemplateHl7v2Service service = new TemplateHl7v2ServiceImpl(dao);
    private static TemplateHl7v2 template;

    @BeforeAll
    static void init() {
        template = TemplateHl7v2.builder()
                .withTitle("Titre template")
                .withDescription("Description")
                .withEnabled(false)
                .withFileName("test.mustache")
                .withTemplateMustache(null)
                .build();
    }

    @Test
    void createTemplate() {
        doNothing().when(dao).insert(isA(TemplateHl7v2.class));
        service.createTemplate(template);
        verify(dao).insert(template);
    }

    @Test
    void createTemplate__with_duplication_OK() {
        doNothing().when(dao).insert(isA(TemplateHl7v2.class));
        service.createTemplate(template);
        verify(dao, times(1)).insert(template);
        TemplateHl7v2 test = TemplateHl7v2.builder()
                .withTitle("Titre template")
                .withDescription("Description")
                .withEnabled(false)
                .withFileName("test.mustache")
                .withTemplateMustache(null)
                .build();
        service.createTemplate(test);
        doThrow(new DuplicationException()).when(dao).insert(test);
    }

    @Test
    void getAllTemplatesCDA_OK() {
        List<TemplateHl7v2> list = new ArrayList<>();
        list.add(template);
        when(dao.getAll()).thenReturn(list);
        List<TemplateHl7v2> testedList = service.getAllTemplatesCDA();
        assertEquals(1, testedList.size());
    }

    @Test
    void getAllTemplatesEnableCDA_OK() {
        List<TemplateHl7v2> list = new ArrayList<>();
        list.add(template);
        when(dao.getEnables()).thenReturn(list);
        List<TemplateHl7v2> testedList = service.getEnableTemplates();
        assertEquals(1, testedList.size());
    }
    @Test
    void getOneTemplate_OK() {
        when(dao.getOne(50)).thenReturn(template);
        TemplateHl7v2 test = service.getOneTemplate(50);
        assertEquals("Titre template", test.getTitle());
    }

    @Test
    void updateTemplate_OK() {
        when(dao.getOne(template.getId())).thenReturn(template);
        service.updateTemplate(template);
        verify(dao).update(template);
    }

    @Test
    void updateTemplate_DuplicationException() {
        when(dao.getOne(template.getId())).thenReturn(template);
        service.createTemplate(template);
        verify(dao).insert(template);
        TemplateHl7v2 test = TemplateHl7v2.builder()
                .withTitle("Titre template")
                .withDescription("Description")
                .withEnabled(false)
                .withFileName("test2.mustache")
                .withTemplateMustache(null)
                .build();
        service.updateTemplate(test);
        verify(dao).update(test);
        doThrow(new DuplicationException()).when(dao).update(test);
    }

    @Test
    void deleteTemplate_OK() {
        when(dao.getOne(template.getId())).thenReturn(template);
        service.deleteTemplate(template);
        verify(dao).delete(template);
    }

    @Test
    void deleteTemplate_NotFoundException() {
        TemplateHl7v2 test = TemplateHl7v2.builder()
                .withId(51)
                .withTitle("Titre template")
                .withDescription("Description")
                .withEnabled(false)
                .withFileName("test.mustache")
                .withTemplateMustache(null)
                .build();
        assertThrows(NotFoundException.class, () -> service.deleteTemplate(test));
    }

    @Test
    void getTemplateByFileName() {
        when(dao.getByFileName("test.mustache")).thenReturn(template);
        service.getTemplateByFileName("test.mustache");
        verify(dao).getByFileName("test.mustache");
    }

    @Test
    void getTemplateByTitle() {
        when(dao.getByTitle("Titre template")).thenReturn(template);
        service.getTemplateByTitle("Titre template");
        verify(dao).getByTitle("Titre template");
    }

    @Test
    void isZAMThere_OK() {
        Assertions.assertDoesNotThrow(() -> service.isZAMThere());
    }


}