package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao;

import io.quarkus.test.junit.QuarkusTest;
import io.zonky.test.db.postgres.junit.EmbeddedPostgresRules;
import io.zonky.test.db.postgres.junit.SingleInstancePostgresRule;
import jakarta.inject.Inject;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.dao.TemplateDao;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.TemplateHl7v2;
import org.junit.jupiter.api.*;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 22/07/2023
 */
@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TemplateDaoImplTest {
    @Inject
    TemplateDao dao;
    private static TemplateHl7v2 template;
    static SingleInstancePostgresRule pg;

    @BeforeAll
    static void init() {
        pg = EmbeddedPostgresRules.singleInstance();
        template = TemplateHl7v2.builder()
                .withTitle("Template title")
                .withDescription("Template description")
                .withEnabled(true)
                .withFileName("/path/to/file")
                .withTemplateMustache(null)
                .build();
    }

    @Test
    @Order(1)
    void createTemplateTest() {
        dao.insert(template);
        TemplateHl7v2 test = dao.getOne(50);
        Assertions.assertEquals("/path/to/file", test.getFileName());
    }

    @Test
    @Order(2)
    void getFilebyNameTest() {
        Assertions.assertTrue(dao.isFileNameExist("/path/to/file"));
    }


    @Test
    @Order(3)
    void findOneTemplateTest() {
        Assertions.assertEquals(template.getTitle(), dao.getOne(50).getTitle());
    }

    @Test
    @Order(4)
    void updateTemplateTest() {
        TemplateHl7v2 templateUpdated = TemplateHl7v2.builder()
                .withId(50)
                .withTitle(template.getTitle())
                .withDescription(template.getDescription())
                .withEnabled(false)
                .withFileName(template.getFileName())
                .withTemplateMustache(null)
                .build();
        dao.update(templateUpdated);
        Assertions.assertFalse(dao.getOne(50).isEnable());
        Assertions.assertEquals("Template description", dao.getOne(50).getDescription());
    }

    @Test
    @Order(5)
    void deleteTemplateTest() {
        TemplateHl7v2 toDelete = dao.getOne(50);
        dao.delete(toDelete);
        Assertions.assertNull(dao.getOne(toDelete.getId()));
    }


}