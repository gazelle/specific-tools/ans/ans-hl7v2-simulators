package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.mapper;

import io.quarkus.test.junit.mockito.InjectMock;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;
import net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.entity.ConfigurationSUTEntity;
import net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.mapper.ConfigurationSUTMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 23/06/2023
 */
class ConfigurationSUTMapperTest {

    @InjectMock
    ConfigurationSUTMapper mapper;
    private static ConfigurationSUT sut;
    private static List<ConfigurationSUT> sutList;
    private static ConfigurationSUTEntity sutEntity;
    private static List<ConfigurationSUTEntity> sutEntityList;

    @BeforeEach
    void init() {
        mapper = ConfigurationSUTMapper.instance;
    }

    @BeforeEach
    void setUp() {
        sut = ConfigurationSUT.builder()
                .withId(1)
                .withName("myConf")
                .withHost("myHost")
                .withPort("8080")
                .withApplication("myApp")
                .withFacility("myFacility")
                .withOwner("me")
                .withCompanyOwner("myCompany")
                .withShare(true)
                .withCharset("UTF-8")
                .build();
        ConfigurationSUT sut1 = ConfigurationSUT.builder()
                .build();
        sutList = new ArrayList<>();
        sutList.add(sut);
        sutList.add(sut1);
        sutEntity = ConfigurationSUTEntity.builder()
                .withId(2)
                .withName("myConf")
                .withHost("myHost")
                .withPort("8080")
                .withApplication("myApp")
                .withFacility("myFacility")
                .withOwner("me")
                .withCompanyOwner("myCompany")
                .withShare(true)
                .withCharset("UTF-8")
                .build();
        ConfigurationSUTEntity preferenceEntity2 = ConfigurationSUTEntity.builder().build();
        sutEntityList = new ArrayList<>();
        sutEntityList.add(sutEntity);
        sutEntityList.add(preferenceEntity2);
    }

    @Test
    void sutEntityToSut() {
        ConfigurationSUTEntity test = mapper.sutToSutEntity(sut);
        assertNotNull(test);
    }

    @Test
    void sutToSutEntity() {
        ConfigurationSUT test = mapper.sutEntityToSut(sutEntity);
        assertNotNull(test);
    }

    @Test
    void sutEntitiesListToSutList() {
        List<ConfigurationSUT> list = mapper.sutEntitiesListToSutList(sutEntityList);
        Assertions.assertEquals(2, list.size());
    }

    @Test
    void sutListToSutEntitiesList() {
        List<ConfigurationSUTEntity> list = mapper.sutListToSutEntitiesList(sutList);
        Assertions.assertEquals(2, list.size());
    }
}