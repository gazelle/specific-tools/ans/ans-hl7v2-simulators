package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.service;

import jakarta.inject.Inject;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.dao.ConfigurationSUTDao;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.dao.PreferenceDao;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.DuplicationException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.NotFoundException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.ConfigurationService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.PreferenceService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.Preference;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 19/07/2023
 */
class ConfigurationServiceImplTest {

    ConfigurationSUTDao dao = mock(ConfigurationSUTDao.class);
    PreferenceService preferenceService = mock(PreferenceService.class);
    ConfigurationService service = new ConfigurationServiceImpl(dao, preferenceService);
    private ConfigurationSUT configurationSUT;


    @BeforeEach
    void init() {
        configurationSUT = ConfigurationSUT.builder()
                .withId(50)
                .withName("myConf")
                .withHost("myHost")
                .withPort("8080")
                .withApplication("myApp")
                .withFacility("myFacility")
                .withOwner("me")
                .withCompanyOwner("myCompany")
                .withShare(true)
                .withCharset("UTF-8")
                .build();

    }

    @Test
    void insertConfiguration_OK() {
        doNothing().when(dao).insert(isA(ConfigurationSUT.class));
        service.insertConfiguration(configurationSUT);
        verify(dao, times(1)).insert(configurationSUT);
    }

    @Test
    void getAllConfiguration_OK() {
        List<ConfigurationSUT> list = new ArrayList<>();
        list.add(configurationSUT);
        when(dao.getAll()).thenReturn(list);
        List<ConfigurationSUT> testedList = service.getAllConfiguration();
        assertEquals(1, testedList.size());
    }

    @Test
    void getOneConfiguration_OK() {
        when(dao.getOne(50)).thenReturn(configurationSUT);
        ConfigurationSUT sut = service.getOneConfiguration(50);
        assertEquals("myFacility", sut.getFacility());
    }

    @Test
    void updateConfiguration_OK() {
        when(dao.getOne(configurationSUT.getId())).thenReturn(configurationSUT);
        service.updateConfiguration(configurationSUT);
        verify(dao).update(configurationSUT);
    }

    @Test
    void delete_OK() {
        when(dao.getOne(configurationSUT.getId())).thenReturn(configurationSUT);
        service.deleteConfiguration(configurationSUT);
        verify(dao).delete(configurationSUT);
    }

    @Test
    void insert_with_duplication() {
        doNothing().when(dao).insert(isA(ConfigurationSUT.class));
        service.insertConfiguration(configurationSUT);
        verify(dao, times(1)).insert(configurationSUT);
        ConfigurationSUT test = ConfigurationSUT.builder()
                .withName("myConf")
                .withHost("myHost")
                .withPort("8080")
                .withApplication("myApp")
                .withFacility("myFacility")
                .withOwner("me")
                .withCompanyOwner("myCompany")
                .withShare(true)
                .withCharset("UTF-8")
                .build();
        service.insertConfiguration(test);
        doThrow(new DuplicationException()).when(dao).insert(test);
    }

    @Test
    void findSupplier() {
        when(dao.findSupplier("myApp", "myFacility")).thenReturn(configurationSUT);
        ConfigurationSUT test = service.findSupplier("myApp", "myFacility");
        assertEquals("myApp", test.getApplication());
        assertEquals("myFacility", test.getFacility());
    }

    @Test
    void getConfigurationFromApplication_OK() {
        when(dao.findApplication("myApp")).thenReturn(configurationSUT);
        Assertions.assertEquals(service.getConfigurationFromApplication("myApp"), configurationSUT);


    }

    @Test
    void getConfigurationFromApplication_KO() {
        when(dao.findApplication("myApp")).thenReturn(null);
        Assertions.assertThrows(NotFoundException.class, () -> service.getConfigurationFromApplication("myApp"));
    }

    @Test
    void getConfigurationLocalCreator_OK() {
        when(preferenceService.getLocalCreatorApplication()).thenReturn("App");
        when(preferenceService.getLocalCreatorFacility()).thenReturn("Facility");
        when(preferenceService.getLocalCreatorHost()).thenReturn("Host");
        when(preferenceService.getLocalCreatorPort()).thenReturn("Port");

        Assertions.assertNotEquals(null, service.getLocalCreatorConfiguration());
        Assertions.assertEquals("App", service.getLocalCreatorConfiguration().getApplication());
        Assertions.assertEquals("Facility", service.getLocalCreatorConfiguration().getFacility());
        Assertions.assertEquals("Host", service.getLocalCreatorConfiguration().getHost());
        Assertions.assertEquals("Port", service.getLocalCreatorConfiguration().getPort());

    }

    @Test
    void getConfigurationLocalUTF8Manager_OK() {
        when(preferenceService.getLocalUTF8ManagerApplication()).thenReturn("App");
        when(preferenceService.getLocalUTF8ManagerFacility()).thenReturn("Facility");
        when(preferenceService.getLocalUTF8ManagerHost()).thenReturn("Host");
        when(preferenceService.getLocalUTF8ManagerPort()).thenReturn("Port");

        Assertions.assertNotEquals(null, service.getLocalUTF8ManagerConfiguration());
        Assertions.assertEquals("App", service.getLocalUTF8ManagerConfiguration().getApplication());
        Assertions.assertEquals("Facility", service.getLocalUTF8ManagerConfiguration().getFacility());
        Assertions.assertEquals("Host", service.getLocalUTF8ManagerConfiguration().getHost());
        Assertions.assertEquals("Port", service.getLocalUTF8ManagerConfiguration().getPort());
    }


    @Test
    void getConfigurationLocalI885915Manager_OK() {
        when(preferenceService.getLocalI885915ManagerApplication()).thenReturn("App");
        when(preferenceService.getLocalI885915ManagerFacility()).thenReturn("Facility");
        when(preferenceService.getLocalI885915ManagerHost()).thenReturn("Host");
        when(preferenceService.getLocalI885915ManagerPort()).thenReturn("Port");

        Assertions.assertNotEquals(null, service.getLocalI885915ManagerConfiguration());
        Assertions.assertEquals("App", service.getLocalI885915ManagerConfiguration().getApplication());
        Assertions.assertEquals("Facility", service.getLocalI885915ManagerConfiguration().getFacility());
        Assertions.assertEquals("Host", service.getLocalI885915ManagerConfiguration().getHost());
        Assertions.assertEquals("Port", service.getLocalI885915ManagerConfiguration().getPort());
    }


}