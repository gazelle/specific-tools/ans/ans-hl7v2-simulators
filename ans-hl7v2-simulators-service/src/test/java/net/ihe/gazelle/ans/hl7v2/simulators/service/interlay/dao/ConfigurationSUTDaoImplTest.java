package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao;

import io.quarkus.test.junit.QuarkusTest;
import io.zonky.test.db.postgres.junit.EmbeddedPostgresRules;
import io.zonky.test.db.postgres.junit.SingleInstancePostgresRule;
import jakarta.inject.Inject;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.dao.ConfigurationSUTDao;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;
import org.junit.jupiter.api.*;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ConfigurationSUTDaoImplTest {
    @Inject
    ConfigurationSUTDao dao;
    private static ConfigurationSUT configurationSUT;
    static SingleInstancePostgresRule pg;

    @BeforeAll
    static void init() {
        pg = EmbeddedPostgresRules.singleInstance();
        configurationSUT = ConfigurationSUT.builder()
                .withName("myConf")
                .withHost("myHost")
                .withPort("8080")
                .withApplication("myApp")
                .withFacility("myFacility")
                .withOwner("me")
                .withCompanyOwner("myCompany")
                .withShare(true)
                .withCharset("UTF-8")
                .build();
    }

    @Test
    @Order(1)
    void createConfigurationSUT_Test() {
        dao.insert(configurationSUT);
        ConfigurationSUT entity = dao.getOne(50);
        Assertions.assertEquals("myHost", entity.getHost());
    }

    @Test
    @Order(2)
    void findOneConfigurationSUT_Test() {
        Assertions.assertEquals(configurationSUT.getHost(), dao.getOne(50).getHost());
    }

    @Test
    @Order(3)
    void updateConfigurationSUT_Test() {
        ConfigurationSUT configurationSUTUpdated = ConfigurationSUT.builder()
                .withId(50)
                .withHost("myNewHost")
                .build();
        dao.update(configurationSUTUpdated);
        Assertions.assertEquals("myNewHost", dao.getOne(50).getHost());
        Assertions.assertEquals("8080", dao.getOne(50).getPort());
    }


    @Test
    @Order(4)
    void findSupplier_Test() {
        ConfigurationSUT entity = dao.findSupplier("myApp", "myFacility");
        Assertions.assertEquals("myApp", entity.getApplication());
    }

    @Test
    @Order(5)
    void deleteConfigurationSUT_Test() {
        ConfigurationSUT toDelete = dao.getOne(50);
        dao.delete(toDelete);
        Assertions.assertNull(dao.getOne(toDelete.getId()));
    }
}