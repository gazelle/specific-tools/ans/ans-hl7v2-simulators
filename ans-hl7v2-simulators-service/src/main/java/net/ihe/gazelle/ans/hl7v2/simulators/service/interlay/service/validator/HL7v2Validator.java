package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.service.validator;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.DefaultXMLParser;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.CustomHl7Exception;
import net.ihe.gazelle.hl7.validator.GazelleHL7V2ValidationWSServiceStub;
import net.ihe.gazelle.hl7.validator.SOAPExceptionException;
import net.ihe.gazelle.hl7.ws.ValidateMessage;
import net.ihe.gazelle.hl7.ws.ValidateMessageE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public class HL7v2Validator {

    public static final String DEFAULT_XSL = "http://gazelle.ihe.net/xsl/hl7Validation/resultStylesheet.xsl";
    public static final long DEFAULT_TIMEOUT = 30000;

    private static final Logger log = LoggerFactory.getLogger(HL7v2Validator.class);

    protected String endpoint;
    protected String validationResult;
    protected String xslLocation;
    protected XmlResultParser resultParser;
    protected Map<String, ValidationContextBuilder.FaultLevel> validationOptions;
    protected long timeout;

    public HL7v2Validator(String endpoint) {
        this.endpoint = endpoint;
        this.xslLocation = DEFAULT_XSL;
        this.timeout = DEFAULT_TIMEOUT;
        validationOptions = new HashMap<>();
        validationOptions.put(ValidationContextBuilder.LENGTH, ValidationContextBuilder.FaultLevel.WARNING);
        validationOptions.put(ValidationContextBuilder.MESSAGE_STRUCTURE, ValidationContextBuilder.FaultLevel.ERROR);
        validationOptions.put(ValidationContextBuilder.DATATYPE, ValidationContextBuilder.FaultLevel.ERROR);
        validationOptions.put(ValidationContextBuilder.DATAVALUE, ValidationContextBuilder.FaultLevel.WARNING);
    }

    public HL7v2Validator(String endpoint, Map<String, ValidationContextBuilder.FaultLevel> validationOptions) {
        this.endpoint = endpoint;
        this.xslLocation = DEFAULT_XSL;
        this.validationOptions = validationOptions;
    }

    public String validate(String hl7v2Message, String profileOID, String encodingCharacter) {
        return validate(hl7v2Message, profileOID, encodingCharacter, MessageEncoding.ER7);
    }

    /**
     * Creates the query and calls the validation service
     *
     * @param hl7v2Message      : the message to validate (ER7 format)
     * @param profileOID        : the OID of the message profile to validate against (see http://gazelle.ihe.net/GMM)
     * @param encodingCharacter : if null use UTF-8
     * @param messageEncoding   : ER7 or XML
     * @return : string of the validation result
     */

    public String validate(String hl7v2Message, String profileOID, String encodingCharacter,
                           MessageEncoding messageEncoding) {
        if (hl7v2Message == null || hl7v2Message.isEmpty() || profileOID == null || profileOID.isEmpty()) {
            validationResult = null;
        } else if (endpoint != null && !endpoint.isEmpty()) {

            validationResult = validateProcess(hl7v2Message, profileOID, encodingCharacter,
                    messageEncoding);
        } else {
            log.error("The web service endpoint is not defined! Cannot process validation");
            throw new CustomHl7Exception(getClass().getSimpleName() + " validation failed: The web service endpoint is not defined! Cannot process validation");
        }
        resultParser = new XmlResultParser(validationResult);
        return validationResult;
    }

    public String validateProcess(String hl7v2Message, String profileOID, String encodingCharacter,
                                  MessageEncoding messageEncoding) {
        // create stub
        try {
            GazelleHL7V2ValidationWSServiceStub stub = new GazelleHL7V2ValidationWSServiceStub(endpoint);
            stub._getServiceClient().getOptions().setTimeOutInMilliSeconds(this.timeout);
            stub._getServiceClient().getOptions().setProperty(org.apache.axis2.Constants.Configuration.DISABLE_SOAP_ACTION, true);
            // set request parameters
            ValidationContextBuilder contextBuilder = new ValidationContextBuilder(profileOID, encodingCharacter,
                    validationOptions);
            String validationContext = contextBuilder.build();
            ValidateMessage params = new ValidateMessage();
            params.setMessageToValidate(getER7EncodedMessage(hl7v2Message, messageEncoding));
            params.setXmlValidationContext(validationContext);
            ValidateMessageE paramsE = new ValidateMessageE();
            paramsE.setValidateMessage(params);
            return stub.validateMessage(paramsE).getValidateMessageResponse().getResult();
        } catch (SOAPExceptionException | RemoteException e) {
            throw new CustomHl7Exception(getClass().getSimpleName() + " validation failed" + (e.getMessage() != null && !e.getMessage().trim().isEmpty() ? ":" + e.getMessage() : ""), e);
        }
    }


    private String getER7EncodedMessage(String hl7v2Message, MessageEncoding messageEncoding) {
        if (messageEncoding != null && messageEncoding.equals(MessageEncoding.XML)) {
            try {
                DefaultXMLParser parser = (DefaultXMLParser) DefaultXMLParser.getInstanceWithNoValidation();
                Message message = parser.parse(hl7v2Message);
                PipeParser pipeParser = PipeParser.getInstanceWithNoValidation();
                return pipeParser.encode(message);
            } catch (HL7Exception e) {
                log.error(e.getMessage(), e);
                return hl7v2Message;
            }
        } else {
            return hl7v2Message;
        }
    }

    /**
     * Extracts the status of the last validation from the returned XML
     *
     * @return String ValidationStatus
     */
    public String getLastValidationStatus() {
        return resultParser.getValidationStatus();
    }

    /**
     * Extracts the number of errors from the last validation result
     *
     * @return int Count of errors for last validation
     */
    public int getErrorsCountForLastValidation() {
        return resultParser.getErrorCount();
    }

    /**
     * Extracts the number of warnings from the last validation result
     *
     * @return int Count of warns for last validation
     */
    public int getWarningsCountForLastValidation() {
        return resultParser.getWarningCount();
    }

    /**
     * Getters and Setters
     */
    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getValidationResult() {
        return validationResult;
    }

    public void setValidationResult(String validationResult) {
        this.validationResult = validationResult;
    }

    public String getXslLocation() {
        return xslLocation;
    }

    public void setXslLocation(String xslLocation) {
        this.xslLocation = xslLocation;
    }

    public Map<String, ValidationContextBuilder.FaultLevel> getValidationOptions() {
        return validationOptions;
    }

    public void setValidationOptions(Map<String, ValidationContextBuilder.FaultLevel> validationOptions) {
        this.validationOptions = validationOptions;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }
}
