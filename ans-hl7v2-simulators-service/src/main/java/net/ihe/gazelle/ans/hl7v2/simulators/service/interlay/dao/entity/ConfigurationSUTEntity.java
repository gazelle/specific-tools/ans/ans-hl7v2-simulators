package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
@Entity(name = "ConfigurationSUT")
@Table(name = "cmn_application_configuration_sut")
@NamedQuery(name = "ConfigurationSUTEntity.findAll", query = "FROM ConfigurationSUT c")
@NamedQuery(name = "ConfigurationSUTEntity.findAvailableConfigurations", query = "FROM ConfigurationSUT c where  c.companyOwner=:companyOwner OR c.share=true")
@NamedQuery(name = "ConfigurationSUTEntity.findSupplier", query = "FROM ConfigurationSUT c where c.application=:application AND c.facility=:facility")
@NamedQuery(name = "ConfigurationSUTEntity.findApplication", query = "FROM ConfigurationSUT c where c.application=:application")
@NamedQuery(name = "ConfigurationSUTEntity.deleteOne", query = "DELETE FROM ConfigurationSUT c WHERE c.id = :id")
@NamedQuery(name = "ConfigurationSUTEntity.updateOne", query = "UPDATE ConfigurationSUT c SET c.name=:name , c.host=:host, c.port=:port, c.application =:application, c.facility=:facility,c.owner=:owner,c.companyOwner=:companyOwner,c.share=:share,c.charset=:charset WHERE c.id=:id")

public class ConfigurationSUTEntity implements Serializable, Comparable<ConfigurationSUTEntity> {

    @Serial
    private static final long serialVersionUID = -951545649136777331L;
    @Id
    @SequenceGenerator(name = "configurationSeqGen", sequenceName = "configurationSeq", initialValue = 50, allocationSize = 100)
    @GeneratedValue(generator = "configurationSeqGen")
    private int id;
    @NotNull
    @NotBlank
    @Column(name = "name", nullable = false)
    private String name;
    @NotNull
    @NotBlank
    @Column(name = "host", nullable = false)
    private String host;
    @NotNull
    @NotBlank
    @Column(name = "port", nullable = false)
    private String port;
    @NotNull
    @NotBlank
    @Column(name = "application", nullable = false, unique = true)
    private String application;
    @NotNull
    @NotBlank
    @Column(name = "facility", nullable = false)
    private String facility;

    @NotNull
    @NotBlank
    @Column(name = "owner", nullable = false)
    private String owner;

    @NotNull
    @NotBlank
    @Column(name = "companyOwner", nullable = false)
    private String companyOwner;

    @NotNull
    @Column(name = "share", nullable = false)
    private boolean share;

    @NotNull
    @Column(name = "charset", nullable = false)
    private String charset;


    public ConfigurationSUTEntity(ConfigurationSUTBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.host = builder.host;
        this.port = builder.port;
        this.application = builder.application;
        this.facility = builder.facility;
        this.owner = builder.owner;
        this.companyOwner = builder.companyOwner;
        this.share = builder.share;
        this.charset = builder.charset;

    }

    public ConfigurationSUTEntity() {
    }

    public static ConfigurationSUTBuilder builder() {
        return new ConfigurationSUTBuilder();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getHost() {
        return host;
    }

    public String getPort() {
        return port;
    }

    public String getApplication() {
        return application;
    }

    public String getFacility() {
        return facility;
    }

    public String getOwner() {
        return owner;
    }

    public String getCompanyOwner() {
        return companyOwner;
    }

    public Boolean isShare() {
        return share;
    }

    public String getCharset() {
        return charset;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConfigurationSUTEntity that = (ConfigurationSUTEntity) o;
        return id == that.id && Objects.equals(name, that.name) && Objects.equals(host, that.host) && Objects.equals(port, that.port) && Objects.equals(application, that.application) && Objects.equals(facility, that.facility) && Objects.equals(owner, that.owner) && Objects.equals(companyOwner, that.companyOwner) && Objects.equals(share, that.share) && Objects.equals(charset, that.charset) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, host, port, application, facility, owner, companyOwner, share, charset);
    }

    @Override
    public String toString() {


        return "ConfigurationSUTEntity{" + "id=" + id +
                ", name='" + name + '\'' +
                ", host='" + host + '\'' +
                ", port='" + port + '\'' +
                ", application='" + application + '\'' +
                ", facility='" + facility + '\'' +
                ", owner='" + owner + '\'' +
                ", companyOwner='" + companyOwner + '\'' +
                ", share='" + share + '\'' +
                ", charset='" + charset + '\'' +
                '}';
    }

    @Override
    public int compareTo(ConfigurationSUTEntity configurationSUT) {
        return host.compareTo(configurationSUT.getHost());
    }

    public static class ConfigurationSUTBuilder {
        private int id;
        private String name;
        private String host;
        private String port;
        private String application;
        private String facility;
        private String owner;
        private String companyOwner;
        private boolean share;
        private String charset;

        public ConfigurationSUTBuilder withId(int id) {
            this.id = id;
            return this;
        }

        public ConfigurationSUTBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public ConfigurationSUTBuilder withHost(String host) {
            this.host = host;
            return this;
        }

        public ConfigurationSUTBuilder withPort(String port) {
            this.port = port;
            return this;
        }

        public ConfigurationSUTBuilder withApplication(String application) {
            this.application = application;
            return this;
        }

        public ConfigurationSUTBuilder withFacility(String facility) {
            this.facility = facility;
            return this;
        }

        public ConfigurationSUTBuilder withOwner(String owner) {
            this.owner = owner;
            return this;
        }

        public ConfigurationSUTBuilder withCompanyOwner(String companyOwner) {
            this.companyOwner = companyOwner;
            return this;
        }

        public ConfigurationSUTBuilder withShare(boolean share) {
            this.share = share;
            return this;
        }

        public ConfigurationSUTBuilder withCharset(String charset) {
            this.charset = charset;
            return this;
        }



        public ConfigurationSUTEntity build() {
            return new ConfigurationSUTEntity(this);
        }
    }
}