package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.mapper;

import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.Preference;
import net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.entity.PreferenceEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 23/06/2023
 */
@Mapper
public interface PreferenceMapper {

    PreferenceMapper instance = Mappers.getMapper(PreferenceMapper.class);

    @Mapping(source = "id", target = "withId")
    @Mapping(source = "className", target = "withClassName")
    @Mapping(source = "description", target = "withDescription")
    @Mapping(source = "preferenceName", target = "withPreferenceName")
    @Mapping(source = "preferenceValue", target = "withPreferenceValue")
    Preference preferenceEntityToPreference(PreferenceEntity preferenceEntity);
    @Mapping(source = "id", target = "withId")
    @Mapping(source = "className", target = "withClassName")
    @Mapping(source = "description", target = "withDescription")
    @Mapping(source = "preferenceName", target = "withPreferenceName")
    @Mapping(source = "preferenceValue", target = "withPreferenceValue")
    PreferenceEntity preferenceToPreferenceEntity(Preference preferenceEntity);
    @Mapping(source = "id", target = "withId")
    @Mapping(source = "className", target = "withClassName")
    @Mapping(source = "description", target = "withDescription")
    @Mapping(source = "preferenceName", target = "withPreferenceName")
    @Mapping(source = "preferenceValue", target = "withPreferenceValue")
    List<Preference> preferenceEntitiesListTopreferenceList(List<PreferenceEntity> preferenceEntities);

    @Mapping(source = "id", target = "withId")
    @Mapping(source = "className", target = "withClassName")
    @Mapping(source = "description", target = "withDescription")
    @Mapping(source = "preferenceName", target = "withPreferenceName")
    @Mapping(source = "preferenceValue", target = "withPreferenceValue")
    List<PreferenceEntity> preferenceListToPreferenceEntitiesList(List<Preference> preferences);

}
