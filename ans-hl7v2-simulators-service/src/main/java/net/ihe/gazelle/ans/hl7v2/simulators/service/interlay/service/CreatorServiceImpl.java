package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.server.CreatorServer;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.CreatorService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.TemplateHl7v2;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.model.MessageData;

import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
@ApplicationScoped
public class CreatorServiceImpl implements CreatorService<MessageData> {

    @Inject
    CreatorServer<MessageData> creatorServer;

    @Override
    public void sendMessage(ConfigurationSUT configuration, TemplateHl7v2 templateHl7v2,ConfigurationSUT configurationSUTCreator) throws InterruptedException {
       creatorServer.sendMessage(configuration, templateHl7v2,configurationSUTCreator);
    }



    @Override
    public List getHistoryMessage() {
        return this.creatorServer.getMessageHistory();
    }

    public void resetCreatorServer(){
        creatorServer.resetCreatorServer();
    }

}
