package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.mapper;

import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.TemplateHl7v2;
import net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.entity.TemplateEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 22/07/2023
 */
@Mapper
public interface TemplateMapper {

    TemplateMapper instance = Mappers.getMapper(TemplateMapper.class);

    @Mapping(source = "id", target = "withId")
    @Mapping(source = "title", target = "withTitle")
    @Mapping(source = "description", target = "withDescription")
    @Mapping(source = "enable", target = "withEnabled")
    @Mapping(source = "fileName", target = "withFileName")
    @Mapping(source = "templateMustache", target = "withTemplateMustache")
    TemplateHl7v2 templateEntityToTemplate(TemplateEntity templateEntity);

    @Mapping(source = "id", target = "withId")
    @Mapping(source = "title", target = "withTitle")
    @Mapping(source = "description", target = "withDescription")
    @Mapping(source = "enable", target = "withEnabled")
    @Mapping(source = "fileName", target = "withFileName")
    @Mapping(source = "templateMustache", target = "withTemplateMustache")
    TemplateEntity templateToTemplateEntity(TemplateHl7v2 templateHl7v2);

    @Mapping(source = "id", target = "withId")
    @Mapping(source = "title", target = "withTitle")
    @Mapping(source = "description", target = "withDescription")
    @Mapping(source = "enable", target = "withEnabled")
    @Mapping(source = "fileName", target = "withFileName")
    @Mapping(source = "templateMustache", target = "withTemplateMustache")
    List<TemplateHl7v2> templateEntityListToTemplateList(List<TemplateEntity> templateEntities);

    @Mapping(source = "id", target = "withId")
    @Mapping(source = "title", target = "withTitle")
    @Mapping(source = "description", target = "withDescription")
    @Mapping(source = "enable", target = "withEnabled")
    @Mapping(source = "fileName", target = "withFileName")
    @Mapping(source = "templateMustache", target = "withTemplateMustache")
    List<TemplateEntity> templateListToTemplateEntityList(List<TemplateHl7v2> templateHl7v2List);


}
