package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.transaction.Transactional;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.dao.PreferenceDao;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.Preference;
import net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.entity.PreferenceEntity;
import net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.mapper.PreferenceMapper;

import java.util.List;

import static org.hibernate.internal.util.StringHelper.isBlank;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
@Transactional
@ApplicationScoped
public class PreferenceDaoImpl implements PreferenceDao {

    @Inject
    EntityManager entityManager;
    PreferenceMapper mapper = PreferenceMapper.instance;

    @Override
    @Transactional
    public void insert(Preference preference) {
        entityManager.persist(mapper.preferenceToPreferenceEntity(preference));
    }

    @Override
    public List<Preference> getAll() {
        return mapper.preferenceEntitiesListTopreferenceList( entityManager.createNamedQuery("PreferenceEntity.findAll").getResultList());
    }

    @Override
    public Preference getOne(int id) {
        return mapper.preferenceEntityToPreference(entityManager.find(PreferenceEntity.class, id));
    }

    @Transactional
    @Override
    public void update(Preference preference) {
        Preference origin = getOne(preference.getId());
        String className = preference.getClassName() != null && !isBlank(preference.getClassName())
                ? preference.getClassName()
                : origin.getClassName();
        String description = preference.getDescription() != null && !isBlank(preference.getDescription())
                ? preference.getDescription()
                : origin.getDescription();
        String preferenceName = preference.getPreferenceName() != null && !isBlank(preference.getPreferenceName())
                ? preference.getPreferenceName()
                : origin.getPreferenceName();
        String preferenceValue = preference.getPreferenceValue() != null && !isBlank(preference.getPreferenceValue())
                ? preference.getPreferenceValue()
                : origin.getPreferenceValue();

        entityManager.createNamedQuery("PreferenceEntity.updateOne")
                .setParameter("id", preference.getId())
                .setParameter("className", className)
                .setParameter("description", description)
                .setParameter("preferenceName", preferenceName)
                .setParameter("preferenceValue", preferenceValue)
                .executeUpdate();
    }

    @Override
    public boolean isPreferenceByNameExist(String name) {
        return getByName(name) != null;
    }

    @Override
    public Preference getByName(String name) {
        try {
            return mapper.preferenceEntityToPreference((PreferenceEntity) entityManager.createNamedQuery("PreferenceEntity.findByName").setParameter("preferenceName", name).getSingleResult());
        } catch (NoResultException e) {
            return null;
        }
    }

    @Transactional
    @Override
    public void delete(Preference preference) {
        entityManager.remove(entityManager.contains(mapper.preferenceToPreferenceEntity(preference))
                ? mapper.preferenceToPreferenceEntity(preference)
                : entityManager.merge(mapper.preferenceToPreferenceEntity(preference)));
    }
}