package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.service.validator;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public enum MessageEncoding {
	XML("XML"),
	ER7("ER7");
	
	String value;
	
	MessageEncoding(String value){
		this.value = value;
	}
	
	public String getValue(){
		return this.value;
	}
}
