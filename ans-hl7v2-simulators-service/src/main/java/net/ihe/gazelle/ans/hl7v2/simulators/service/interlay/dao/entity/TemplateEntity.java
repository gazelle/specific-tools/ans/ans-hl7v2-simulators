package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.entity;

import com.github.mustachejava.Mustache;
import jakarta.persistence.*;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 21/07/2023
 */
@Entity(name = "Template")
@Table(name = "cmn_application_template")
@NamedQuery(name = "Template.findAll", query = "FROM Template t")
@NamedQuery(name = "Template.findByTitle", query = "FROM Template t WHERE t.title=:title")
@NamedQuery(name = "Template.findByFileName", query = "FROM Template t WHERE t.fileName=:fileName")
@NamedQuery(name = "Template.findEnable", query = "FROM Template t WHERE t.enable = true")
@NamedQuery(name = "Template.deleteOne", query = "DELETE FROM Template t WHERE t.id = :id")
@NamedQuery(name = "Template.updateOne", query = "UPDATE Template t SET t.title=:title , t.description=:description, t.enable=:enable,t.fileName=:fileName WHERE t.id=:id")
public class TemplateEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -8436693543471296515L;
    @Id
    @SequenceGenerator(name = "templateSeqGen", sequenceName = "templateSeq", initialValue = 50, allocationSize = 100)
    @GeneratedValue(generator = "templateSeqGen")
    private int id;
    private boolean enable;
    private String title;
    private String description;
    @Column(name = "file_name", nullable = false, unique = true)
    private String fileName;
    private transient Mustache templateMustache;

    public TemplateEntity(TemplateEntityBuilder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.description = builder.description;
        this.enable = builder.enable;
        this.fileName = builder.fileName;
        this.templateMustache = builder.templateMustache;
    }

    public TemplateEntity() {
    }

    public static TemplateEntityBuilder builder() {
        return new TemplateEntityBuilder();
    }

    public static class TemplateEntityBuilder {
        private int id;
        private String title;
        private String description;
        private boolean enable;
        private String fileName;
        private Mustache templateMustache;

        public TemplateEntityBuilder withId(int id) {
            this.id = id;
            return this;
        }

        public TemplateEntityBuilder withTitle(String title) {
            this.title = title;
            return this;
        }

        public TemplateEntityBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public TemplateEntityBuilder withEnabled(boolean enable) {
            this.enable = enable;
            return this;
        }

        public TemplateEntityBuilder withFileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        public TemplateEntityBuilder withTemplateMustache(Mustache templateMustache) {
            this.templateMustache = templateMustache;
            return this;
        }

        public TemplateEntity build() {
            return new TemplateEntity(this);
        }

    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public boolean isEnable() {
        return enable;
    }

    public String getFileName() {
        return fileName;
    }

    public Mustache getTemplateMustache() {
        return templateMustache;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TemplateEntity that = (TemplateEntity) o;
        return id == that.id && enable == that.enable && Objects.equals(title, that.title) && Objects.equals(description, that.description) && Objects.equals(fileName, that.fileName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, enable, title, description, fileName);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TemplateEntity{");
        sb.append("id=").append(id);
        sb.append(", enable=").append(enable);
        sb.append(", title='").append(title).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", filePath='").append(fileName).append('\'');
        sb.append(", templateMustache=").append(templateMustache);
        sb.append('}');
        return sb.toString();
    }
}
