package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.service;

import ca.uhn.hl7v2.model.Message;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.server.CheckMessageService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.server.ManagerServer;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.ConfigurationService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.ManagerService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.TemplateHl7v2Service;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 03/07/2023
 */
@ApplicationScoped
public class ManagerServiceImpl implements ManagerService {

    @Inject
    ManagerServer managerServer;

    @Inject
    CheckMessageService<Message> checkMessageService;




    @Override
    public void switchServerStateMessages( ConfigurationService configurationService, TemplateHl7v2Service templateHl7v2Service)   {

        managerServer.switchServerStateMessages( configurationService, templateHl7v2Service,checkMessageService);
    }

    @Override
    public boolean isServerRunning() {
        return managerServer.isServerRunning();
    }

}
