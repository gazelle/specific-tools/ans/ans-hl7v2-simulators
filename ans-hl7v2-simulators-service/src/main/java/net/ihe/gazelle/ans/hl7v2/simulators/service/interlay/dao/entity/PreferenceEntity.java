package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
@Entity(name = "Preference")
@Table(name = "cmn_application_preference")
@NamedQuery(name = "PreferenceEntity.findAll", query = "FROM Preference p")
@NamedQuery(name = "PreferenceEntity.findByName", query = "FROM Preference p WHERE p.preferenceName =:preferenceName")
@NamedQuery(name = "PreferenceEntity.updateOne", query = "UPDATE  Preference p SET p.className = :className , p.description = :description, p.preferenceName=:preferenceName, p.preferenceValue =:preferenceValue WHERE p.id = :id")
public class PreferenceEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -6049998164654568432L;
    @Id
    @SequenceGenerator(name = "preferenceSeqGen", sequenceName = "preferenceSeq", initialValue = 50, allocationSize = 100)
    @GeneratedValue(generator = "preferenceSeqGen")
    private int id;
    @NotNull
    @NotBlank
    @Column(name = "class_name", nullable = false)
    private String className;
    private String description;
    @NotNull
    @NotBlank
    @Column(name = "preference_name", nullable = false, unique = true)
    private String preferenceName;
    @NotNull
    @NotBlank
    @Column(name = "preference_value", nullable = false)
    private String preferenceValue;

    public PreferenceEntity(PreferenceBuilder builder) {
        this.id = builder.id;
        this.className = builder.className;
        this.description = builder.description;
        this.preferenceName = builder.preferenceName;
        this.preferenceValue = builder.preferenceValue;
    }

    public PreferenceEntity() {
    }

    public static PreferenceBuilder builder() {
        return new PreferenceBuilder();
    }

    public static class PreferenceBuilder {
        private int id;
        private String className;
        private String description;
        private String preferenceName;
        private String preferenceValue;

        public PreferenceBuilder withId(int id) {
            this.id = id;
            return this;
        }

        public PreferenceBuilder withClassName(String className) {
            this.className = className;
            return this;
        }

        public PreferenceBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public PreferenceBuilder withPreferenceName(String preferenceName) {
            this.preferenceName = preferenceName;
            return this;
        }

        public PreferenceBuilder withPreferenceValue(String preferenceValue) {
            this.preferenceValue = preferenceValue;
            return this;
        }

        public PreferenceEntity build() {
            return new PreferenceEntity(this);
        }
    }

    public int getId() {
        return id;
    }

    public String getClassName() {
        return className;
    }

    public String getDescription() {
        return description;
    }

    public String getPreferenceName() {
        return preferenceName;
    }

    public String getPreferenceValue() {
        return preferenceValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PreferenceEntity that = (PreferenceEntity) o;
        return id == that.id && Objects.equals(className, that.className) && Objects.equals(description, that.description) && Objects.equals(preferenceName, that.preferenceName) && Objects.equals(preferenceValue, that.preferenceValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, className, description, preferenceName, preferenceValue);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PreferenceEntity{");
        sb.append("id=").append(id);
        sb.append(", className='").append(className).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", preferenceName='").append(preferenceName).append('\'');
        sb.append(", preferenceValue='").append(preferenceValue).append('\'');
        sb.append('}');
        return sb.toString();
    }
}