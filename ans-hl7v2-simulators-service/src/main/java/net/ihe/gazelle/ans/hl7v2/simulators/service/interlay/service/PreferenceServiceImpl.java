package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.dao.PreferenceDao;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.DuplicationException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.NotFoundException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.PreferenceService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.Preference;

import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 23/06/2023
 */
@ApplicationScoped
public class PreferenceServiceImpl implements PreferenceService {

    public static final String STRING = "String";
    public static final String LOCAL = "127.0.0.1";
    @Inject
    PreferenceDao preferenceDao;

    public PreferenceServiceImpl(PreferenceDao preferenceDao) {
        this();
        this.preferenceDao = preferenceDao;
    }

    public PreferenceServiceImpl() {
    }

    @Override
    public List<Preference> getAllPreference() {
        return this.preferenceDao.getAll();
    }

    @Override
    public void insertPreference(Preference preference) {
        if (!isPreferenceByNameExist(preference.getPreferenceName())) {
            this.preferenceDao.insert(preference);
        } else throw new DuplicationException();

    }

    @Override
    public Preference getOnePreference(int id) {
        return this.preferenceDao.getOne(id);
    }

    @Override
    public void updatePreference(Preference preference) {
        Preference oldPreference = getOnePreference(preference.getId());

        if (oldPreference != null) {
            if (oldPreference.getPreferenceName().equals(preference.getPreferenceName())
                    || !isPreferenceByNameExist(preference.getPreferenceName())) {
                this.preferenceDao.update(preference);
            } else throw new DuplicationException();
        } else throw new NotFoundException();

    }

    @Override
    public void deletePreference(Preference preference) {
        if (getOnePreference(preference.getId()) != null)
            this.preferenceDao.delete(preference);
        else throw new NotFoundException();
    }

    @Override
    public boolean isPreferenceByNameExist(String name) {
        return this.preferenceDao.isPreferenceByNameExist(name);
    }

    @Override
    public Preference getPreferenceByName(String name) {
            return this.preferenceDao.getByName(name);
    }



   @Override
   public String getLocalCreatorPort() {
        Preference preferencePort;
        preferencePort = this.getPreferenceByName("Creator_Port");
        if (preferencePort==null ){
            preferencePort = Preference.builder().withClassName(STRING).withPreferenceName("Creator_Port").withPreferenceValue("5456").build();
            this.insertPreference(preferencePort);
        }
        return preferencePort.getPreferenceValue();
    }

    @Override
    public String getLocalCreatorApplication() {
        Preference preferencePort;
        preferencePort = this.getPreferenceByName("Creator_Application");
        if (preferencePort==null ){
            preferencePort = Preference.builder().withClassName(STRING).withPreferenceName("Creator_Application").withPreferenceValue("ans-simulators-creator").build();
            this.insertPreference(preferencePort);
        }
        return preferencePort.getPreferenceValue();
    }

    @Override
    public String getLocalCreatorFacility() {
        Preference preferencePort;
        preferencePort = this.getPreferenceByName("Creator_Facility");
        if (preferencePort==null ){
            preferencePort = Preference.builder().withClassName(STRING).withPreferenceName("Creator_Facility").withPreferenceValue("ANS").build();
            this.insertPreference(preferencePort);
        }
        return preferencePort.getPreferenceValue();
    }

    @Override
    public String getLocalCreatorHost() {
        Preference preferenceHost = this.getPreferenceByName("Creator_Host");
        if (preferenceHost == null) {

                preferenceHost = Preference.builder().withClassName(STRING).withPreferenceName("Creator_Host").withPreferenceValue(String.valueOf(LOCAL)).build();

            this.insertPreference(preferenceHost);
        }
        return preferenceHost.getPreferenceValue();

    }

    @Override
    public String getLocalUTF8ManagerPort() {
        Preference preferencePort = this.getPreferenceByName("UTF8_Manager_Port");
        if (preferencePort == null) {
            preferencePort = Preference.builder().withClassName(STRING).withPreferenceName("UTF8_Manager_Port").withPreferenceValue("5453").build();
            this.insertPreference(preferencePort);
        }
        return preferencePort.getPreferenceValue();
    }


    @Override
    public String getLocalUTF8ManagerHost() {
        Preference preferenceHost = this.getPreferenceByName("UTF8_Manager_Host");
        if (preferenceHost == null) {
            preferenceHost = Preference.builder().withClassName(STRING).withPreferenceName("UTF8_Manager_Host").withPreferenceValue(String.valueOf(LOCAL)).build();
            this.insertPreference(preferenceHost);
        }
        return preferenceHost.getPreferenceValue();

    }

    @Override
    public String getLocalUTF8ManagerApplication() {
        Preference preferenceReceivingApp = this.getPreferenceByName("UTF8_Manager_Application");
        if (preferenceReceivingApp == null) {
            preferenceReceivingApp = Preference.builder().withClassName(STRING).withPreferenceName("UTF8_Manager_Application").withPreferenceValue("ans-simulators-utf8-manager").build();
            this.insertPreference(preferenceReceivingApp);
        }
        return preferenceReceivingApp.getPreferenceValue();
    }

    @Override
    public String getLocalUTF8ManagerFacility() {
        Preference preferenceReceivingFacility = this.getPreferenceByName("UTF8_Manager_Facility");
        if (preferenceReceivingFacility == null) {
            preferenceReceivingFacility = Preference.builder().withClassName(STRING).withPreferenceName("UTF8_Manager_Facility").withPreferenceValue("ANS").build();
            this.insertPreference(preferenceReceivingFacility);
        }
        return preferenceReceivingFacility.getPreferenceValue();
    }



    @Override
    public String getLocalI885915ManagerPort() {
        Preference preferencePort = this.getPreferenceByName("I885915_Manager_Port");
        if (preferencePort == null) {
            preferencePort = Preference.builder().withClassName(STRING).withPreferenceName("I885915_Manager_Port").withPreferenceValue("5455").build();
            this.insertPreference(preferencePort);
        }
        return preferencePort.getPreferenceValue();
    }


    @Override
    public String getLocalI885915ManagerHost() {
        Preference preferenceHost = this.getPreferenceByName("I885915_Manager_Host");
        if (preferenceHost == null) {
            preferenceHost = Preference.builder().withClassName(STRING).withPreferenceName("I885915_Manager_Host").withPreferenceValue(LOCAL).build();
            this.insertPreference(preferenceHost);
        }
        return preferenceHost.getPreferenceValue();

    }

    @Override
    public String getLocalI885915ManagerApplication() {
        Preference preferenceReceivingApp = this.getPreferenceByName("I885915_Manager_Application");
        if (preferenceReceivingApp == null) {
            preferenceReceivingApp = Preference.builder().withClassName(STRING).withPreferenceName("I885915_Manager_Application").withPreferenceValue("ans-simulators-i885915-manager").build();
            this.insertPreference(preferenceReceivingApp);
        }
        return preferenceReceivingApp.getPreferenceValue();
    }

    @Override
    public String getLocalI885915ManagerFacility() {
        Preference preferenceReceivingFacility = this.getPreferenceByName("I885915_Manager_Facility");
        if (preferenceReceivingFacility == null) {
            preferenceReceivingFacility = Preference.builder().withClassName(STRING).withPreferenceName("I885915_Manager_Facility").withPreferenceValue("ANS").build();
            this.insertPreference(preferenceReceivingFacility);
        }
        return preferenceReceivingFacility.getPreferenceValue();
    }

}
