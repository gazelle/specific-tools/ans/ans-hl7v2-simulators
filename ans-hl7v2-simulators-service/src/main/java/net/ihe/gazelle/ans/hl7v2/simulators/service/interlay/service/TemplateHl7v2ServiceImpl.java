package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.service;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.dao.TemplateDao;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.CustomHl7Exception;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.DuplicationException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.NotFoundException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.TemplateHl7v2Service;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.TemplateHl7v2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
@ApplicationScoped
public class TemplateHl7v2ServiceImpl implements TemplateHl7v2Service {

    @Inject
    TemplateDao dao;
    private static final String CANNOT_COMPILE = "Can't compile mustache template!";

    private static final String ZAM_FOLDER = "/opt/ans-hl7v2-simulators/templates/ZAM/";

    public TemplateHl7v2ServiceImpl(TemplateDao dao) {
        this();
        this.dao = dao;
    }

    public TemplateHl7v2ServiceImpl() {
    }

    @Override
    public TemplateHl7v2 getZAMZ01RCVOK() {
        File file = new File(ZAM_FOLDER + "ZAM_Z01_RCV_OK.mustache");
        MustacheFactory mustacheFactory = new DefaultMustacheFactory();
        Mustache mustache;
        try {
            mustache = mustacheFactory.compile(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8), file.getName());
        } catch (FileNotFoundException e) {
            throw new CustomHl7Exception(CANNOT_COMPILE, e);
        }
        return TemplateHl7v2.builder()
                .withTitle("ZAM_Z01_RCV_OK")
                .withDescription("ZAM_Z01_RCV_OK")
                .withTemplateMustache(mustache)
                .build();
    }



    @Override
    public TemplateHl7v2 getZAMZ02RCVOK() {
        File file = new File(ZAM_FOLDER + "ZAM_Z02_RCV_OK.mustache");
        MustacheFactory mustacheFactory = new DefaultMustacheFactory();
        Mustache mustache;
        try {
            mustache = mustacheFactory.compile(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8), file.getName());
        } catch (FileNotFoundException e) {
            throw new CustomHl7Exception(CANNOT_COMPILE, e);
        }
        return TemplateHl7v2.builder()
                .withTitle("ZAM_Z02_RCV_OK")
                .withDescription("ZAM_Z02_RCV_OK")
                .withTemplateMustache(mustache)
                .build();
    }

    @Override
    public TemplateHl7v2 getZAMZ03LCTOK() {
        File file = new File(ZAM_FOLDER + "ZAM_Z03_LCT_OK.mustache");
        MustacheFactory mustacheFactory = new DefaultMustacheFactory();
        Mustache mustache;
        try {
            mustache = mustacheFactory.compile(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8), file.getName());
        } catch (FileNotFoundException e) {
            throw new CustomHl7Exception(CANNOT_COMPILE, e);
        }
        return TemplateHl7v2.builder()
                .withTitle("ZAM_Z03_LCT_OK")
                .withDescription("ZAM_Z03_LCT_OK")
                .withTemplateMustache(mustache)
                .build();
    }

    @Override
    public TemplateHl7v2 getZAMZ03RCVKO() {
        File file = new File(ZAM_FOLDER + "ZAM_Z03_RCV_KO.mustache");
        MustacheFactory mustacheFactory = new DefaultMustacheFactory();
        Mustache mustache;
        try {
            mustache = mustacheFactory.compile(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8), file.getName());
        } catch (FileNotFoundException e) {
            throw new CustomHl7Exception(CANNOT_COMPILE, e);
        }
        return TemplateHl7v2.builder()
                .withTitle("ZAM_Z03_RCV_KO")
                .withDescription("ZAM_Z03_RCV_KO")
                .withTemplateMustache(mustache)
                .build();
    }

    public boolean isZAMThere() {
        try {
            getZAMZ01RCVOK();
            getZAMZ02RCVOK();
            getZAMZ03LCTOK();
            getZAMZ03RCVKO();
        } catch (Exception e) {
            return false;
        }
        return true;

    }


    @Override
    public List<TemplateHl7v2> getAllTemplatesCDA() {
        return this.dao.getAll();
    }

    @Override
    public List<TemplateHl7v2> getEnableTemplates() {
        return this.dao.getEnables();
    }

    @Override
    public TemplateHl7v2 getTemplateByTitle(String title) {
        return this.dao.getByTitle(title);
    }

    @Override
    public TemplateHl7v2 getTemplateByFileName(String name) {
        return this.dao.getByFileName(name);
    }

    @Override
    public TemplateHl7v2 getOneTemplate(int id) {
        return this.dao.getOne(id);
    }

    @Override
    public void createTemplate(TemplateHl7v2 templateHl7v2) {
        if (istemplatetitleexist(templateHl7v2.getTitle()) && istemplatefilenameexist(templateHl7v2.getFileName())) {
            this.dao.insert(templateHl7v2);
        } else throw new DuplicationException();

    }

    @Override
    public void updateTemplate(TemplateHl7v2 template) {
        TemplateHl7v2 origin = getOneTemplate(template.getId());

        if (origin.getTitle() != null) {
            if (((template.getTitle().equals(origin.getTitle()) || istemplatetitleexist(template.getTitle()))
                    && (template.getFileName().equals(origin.getFileName()) || istemplatefilenameexist(template.getFileName())))) {
                this.dao.update(template);
            } else throw new DuplicationException();
        } else throw new NotFoundException();

    }

    @Override
    public void deleteTemplate(TemplateHl7v2 templateHl7v2) {
        if (getOneTemplate(templateHl7v2.getId()) != null) {
            this.dao.delete(templateHl7v2);
        } else throw new NotFoundException();
    }

    @Override
    public boolean istemplatetitleexist(String title) {
        return !this.dao.isTemplateTitleExist(title);
    }

    @Override
    public boolean istemplatefilenameexist(String fileName) {
        return !this.dao.isFileNameExist(fileName);
    }


}
