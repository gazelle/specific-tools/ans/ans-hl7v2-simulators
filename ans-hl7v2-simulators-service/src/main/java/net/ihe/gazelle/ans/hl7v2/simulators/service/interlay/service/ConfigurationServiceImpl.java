package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.dao.ConfigurationSUTDao;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.DuplicationException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.NotFoundException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.ConfigurationService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.PreferenceService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;

import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 22/06/2023
 */
@ApplicationScoped
public class ConfigurationServiceImpl implements ConfigurationService {
    public static final String ANS_HL_7_V_2_SIMULATOR = "ANS-HL7V2-SIMULATOR";
    @Inject
    ConfigurationSUTDao configurationDAO;
    @Inject
    PreferenceService preferenceService;

    public ConfigurationServiceImpl(ConfigurationSUTDao configurationDAO,PreferenceService preferenceService) {
        this();
        this.configurationDAO = configurationDAO;
        this.preferenceService =preferenceService;
    }

    public ConfigurationServiceImpl(ConfigurationSUTDao configurationDAO) {
        this();
        this.configurationDAO = configurationDAO;
    }

    public ConfigurationServiceImpl() {
    }

    @Override
    public List<ConfigurationSUT> getAllConfiguration() {
        return this.configurationDAO.getAll();
    }

    @Override
    public List<ConfigurationSUT> getAvailableConfigurations(String owner ,String companyOwner) {
        return this.configurationDAO.getAvailableConfigurations(owner,companyOwner);
    }


    @Override
    public void insertConfiguration(ConfigurationSUT configuration) {
        if (isApplicationExisting(configuration.getApplication())) {
            this.configurationDAO.insert(configuration);
        } else throw new DuplicationException();
    }

    @Override
    public ConfigurationSUT getOneConfiguration(int id) {
        return this.configurationDAO.getOne(id);
    }

    @Override
    public ConfigurationSUT findSupplier(String applicationSupplier, String facilitySupplier) {
        ConfigurationSUT foundConfiguration = this.configurationDAO.findSupplier(applicationSupplier, facilitySupplier);
        if (foundConfiguration != null) return foundConfiguration;
        else throw new NotFoundException();
    }

    @Override
    public void updateConfiguration(ConfigurationSUT configurationSUT) {
        ConfigurationSUT oldConfigurationSUT = getOneConfiguration(configurationSUT.getId());

        if (oldConfigurationSUT != null) {
            if (oldConfigurationSUT.getApplication().equals(configurationSUT.getApplication())
                    || isApplicationExisting(configurationSUT.getApplication())) {
                this.configurationDAO.update(configurationSUT);
            } else throw new DuplicationException("Configuration already exist!");
        } else throw new NotFoundException("No configuration found!");

    }

    @Override
    public void deleteConfiguration(ConfigurationSUT configurationSUT) {
        if (getOneConfiguration(configurationSUT.getId()) != null) {
            this.configurationDAO.delete(configurationSUT);
        } else throw new NotFoundException("No configuration found!");
    }

    @Override
    public boolean isApplicationExisting(String application) {
        return this.configurationDAO.findApplication(application) == null;
    }

    @Override
    public ConfigurationSUT getConfigurationFromApplication(String application) {
        ConfigurationSUT configuration = this.configurationDAO.findApplication(application);
        if (configuration == null) throw new NotFoundException();
        return configuration;
    }


    @Override
    public ConfigurationSUT getLocalCreatorConfiguration() {

        return ConfigurationSUT.builder().withApplication(preferenceService.getLocalCreatorApplication()).withFacility(preferenceService.getLocalCreatorFacility()).withPort(preferenceService.getLocalCreatorPort()).withHost(preferenceService.getLocalCreatorHost()).withName("Local Creator Configuration").withOwner(ANS_HL_7_V_2_SIMULATOR).withCompanyOwner(ANS_HL_7_V_2_SIMULATOR).withShare(true).withCharset("")
                .build();

    }



    @Override
    public ConfigurationSUT getLocalUTF8ManagerConfiguration() {
        return ConfigurationSUT.builder().withApplication(preferenceService.getLocalUTF8ManagerApplication()).withFacility(preferenceService.getLocalUTF8ManagerFacility()).withPort(preferenceService.getLocalUTF8ManagerPort()).withHost(preferenceService.getLocalUTF8ManagerHost()).withName("Local UTF-8 Manager Configuration").withOwner(ANS_HL_7_V_2_SIMULATOR).withCompanyOwner(ANS_HL_7_V_2_SIMULATOR).withShare(true).withCharset("UNICODE UTF-8").build();

    }





    @Override
    public ConfigurationSUT getLocalI885915ManagerConfiguration() {
        return ConfigurationSUT.builder().withApplication(preferenceService.getLocalI885915ManagerApplication()).withFacility(preferenceService.getLocalI885915ManagerFacility()).withPort(preferenceService.getLocalI885915ManagerPort()).withHost(preferenceService.getLocalI885915ManagerHost()).withName("Local 8859/15 Manager Configuration").withOwner(ANS_HL_7_V_2_SIMULATOR).withCompanyOwner(ANS_HL_7_V_2_SIMULATOR).withShare(true).withCharset("8859/15").build();

    }



}