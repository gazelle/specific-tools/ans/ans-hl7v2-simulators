package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.utils;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 24/07/2023
 */
@Produces(MediaType.APPLICATION_JSON)
@Path("/")
public class HealthCheckEndpoint {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("health")
    public Response getHealth() {
        return Response.ok().build();
    }

}
