package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.service;

import jakarta.enterprise.context.ApplicationScoped;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.AccesDeniedException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.NotFoundException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.SaveFileException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.FileCRUDService;
import org.apache.commons.io.IOUtils;
import org.primefaces.model.file.UploadedFile;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
@ApplicationScoped
public class FileCRUDServiceImpl implements FileCRUDService<UploadedFile> {

    private static final String DESTINATION = "/opt/ans-hl7v2-simulators/templates/CDA/";


    @Override
    public void upload(UploadedFile file) {
        try {
            Path path = Path.of(DESTINATION);

            File directory = new File(DESTINATION);
            if (!directory.exists() && (!directory.mkdirs()))
                throw new AccesDeniedException();
            if (!Files.isWritable(path)) {
                throw new AccesDeniedException();
            } else {
                InputStream in = file.getInputStream();
                copyFile(file.getFileName(), in, path);
            }
        } catch (IOException e) {
            throw new SaveFileException();
        }

    }

    private void copyFile(String fileName, InputStream in, Path path) {
        try {
            // write the inputStream to a FileOutputStream
            try (OutputStream out = Files.newOutputStream(Path.of(path + "/" + fileName))) {
                int read = 0;
                byte[] bytes = new byte[1024];
                while ((read = in.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                in.close();
            }
        } catch (IOException e) {
            throw new SaveFileException();
        }
    }

    @Override
    public void delete(String fileName) {
        try {
            Files.delete(Path.of(DESTINATION + fileName));
        } catch (IOException e) {
            throw new NotFoundException();
        }
    }

    @Override
    public String read(String fileName) {
        try {
            String filePath = Path.of(DESTINATION + fileName).toString();
            FileInputStream fileContentStream = new FileInputStream(filePath);
            return IOUtils.toString(fileContentStream, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new NotFoundException();
        }
    }
}
