package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.service;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v26.segment.MSH;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.server.CheckMessageService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.PreferenceService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.utils.MessageUtils;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.utils.MessageUtilsImpl;
import net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.service.validator.HL7v2Validator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
@ApplicationScoped
public class CheckMessageServiceImpl implements CheckMessageService<Message> {


    MessageUtils<Message, MSH> utils = new MessageUtilsImpl();

    @Inject
    PreferenceService preferenceService;



    @Override
    public boolean checkMessage(Message message) throws HL7Exception {
        String profilOID = getProfilOIDFromMessage(message);
        if (profilOID.isEmpty()) return false;

        String endpoint = preferenceService.getPreferenceByName("Endpoint_Validator").getPreferenceValue();
        HL7v2Validator validator = new HL7v2Validator(endpoint);
        String validationSummary = validator.validate(message.encode(), profilOID, message.getEncodingCharactersValue());
        String validationResult = getValidationResult(validationSummary);
        return validationResult.equals("PASSED");
    }


    public String getProfilOIDFromMessage(Message message) {
        String typeMSG = utils.getTypeMessage(message);
        if (typeMSG.equals("ORU^R01^ORU_R01")) return "1.3.6.1.4.1.12559.11.36.8.3.19";
        if (utils.getTransaction(message).contains("CISIS_CDA_HL7_LPS")) {
            if (typeMSG.equals("MDM^T02^MDM_T02")) return "1.3.6.1.4.1.12559.11.36.8.3.20";
            if (typeMSG.equals("MDM^T04^MDM_T02")) return "1.3.6.1.4.1.12559.11.36.8.3.22";
            if (typeMSG.equals("MDM^T10^MDM_T02")) return "1.3.6.1.4.1.12559.11.36.8.3.23";
        }
        if (utils.getTransaction(message).contains("CISIS_CDA_HL7_V2")) {
            if (typeMSG.equals("MDM^T02^MDM_T02")) return "1.3.6.1.4.1.12559.11.36.8.3.24";
            if (typeMSG.equals("MDM^T04^MDM_T02")) return "1.3.6.1.4.1.12559.11.36.8.3.25";
            if (typeMSG.equals("MDM^T10^MDM_T02")) return "1.3.6.1.4.1.12559.11.36.8.3.21";
        }
        return "";
    }

    public String getValidationResult(String validationSummary) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newDefaultInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            StringReader stringReader = new StringReader(validationSummary);
            org.xml.sax.InputSource inputSource = new org.xml.sax.InputSource(stringReader);

            // Parse the XML document
            Document document = builder.parse(inputSource);
            // Find the ValidationTestResult element
            NodeList validationTestResultList = document.getElementsByTagName("ValidationTestResult");
            if (validationTestResultList.getLength() > 0) {
                Element validationTestResultElement = (Element) validationTestResultList.item(0);
                return validationTestResultElement.getTextContent();
            }
            return "";
        } catch (Exception e) {
            return "Error parsing";

        }
    }


}