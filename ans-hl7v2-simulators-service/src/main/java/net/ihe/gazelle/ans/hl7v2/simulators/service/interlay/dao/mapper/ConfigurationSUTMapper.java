package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.mapper;

import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;
import net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.entity.ConfigurationSUTEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project GazelleTokenService
 * @date 14/06/2022
 */
@Mapper
public interface ConfigurationSUTMapper {

    ConfigurationSUTMapper instance = Mappers.getMapper(ConfigurationSUTMapper.class);

    @Mapping(source = "id", target = "withId")
    @Mapping(source = "name", target = "withName")
    @Mapping(source = "host", target = "withHost")
    @Mapping(source = "port", target = "withPort")
    @Mapping(source = "application", target = "withApplication")
    @Mapping(source = "facility", target = "withFacility")
    @Mapping(source = "owner", target = "withOwner")
    @Mapping(source = "companyOwner", target = "withCompanyOwner")
    @Mapping(source = "share", target = "withShare")
    @Mapping(source = "charset", target = "withCharset")


    ConfigurationSUT sutEntityToSut(ConfigurationSUTEntity configurationSUTEntity);

    @Mapping(source = "id", target = "withId")
    @Mapping(source = "name", target = "withName")
    @Mapping(source = "host", target = "withHost")
    @Mapping(source = "port", target = "withPort")
    @Mapping(source = "application", target = "withApplication")
    @Mapping(source = "facility", target = "withFacility")
    @Mapping(source = "owner", target = "withOwner")
    @Mapping(source = "companyOwner", target = "withCompanyOwner")
    @Mapping(source = "share", target = "withShare")
    @Mapping(source = "charset", target = "withCharset")

    ConfigurationSUTEntity sutToSutEntity(ConfigurationSUT configurationSUT);

    @Mapping(source = "id", target = "withId")
    @Mapping(source = "name", target = "withName")
    @Mapping(source = "host", target = "withHost")
    @Mapping(source = "port", target = "withPort")
    @Mapping(source = "application", target = "withApplication")
    @Mapping(source = "facility", target = "withFacility")
    @Mapping(source = "owner", target = "withOwner")
    @Mapping(source = "companyOwner", target = "withCompanyOwner")
    @Mapping(source = "share", target = "withShare")
    @Mapping(source = "charset", target = "withCharset")

    List<ConfigurationSUT> sutEntitiesListToSutList(List<ConfigurationSUTEntity> configurationSUTEntities);


    @Mapping(source = "id", target = "withId")
    @Mapping(source = "name", target = "withName")
    @Mapping(source = "host", target = "withHost")
    @Mapping(source = "port", target = "withPort")
    @Mapping(source = "application", target = "withApplication")
    @Mapping(source = "facility", target = "withFacility")
    @Mapping(source = "owner", target = "withOwner")
    @Mapping(source = "companyOwner", target = "withCompanyOwner")
    @Mapping(source = "share", target = "withShare")
    @Mapping(source = "charset", target = "withCharset")

    List<ConfigurationSUTEntity> sutListToSutEntitiesList(List<ConfigurationSUT> configurationSUTList);


}
