package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.transaction.Transactional;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.dao.TemplateDao;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.TemplateHl7v2;
import net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.entity.TemplateEntity;
import net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.mapper.TemplateMapper;

import java.util.List;

import static org.hibernate.internal.util.StringHelper.isBlank;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 21/07/2023
 */
@Transactional
@ApplicationScoped
public class TemplateDaoImpl implements TemplateDao {

    @Inject
    EntityManager entityManager;

    TemplateMapper mapper = TemplateMapper.instance;

    @Override
    @Transactional
    public void insert(TemplateHl7v2 template) {
        entityManager.persist(mapper.templateToTemplateEntity(template));
    }

    @Override
    public List<TemplateHl7v2> getAll() {
        return mapper.templateEntityListToTemplateList(entityManager.createNamedQuery("Template.findAll").getResultList());
    }

    @Override
    public List<TemplateHl7v2> getEnables() {
        return mapper.templateEntityListToTemplateList(entityManager.createNamedQuery("Template.findEnable").getResultList());
    }

    @Override
    public TemplateHl7v2 getOne(int id) {
        return mapper.templateEntityToTemplate(entityManager.find(TemplateEntity.class, id));
    }

    @Transactional
    @Override
    public void update(TemplateHl7v2 templateHl7v2) {
        TemplateHl7v2 origin = getOne(templateHl7v2.getId());
        String title = templateHl7v2.getTitle() != null && !isBlank(templateHl7v2.getTitle())
                ? templateHl7v2.getTitle()
                : origin.getTitle();
        String description = templateHl7v2.getDescription() != null && !isBlank(templateHl7v2.getDescription())
                ? templateHl7v2.getDescription()
                : origin.getDescription();

        String fileName = templateHl7v2.getFileName() != null && !isBlank(templateHl7v2.getFileName())
                ? templateHl7v2.getFileName()
                : origin.getFileName();
        entityManager.createNamedQuery("Template.updateOne")
                .setParameter("id", templateHl7v2.getId())
                .setParameter("title", title)
                .setParameter("description", description)
                .setParameter("enable", templateHl7v2.isEnable())
                .setParameter("fileName", fileName)
                .executeUpdate();
    }

    @Override
    public boolean isTemplateTitleExist(String title) {
        return getByTitle(title) != null;
    }

    @Override
    public TemplateHl7v2 getByTitle(String title) {
        try {
            return mapper.templateEntityToTemplate((TemplateEntity) entityManager.createNamedQuery("Template.findByTitle").setParameter("title", title).getSingleResult());
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public TemplateHl7v2 getByFileName(String fileName) {
        try {
            return mapper.templateEntityToTemplate((TemplateEntity) entityManager.createNamedQuery("Template.findByFileName").setParameter("fileName", fileName).getSingleResult());
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public boolean isFileNameExist(String fileName) {
        return getByFileName(fileName) != null;
    }

    @Transactional
    @Override
    public void delete(TemplateHl7v2 templateHl7v2) {
        entityManager.remove(entityManager.contains(mapper.templateToTemplateEntity(templateHl7v2))
                ? mapper.templateToTemplateEntity(templateHl7v2)
                : entityManager.merge(mapper.templateToTemplateEntity(templateHl7v2)));
    }

}
