package net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.dao.ConfigurationSUTDao;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;
import net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.entity.ConfigurationSUTEntity;
import net.ihe.gazelle.ans.hl7v2.simulators.service.interlay.dao.mapper.ConfigurationSUTMapper;

import java.util.List;

import static org.hibernate.internal.util.StringHelper.isBlank;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
@Transactional
@ApplicationScoped
public class ConfigurationSUTDaoImpl implements ConfigurationSUTDao {

    @Inject
    EntityManager entityManager;
    ConfigurationSUTMapper mapper = ConfigurationSUTMapper.instance;

    private static final String APPLICATION = "application";

    @Override
    @Transactional
    public void insert(ConfigurationSUT configuration) {
        entityManager.persist(mapper.sutToSutEntity(configuration));
    }

    @Override
    @Transactional
    public List<ConfigurationSUT> getAll() {
        List<ConfigurationSUTEntity> list = entityManager.createNamedQuery("ConfigurationSUTEntity.findAll").getResultList();
        return mapper.sutEntitiesListToSutList(list);
    }

    @Override
    @Transactional
    public List<ConfigurationSUT> getAvailableConfigurations(String owner, String companyOwner) {
        List<ConfigurationSUTEntity> list = entityManager.createNamedQuery("ConfigurationSUTEntity.findAvailableConfigurations").setParameter("companyOwner", companyOwner).getResultList();
        return mapper.sutEntitiesListToSutList(list);
    }

    @Override
    public ConfigurationSUT getOne(int id) {
        return mapper.sutEntityToSut(entityManager.find(ConfigurationSUTEntity.class, id));
    }

    @Override
    @Transactional
    public ConfigurationSUT findSupplier(String application, String facility) {
        try {
            return mapper.sutEntityToSut((ConfigurationSUTEntity) entityManager
                    .createNamedQuery("ConfigurationSUTEntity.findSupplier")
                    .setParameter(APPLICATION, application)
                    .setParameter("facility", facility).getSingleResult());
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public ConfigurationSUT findApplication(String application) {
        ConfigurationSUT configurationSUT;
        try {
            configurationSUT = mapper.sutEntityToSut((ConfigurationSUTEntity) entityManager.createNamedQuery("ConfigurationSUTEntity.findApplication")
                    .setParameter(APPLICATION, application)
                    .getSingleResult());
        } catch (Exception e) {
            return null;
        }
        return configurationSUT;
    }

    @Transactional
    @Override
    public void update(ConfigurationSUT configuration) {
        ConfigurationSUT origin = getOne(configuration.getId());
        String port = !isBlank(configuration.getPort())
                ? configuration.getPort()
                : origin.getPort();
        String name = !isBlank(configuration.getName())
                ? configuration.getName()
                : origin.getName();
        String host = !isBlank(configuration.getHost())
                ? configuration.getHost()
                : origin.getHost();
        String application = !isBlank(configuration.getApplication())
                ? configuration.getApplication()
                : origin.getApplication();
        String facility = !isBlank(configuration.getFacility())
                ? configuration.getFacility()
                : origin.getFacility();
        String owner = !isBlank(configuration.getOwner())
                ? configuration.getOwner()
                : origin.getOwner();
        String companyOwner = !isBlank(configuration.getCompanyOwner())
                ? configuration.getCompanyOwner()
                : origin.getCompanyOwner();
        String charset = !isBlank(configuration.getCharset())
                ? configuration.getCharset()
                : origin.getCharset();


        entityManager.createNamedQuery("ConfigurationSUTEntity.updateOne")
                .setParameter("id", configuration.getId())
                .setParameter("port", port)
                .setParameter("host", host)
                .setParameter("name", name)
                .setParameter(APPLICATION, application)
                .setParameter("facility", facility)
                .setParameter("owner", owner)
                .setParameter("companyOwner", companyOwner)
                .setParameter("share", configuration.isShare())
                .setParameter("charset", charset)
                .executeUpdate();
    }

    @Transactional
    @Override
    public void delete(ConfigurationSUT configuration) {
        entityManager.remove(entityManager.contains(mapper.sutToSutEntity(configuration))
                ? mapper.sutToSutEntity(configuration)
                : entityManager.merge(mapper.sutToSutEntity(configuration)));
    }
}