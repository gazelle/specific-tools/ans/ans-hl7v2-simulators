create sequence configurationSeq start with 50 increment by 100;

create sequence preferenceSeq start with 50 increment by 100;

create sequence templateSeq start with 50 increment by 100;

create table cmn_application_configuration_sut
(
    id          integer      not null,
    application varchar(255) not null unique,
    facility    varchar(255) not null,
    host        varchar(255) not null,
    name        varchar(255) not null,
    port        varchar(255) not null,
    owner       varchar(255) not null,
    companyOwner varchar(255) not null,
    share       boolean      not null,
    charset     varchar(255) not null,
    serverSide  boolean      not null,


    primary key (id)
);

create table cmn_application_preference
(
    id               integer      not null,
    class_name       varchar(255) not null,
    description      varchar(255),
    preference_name  varchar(255) not null unique,
    preference_value varchar(255) not null,
    primary key (id)
);

create table cmn_application_template
(
    id          integer      not null,
    description varchar(2500),
    enable      boolean      not null,
    file_name   varchar(255) not null unique,
    title       varchar(255),
    primary key (id)
);
