INSERT INTO cmn_application_preference (id, class_name, preference_name, preference_value, description) VALUES
(200, 'java.lang.String', 'documentation_url', 'https://interop.referencement.esante.gouv.fr/gazelle-documentation/Gazelle-HL7V2-Simulators/user.html', 'Link to the documentation'),
(201, 'java.lang.String', 'contact_and_support', 'contact@kereval.com', 'Email to contact support'),
(202, 'java.lang.String', 'privacy_policy', 'https://www.ihe-europe.net/privacy-policy', 'Link to privacy policy');