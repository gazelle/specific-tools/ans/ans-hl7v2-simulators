package net.ihe.gazelle.ans.hl7v2.simulators.ui.interlay;

import io.quarkus.test.junit.mockito.InjectMock;
import jakarta.faces.context.FacesContext;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.DuplicationException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.NotFoundException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.SaveFileException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.FileCRUDService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.TemplateHl7v2Service;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.TemplateHl7v2;
import org.junit.jupiter.api.*;
import org.primefaces.PrimeFaces;
import org.primefaces.model.file.UploadedFile;
import org.primefaces.model.file.UploadedFileWrapper;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TemplateBeanTest {
    TemplateHl7v2Service templateHl7v2Service = mock(TemplateHl7v2Service.class);
    FileCRUDService<UploadedFile> fileCRUDService = mock(FileCRUDService.class);
    TemplateBean templateBean = new TemplateBean(templateHl7v2Service, fileCRUDService);

    private static TemplateHl7v2 template;

    @BeforeAll
    static void init() {
        template = TemplateHl7v2.builder()
                .withId(1)
                .withTitle("Titre")
                .withDescription("Description")
                .withEnabled(true)
                .withFileName("element.mustache")
                .build();
    }

    @Test
    void getAll_OK() {
        List<TemplateHl7v2> list = new ArrayList<>();
        when(templateHl7v2Service.getAllTemplatesCDA()).thenReturn(list);
        list.add(template);
        List<TemplateHl7v2> test = templateBean.getAll();
        Assertions.assertEquals(1, test.size());
    }

    @Test
    void insertTemplate_OK() {
        UploadedFile uploadedFile = new UploadedFileWrapper();
        doNothing().when(fileCRUDService).upload(uploadedFile);
        doNothing().when(templateHl7v2Service).createTemplate(template);
        Assertions.assertDoesNotThrow(() -> {
            templateBean.insertTemplate(template);
        });
    }

    @Test
    void insertTemplate_KO_Duplicate() {
        PrimeFaces.setCurrent(mock(PrimeFaces.class));
        doReturn(mock(PrimeFaces.Dialog.class)).when(PrimeFaces.current()).dialog();
        UploadedFile uploadedFile = null;
        doNothing().when(fileCRUDService).upload(uploadedFile);
        doThrow(new DuplicationException()).when(templateHl7v2Service).createTemplate(template);
        Assertions.assertDoesNotThrow(() -> {
            templateBean.insertTemplate(template);
        });
    }

    @Test
    void insertTemplate_KO_SaveFile() {
        PrimeFaces.setCurrent(mock(PrimeFaces.class));
        doReturn(mock(PrimeFaces.Dialog.class)).when(PrimeFaces.current()).dialog();
        UploadedFile uploadedFile = null;
        doThrow(new SaveFileException()).when(fileCRUDService).upload(uploadedFile);
        doNothing().when(templateHl7v2Service).createTemplate(template);
        Assertions.assertDoesNotThrow(() -> {
            templateBean.insertTemplate(template);
        });
    }

    @Test
    void insertTemplate_KO_AccesDenied() {
        PrimeFaces.setCurrent(mock(PrimeFaces.class));
        doReturn(mock(PrimeFaces.Dialog.class)).when(PrimeFaces.current()).dialog();
        UploadedFile uploadedFile = null;
        doThrow(new SaveFileException()).when(fileCRUDService).upload(uploadedFile);
        doNothing().when(templateHl7v2Service).createTemplate(template);
        Assertions.assertDoesNotThrow(() -> {
            templateBean.insertTemplate(template);
        });
    }

    @Test
    void insertUpdateTemplate_OK() {
        UploadedFile uploadedFile = null;
        PrimeFaces.setCurrent(mock(PrimeFaces.class));
        doReturn(mock(PrimeFaces.Ajax.class)).when(PrimeFaces.current()).ajax();
        doNothing().when(fileCRUDService).upload(uploadedFile);
        doNothing().when(templateHl7v2Service).createTemplate(template);
        Assertions.assertDoesNotThrow(() -> {
            templateBean.setSelectedTemplate(template);
            templateBean.insertUpdateSelectedTemplate();
        });
    }

    @Test
    void deleteTemplate_OK() {
        ThreadLocal<FacesContext> firstInstance = new ThreadLocal<FacesContext>();
        PrimeFaces.setCurrent(mock(PrimeFaces.class));
        doNothing().when(templateHl7v2Service).deleteTemplate(template);
        doNothing().when(fileCRUDService).delete(template.getFileName());
        doReturn(mock(PrimeFaces.Ajax.class)).when(PrimeFaces.current()).ajax();
        Assertions.assertDoesNotThrow(() -> {
            templateBean.setSelectedTemplate(template);
            templateBean.deleteTemplate();
        });
    }

    @Test
    void updateTemplate_OK() {
        PrimeFaces.setCurrent(mock(PrimeFaces.class));
        doNothing().when(templateHl7v2Service).updateTemplate(template);
        doReturn(template).when(templateHl7v2Service).getOneTemplate(template.getId());
        doNothing().when(templateHl7v2Service).updateTemplate(template);
        doNothing().when(fileCRUDService).delete(template.getFileName());
        doNothing().when(fileCRUDService).upload(mock(UploadedFile.class));
        doReturn(mock(PrimeFaces.Dialog.class)).when(PrimeFaces.current()).dialog();
        Assertions.assertDoesNotThrow(() -> {
            templateBean.setSavedFile(mock(UploadedFile.class));
            templateBean.updateTemplate(template);
        });
    }

    @Test
    void updateTemplate_KO_NotFound() {
        UploadedFile uploadedFile = mock(UploadedFile.class);

        PrimeFaces.setCurrent(mock(PrimeFaces.class));
        doNothing().when(templateHl7v2Service).updateTemplate(template);
        doReturn(template).when(templateHl7v2Service).getOneTemplate(template.getId());
        doNothing().when(templateHl7v2Service).updateTemplate(template);
        doThrow(new NotFoundException()).when(fileCRUDService).delete(template.getFileName());
        doNothing().when(fileCRUDService).upload(uploadedFile);
        doReturn(mock(PrimeFaces.Dialog.class)).when(PrimeFaces.current()).dialog();

        templateBean.setSavedFile(uploadedFile);
        Assertions.assertDoesNotThrow(() -> {

            templateBean.updateTemplate(template);
        });
    }

    @Test
    void updateTemplate_KO_Duplicate() {
        PrimeFaces.setCurrent(mock(PrimeFaces.class));
        doNothing().when(templateHl7v2Service).updateTemplate(template);
        doReturn(template).when(templateHl7v2Service).getOneTemplate(template.getId());
        doNothing().when(templateHl7v2Service).updateTemplate(template);
        doNothing().when(fileCRUDService).delete(template.getFileName());
        doThrow(new DuplicationException()).when(fileCRUDService).upload(mock(UploadedFile.class));
        doReturn(mock(PrimeFaces.Dialog.class)).when(PrimeFaces.current()).dialog();
        Assertions.assertDoesNotThrow(() -> {
            templateBean.setSavedFile(mock(UploadedFile.class));
            templateBean.updateTemplate(template);
        });
    }

    @Test
    void openNew_OK() {
        templateBean.openNew();
        Assertions.assertEquals("", templateBean.getSelectedTemplate().getTitle());
        Assertions.assertEquals("", templateBean.getSelectedTemplate().getDescription());

        Assertions.assertFalse(templateBean.getSelectedTemplate().isEnable());

    }
}
