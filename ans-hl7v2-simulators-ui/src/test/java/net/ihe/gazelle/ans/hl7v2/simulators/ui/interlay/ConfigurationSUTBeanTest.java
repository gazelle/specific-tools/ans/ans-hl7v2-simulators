package net.ihe.gazelle.ans.hl7v2.simulators.ui.interlay;

import io.quarkus.test.junit.mockito.InjectMock;
import jakarta.inject.Inject;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.ConfigurationService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;
import net.ihe.gazelle.ans.hl7v2.simulators.ui.interlay.model.GetPrincipal;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
final class ConfigurationSUTBeanTest {

    ConfigurationService service = mock(ConfigurationService.class);

    GetPrincipal getPrincipal = mock(GetPrincipal.class);

    ConfigurationSUTBean configurationBean = new ConfigurationSUTBean(service,getPrincipal);
    private static ConfigurationSUT configurationSUT;

    @BeforeAll
    static void init() {
        configurationSUT = ConfigurationSUT.builder()
                .withName("myConf")
                .withHost("myHost")
                .withPort("8080")
                .withApplication("myApp")
                .withFacility("myFacility")
                .withOwner("me")
                .withCompanyOwner("myCompany")
                .withShare(true)
                .withCharset("UTF-8")
                .build();
    }

    @Test
    void insert_OK() {
        doNothing().when(service).insertConfiguration(isA(ConfigurationSUT.class));
        configurationBean.insertConfiguration(configurationSUT);
        verify(service, times(1)).insertConfiguration(configurationSUT);

    }

    @Test
    void getOne_OK() {
        when(service.getOneConfiguration(configurationSUT.getId())).thenReturn(configurationSUT);
        ConfigurationSUT entity = configurationBean.getOne(configurationSUT.getId());
        Assertions.assertEquals("myHost", entity.getHost());
    }

    @Test
    void update_OK() {
        doNothing().when(service).updateConfiguration(isA(ConfigurationSUT.class));
        configurationBean.updateConfiguration(configurationSUT);
        verify(service, times(1)).updateConfiguration(configurationSUT);

    }

    @Test
    void delete_OK() {
        doNothing().when(service).deleteConfiguration(isA(ConfigurationSUT.class));
        configurationBean.delete(configurationSUT);
        verify(service, times(1)).deleteConfiguration(configurationSUT);

    }

    @Test
    void getAll_OK() {
        when(getPrincipal.isAdmin()).thenReturn(true);
        List<ConfigurationSUT> list = new ArrayList<>();
        list.add(configurationSUT);
        when(service.getAllConfiguration()).thenReturn(list);
        List<ConfigurationSUT> test = configurationBean.getAll();
        Assertions.assertEquals(1, test.size());
    }
}