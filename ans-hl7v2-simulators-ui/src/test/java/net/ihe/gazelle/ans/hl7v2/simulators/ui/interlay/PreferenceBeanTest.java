package net.ihe.gazelle.ans.hl7v2.simulators.ui.interlay;

import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.DuplicationException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.PreferenceService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.Preference;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
final class PreferenceBeanTest {

    PreferenceService service = mock(PreferenceService.class);
    PreferenceBean preferenceBean = new PreferenceBean(service);
    private static Preference preference;

    @BeforeAll
    static void init() {
        preference = Preference.builder()
                .withPreferenceName("myPref")
                .withClassName("java.lang.String")
                .withPreferenceValue("value")
                .withDescription("My value")
                .build();
    }

    @Test
    void insert_OK() {
        doNothing().when(service).insertPreference(isA(Preference.class));
        preferenceBean.insertPreference(preference);
        verify(service,times(1)).insertPreference(preference);
    }

    @Test
    void insert_KO() {
        doNothing().when(service).insertPreference(isA(Preference.class));
        preferenceBean.insertPreference(preference);
        doThrow(new DuplicationException()).when(service).insertPreference(preference);
        verify(service,times(1)).insertPreference(preference);
    }

    @Test
    void getOne_OK() {
        when(service.getOnePreference(preference.getId())).thenReturn(preference);
        Preference entity = preferenceBean.getOne(preference.getId());
        Assertions.assertEquals("myPref", entity.getPreferenceName());
    }

    @Test
    void update_OK() {
        doNothing().when(service).updatePreference(isA(Preference.class));
        preferenceBean.updatePreference(preference);
        verify(service,times(1)).updatePreference(preference);
    }

    @Test
    void delete_OK() {
        doNothing().when(service).deletePreference(isA(Preference.class));
        preferenceBean.deletePreference(preference);
        verify(service,times(1)).deletePreference(preference);
    }

    @Test
    void getAll_OK() {
        List<Preference> list = new ArrayList<>();
        list.add(preference);
        when(service.getAllPreference()).thenReturn(list);
        List<Preference> test = preferenceBean.getAll();
        Assertions.assertEquals(1, test.size());
    }

    @Test
    void openNew_OK(){
        preferenceBean.openNew();
        Assertions.assertEquals("myPref",preferenceBean.getSelectedPreference().getPreferenceName());
        Assertions.assertEquals("java.lang.String",preferenceBean.getSelectedPreference().getClassName());
        Assertions.assertEquals("value",preferenceBean.getSelectedPreference().getPreferenceValue());
        Assertions.assertEquals("My value",preferenceBean.getSelectedPreference().getDescription());
    }

}