INSERT INTO cmn_application_configuration_sut (id, application, facility, host, name, port, owner, companyOwner, share, charset) VALUES (1, 'local_utf8_manager_configuration', 'ANS', '127.0.0.1', 'ans-simulators-utf8-manager', '5453','ANS-HL7V2-SIMULATOR','ANS-HL7V2-SIMULATOR',true,'UNICODE UTF-8');
INSERT INTO cmn_application_configuration_sut (id, application, facility, host, name, port, owner, companyOwner, share, charset) VALUES (2, 'local_i885915_manager_configuration', 'ANS', '127.0.0.1', 'ans-simulators-i885915-manager', '5455','ANS-HL7V2-SIMULATOR','ANS-HL7V2-SIMULATOR',true,'8859/15');
INSERT INTO cmn_application_configuration_sut (id, application, facility, host, name, port, owner, companyOwner, share, charset) VALUES (3, 'local_creator_configuration', 'ANS', '127.0.0.1', 'ans-hl7v2-simulators-creator', '5456','ANS-HL7V2-SIMULATOR','ANS-HL7V2-SIMULATOR',true,'');


INSERT INTO cmn_application_preference (id,class_name,description,preference_name,preference_value) VALUES(1,'java.lang.String','Endpoint Validator','Endpoint_Validator','https://interop.esante.gouv.fr/GazelleHL7v2Validator-ejb/gazelleHL7v2ValidationWSService/gazelleHL7v2ValidationWS?wsdl');

INSERT INTO cmn_application_preference (id,class_name,description,preference_name,preference_value) VALUES(2,'java.lang.String','UTF8_Manager_Name','UTF8_Manager_Name','ans-simulators-utf8--manager');
INSERT INTO cmn_application_preference (id,class_name,description,preference_name,preference_value) VALUES(3,'java.lang.String','UTF8_Manager_Host','UTF8_Manager_Host','127.0.0.1');
INSERT INTO cmn_application_preference (id,class_name,description,preference_name,preference_value) VALUES(4,'java.lang.String','UTF8_Manager_Port','UTF8_Manager_Port','5453');
INSERT INTO cmn_application_preference (id,class_name,description,preference_name,preference_value) VALUES(5,'java.lang.String','UTF8_Manager_Application','UTF8_Manager_Application','local_manager_configuration');
INSERT INTO cmn_application_preference (id,class_name,description,preference_name,preference_value) VALUES(6,'java.lang.String','UTF8_Manager_Facility','UTF8_Manager_Facility','ANS');

INSERT INTO cmn_application_preference (id,class_name,description,preference_name,preference_value) VALUES(7,'java.lang.String','I885915_Manager_Name','I885915_Manager_Name','ans-simulators-i885915-manager');
INSERT INTO cmn_application_preference (id,class_name,description,preference_name,preference_value) VALUES(8,'java.lang.String','I885915_Manager_Host','I885915_Manager_Host','127.0.0.1');
INSERT INTO cmn_application_preference (id,class_name,description,preference_name,preference_value) VALUES(9,'java.lang.String','I885915_Manager_Port','I885915_Manager_Port','5455');
INSERT INTO cmn_application_preference (id,class_name,description,preference_name,preference_value) VALUES(10,'java.lang.String','I885915_Manager_Application','I885915_Manager_Application','local_manager_configuration');
INSERT INTO cmn_application_preference (id,class_name,description,preference_name,preference_value) VALUES(11,'java.lang.String','I885915_Manager_Facility','I885915_Manager_Facility','ANS');

INSERT INTO cmn_application_preference (id,class_name,description,preference_name,preference_value) VALUES(12,'java.lang.String','Creator_Name','Creator_Name','ans-hl7v2-simulators-creator');
INSERT INTO cmn_application_preference (id,class_name,description,preference_name,preference_value) VALUES(13,'java.lang.String','Creator_Host','Creator_Host','127.0.0.1');
INSERT INTO cmn_application_preference (id,class_name,description,preference_name,preference_value) VALUES(14,'java.lang.String','Creator_Port','Creator_Port','5456');
INSERT INTO cmn_application_preference (id,class_name,description,preference_name,preference_value) VALUES(15,'java.lang.String','Creator_Application','Creator_Application','local_creator_configuration');
INSERT INTO cmn_application_preference (id,class_name,description,preference_name,preference_value) VALUES(16,'java.lang.String','Creator_Facility','Creator_Facility','ANS');
