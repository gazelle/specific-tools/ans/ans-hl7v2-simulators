#!/bin/bash

if [ -n "${HTTP_PROXY_HOST}" ]; then
    export JAVA_OPTS="$JAVA_OPTS -Dhttp.proxyHost=${HTTP_PROXY_HOST} -Dhttp.proxyPort=${HTTP_PROXY_PORT} -Dhttp.nonProxyHosts=${HTTP_NON_PROXY_HOSTS}"
fi

if [ -n "${HTTPS_PROXY_HOST}" ]; then
    export JAVA_OPTS="$JAVA_OPTS -Dhttps.proxyHost=${HTTPS_PROXY_HOST} -Dhttps.proxyPort=${HTTPS_PROXY_PORT} -Dhttps.nonProxyHosts=${HTTPS_NON_PROXY_HOSTS}"
fi


# Database management
if [ "${DB_ENABLED:-true}" = "true" ]; then
    export PGPASSWORD=${DB_PASSWORD}

    # Check if db host is reachable
    while ! pg_isready -h $DB_HOST --port ${DB_PORT} > /dev/null 2> /dev/null;
    do
      echo "⚠ Waiting 5s for database server to be reachable ${DB_HOST}:${DB_PORT}..."
      sleep 5
    done
    echo "✓ Database server is accessible"

    # Check if database exists
    psql -h ${DB_HOST} --port ${DB_PORT} -U ${DB_USER} ${DB_NAME} -c '\q' >/dev/null 2>/dev/null
    if [ $? -ne 0 ]; then
        # Create DB if not exists
        echo "⚠ Create database ${DB_NAME}..."
        createdb -U ${DB_USER} --port ${DB_PORT} -h ${DB_HOST} -E UTF-8 ${DB_NAME}
        echo "⚠ Load init schema ${DB_NAME}..."
        psql -h ${DB_HOST} --port ${DB_PORT} -U ${DB_USER} ${DB_NAME} < /imports/SQL/init_schema.sql
    fi
fi

if [ "${DEFAULT_ANS_SERVERS:-true}" = "true" ]; then
    # Perform insert in databases
    echo "⚠ Perform insert default configuration SUT in databases ..."
    psql -h ${DB_HOST} --port ${DB_PORT} -U ${DB_USER} ${DB_NAME} < /imports/SQL/default_config.sql
fi

if [ "${DEFAULT_ANS_TEMPLATES:-true}" = "true" ]; then
    # Perform update in databases
    echo "⚠ Perform insert ANS Templates in databases ..."
    psql -h ${DB_HOST} --port ${DB_PORT} -U ${DB_USER} ${DB_NAME} < /imports/SQL/default_template.sql
    echo "⚠ Perform copy defaults templates in CDA directory ..."
	  mkdir -p /opt/ans-hl7v2-simulators/templates/CDA
	  mkdir -p /opt/ans-hl7v2-simulators/templates/ZAM
    cp /imports/CDA/* /opt/ans-hl7v2-simulators/templates/CDA
    cp /ZAM/* /opt/ans-hl7v2-simulators/templates/ZAM
fi
mkdir -p /opt/ans-hl7v2-simulators/JDV
cp /JDV/* /opt/ans-hl7v2-simulators/JDV

# Cas configuration
CAS_FILE=/opt/gazelle/cas/${APP_NAME}.properties

if [ ! -f "$CAS_FILE" ];
  then
    echo "------------------------------------------------------------------------------"
    echo "⚠ Perform create CAS File for GUM: /opt/gazelle/cas/${APP_NAME}.properties ..."
    echo "------------------------------------------------------------------------------"
	  mkdir -p /opt/gazelle/cas
    touch $CAS_FILE
fi

echo "⚠ Write environment variables in CAS file: casServerUrlPrefix=${CAS_SERVER_URL_PREFIX} ..."
echo "casServerUrlPrefix=${CAS_SERVER_URL_PREFIX}" > $CAS_FILE
echo "⚠ Write environment variables in CAS file: casServerLoginUrl=${CAS_SERVER_LOGIN_URL} ..."
echo "casServerLoginUrl=${CAS_SERVER_LOGIN_URL}" >> $CAS_FILE
echo "⚠ Write environment variables in CAS file: casLogoutUrl=${CAS_LOGOUT_URL} ..."
echo "casLogoutUrl=${CAS_LOGOUT_URL}" >> $CAS_FILE
# VLD 30/04 : We use service= here because we are using org.apereo.cas.client as CAS client ( does not support serverName like other apps)
echo "⚠ Write environment variables in CAS file: service=${SERVER_NAME} ..."
echo "service=${SERVER_NAME}/hl7v2-simulators" >> $CAS_FILE

echo "                             ,                                                                             "
echo "                                ******,                                                                    "
echo "                                       ***.                                                                "
echo "                                    ....(*****.                                                            "
echo "                                           /*******,                                                       "
echo "                                          //***********                                                    "
echo "                                         //((((                                                            "
echo "                                       /((((((                                                             "
echo "                                     (((#%%%%,                                                             "
echo "                                  (#%%%%%%%%%,                                                             "
echo "                               #%%%%%%%%%%%%%,                                                             "
echo "                           %%%%%%%%%%%%%%%%%%                                                              "
echo "                      ,%%%%%%%%%%%%%%%%%%%%%*                                                              "
echo "           /((((((#%%%%%%%##(((((((((##%%%%                                                                "
echo "              %%(((((/,.....        ..(((                                                                  "
echo "            %%%%%*....             .,((                                                                    "
echo "          /%%%/...                .((                                                                      "
echo "         %%%,.                  ./.                                                                        "
echo "       #%%,.        ,#(,       ,                                                %%%   #%%                  "
echo "      %%(      .%%%%/  (%%%%                                                    %%%   %%%                  "
echo "     %%       %%%          ,       %%%%%%%% %%%   %%%%%%%%%%%#    %%%%%%%%#     %%%   %%%      %%%%%%%%    "
echo "    %.       .%%                 %%%,      %%%%          %%%(   %%%       %%#   %%%   %%%   .%%*      ,%%  "
echo "  *%         /%%      %%%%%%%%   %%*        %%%        %%%,    /%%%%%%%%%%%%%.  %%%   %%%   %%%%%%%%%%%%%% "
echo " (            %%%          %%%   %%(        %%%      %%%       *%%              %%%   %%%   %%#            "
echo ",              %%%(      #%%%    /%%#      %%%%    %%%          %%%.      %%%   %%%   %%%    %%%      ,%%  "
echo "               ,%%%%%%%%         *%%%%%%* %%%   %%%%%%%%%%%.    ,%%%%%%%#     %%%   #%%      %%%%%%%%.     "

echo ""
echo ""
echo " _  _  _    ____ __   __ ___ "
echo "| || || |  |__  |\ \ / /|_  )"
echo "| __ || |__  / /  \ V /  / / "
echo "|_||_||____|/_/    \_/  /___|"
echo " ___  _              _        _                 "
echo "/ __|(_) _ __  _  _ | | __ _ | |_  ___  _ _  ___"
echo "\__ \| || '  \| || || |/ _\` ||  _|/ _ \| '_|(_-<"
echo "|___/|_||_|_|_|\_,_||_|\__,_| \__|\___/|_|  /__/"
echo ""

# Run the script
/usr/local/s2i/run
