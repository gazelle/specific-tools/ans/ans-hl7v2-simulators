package net.ihe.gazelle.ans.hl7v2.simulators.ui.interlay;

import io.quarkus.runtime.Startup;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.faces.application.FacesMessage;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.NotFoundException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.ConfigurationService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.ManagerService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.PreferenceService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.TemplateHl7v2Service;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;
import org.primefaces.PrimeFaces;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 20/06/2023
 */

@Named
@ApplicationScoped
public class Hl7ManagerBean {

    private final ManagerService managerService;
    private final ConfigurationService configurationService;
    private final PreferenceService preferenceService;
    private final TemplateHl7v2Service templateHl7v2Service;
    ConfigurationSUT configurationServerUTF8;
    ConfigurationSUT configurationServerI885915;

    @Inject
    public Hl7ManagerBean(ManagerService managerService, ConfigurationService configurationService, TemplateHl7v2Service templateHl7v2Service, PreferenceService preferenceService) {
        this.managerService = managerService;
        this.configurationService = configurationService;
        this.templateHl7v2Service = templateHl7v2Service;
        this.preferenceService = preferenceService;
    }


    @Startup
    public void initialize() {
        if (!this.managerService.isServerRunning()) {
            configurationServerUTF8 = configurationService.getLocalUTF8ManagerConfiguration();
            configurationServerI885915 = configurationService.getLocalI885915ManagerConfiguration();
            this.managerService.switchServerStateMessages(configurationService, templateHl7v2Service);
        }
    }


    public boolean checkZamFiles() {
        return !templateHl7v2Service.isZAMThere();

    }

    public void switchListenMessages() {
        if (preferenceService.getPreferenceByName("Endpoint_Validator") == null) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error no validator ", "There is no validator define for the manager. You should link one by  creating a preference with 'Endpoint_Validator' as preference name.");
            PrimeFaces.current().dialog().showMessageDynamic(message);
        } else {

            try {
                configurationServerUTF8 = configurationService.getLocalUTF8ManagerConfiguration();
                configurationServerI885915 = configurationService.getLocalI885915ManagerConfiguration();
                this.managerService.switchServerStateMessages(configurationService, templateHl7v2Service);
            } catch (NotFoundException e) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error No Configuration", "There is no configuration define for the manager. You should create one with local_manager_configuration as application name.");
                PrimeFaces.current().dialog().showMessageDynamic(message);
            }
        }
    }

    public String labelActionServer() {
        if (this.managerService.isServerRunning()) return "STOP";
        else return "START";

    }

    public String getStatus() {
        if (this.managerService.isServerRunning()) return "UP";
        else return "DOWN";
    }

    public ManagerService getManagerService() {
        return managerService;
    }

    public ConfigurationSUT getConfigurationServerUTF8() {
        return configurationServerUTF8;
    }


    public ConfigurationSUT getConfigurationServerI885915() {
        return configurationServerI885915;
    }
}
