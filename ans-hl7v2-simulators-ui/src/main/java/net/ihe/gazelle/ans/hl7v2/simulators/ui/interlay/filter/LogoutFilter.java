package net.ihe.gazelle.ans.hl7v2.simulators.ui.interlay.filter;

import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.apereo.cas.client.Protocol;
import org.apereo.cas.client.configuration.ConfigurationKeys;
import org.apereo.cas.client.util.AbstractCasFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;


/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 19/06/2023
 */

@Named("/cas/logout")
public class LogoutFilter extends AbstractCasFilter {

    private static final Logger LOG = LoggerFactory.getLogger(LogoutFilter.class);
    private String casServerUrl;
    private String applicationUrl;
    @Inject
    public HttpSession session;

    public LogoutFilter() {
        super(Protocol.CAS2);
    }

    public String getApplicationUrl() {
        return applicationUrl;
    }

    public void setApplicationUrl(String applicationUrl) {
        this.applicationUrl = applicationUrl;
    }

    public String getCasServerUrl() {
        return casServerUrl;
    }

    public void setCasServerUrl(String casServerUrl) {
        this.casServerUrl = casServerUrl;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) {
        StringBuilder logoutUrlBuilder = new StringBuilder();
        try {
            logoutUrlBuilder.append(getCasServerUrl());
            logoutUrlBuilder.append("/logout");
            logout(session);
            if (getApplicationUrl() != null) {
                logoutUrlBuilder.append("?service=");
                final String encodedKey = URLEncoder.encode(getApplicationUrl(), StandardCharsets.UTF_8);
                logoutUrlBuilder.append(encodedKey);
            }
            ((HttpServletResponse) servletResponse).sendRedirect(logoutUrlBuilder.toString());
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    @Override
    public void init() {
        super.init();
        setCasServerUrl(getString(ConfigurationKeys.CAS_SERVER_URL_PREFIX));
        reloadParameters();
    }

    private void reloadParameters() {
        setApplicationUrl(getString(ConfigurationKeys.SERVICE));
    }

    public String logout(HttpSession session){
        session.invalidate();
        return "redirect:/ans-hl7v2-simulators/";
    }

}
