package net.ihe.gazelle.ans.hl7v2.simulators.ui.interlay;

import jakarta.faces.application.FacesMessage;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.servlet.http.Part;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.AccesDeniedException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.DuplicationException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.NotFoundException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.SaveFileException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.FileCRUDService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.TemplateHl7v2Service;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.TemplateHl7v2;
import org.primefaces.PrimeFaces;
import org.primefaces.model.file.UploadedFile;

import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 22/07/2023
 */
@Named
@ViewScoped
public class TemplateBean {
    private Part uploadedFile;
    private UploadedFile savedFile;
    private TemplateHl7v2 selectedTemplate;
    private String title;
    private String description;
    private boolean enable;
    private String fileName;
    @Inject
    TemplateHl7v2Service service;
    @Inject
    FileCRUDService<UploadedFile> fileService;

    public TemplateBean(TemplateHl7v2Service service, FileCRUDService<UploadedFile> fileService) {
        this();
        this.service = service;
        this.fileService = fileService;
    }

    public TemplateBean() {
    }

    public void openNew() {
        this.setSelectedTemplate(TemplateHl7v2.builder()
                .withTitle("")
                .withDescription("")
                .withEnabled(false)
                .build());
    }

    public List<TemplateHl7v2> getAll() {
        return service.getAllTemplatesCDA();
    }

    public TemplateHl7v2 getOne(int id) {
        return this.service.getOneTemplate(id);
    }

    public void deleteTemplate() {
        try {
            fileService.delete(getSelectedTemplate().getFileName());
        } catch (NotFoundException e) {
            FacesMessage msg = new FacesMessage("Error! ", "Matching template was not found! Deleting element ...");
            PrimeFaces.current().dialog().showMessageDynamic(msg);
        }
        this.service.deleteTemplate(this.getSelectedTemplate());
        PrimeFaces.current().ajax().update("form:messages", "form:dt-templates");
    }

    public void insertUpdateSelectedTemplate() {
        TemplateHl7v2 templateHl7v2 = TemplateHl7v2.builder()
                .withId(this.selectedTemplate.getId())
                .withDescription(this.description)
                .withEnabled(this.enable)
                .withTitle(this.title)
                .withFileName(savedFile != null ? savedFile.getFileName() : this.fileName)
                .build();

        if (templateHl7v2.getId() != 0) {
            updateTemplate(templateHl7v2);
        } else {
            insertTemplate(templateHl7v2);
        }
        PrimeFaces.current().executeScript("PF('manageTemplateDialog').hide()");
        PrimeFaces.current().ajax().update("form:messages", "form:dt-templates");
    }

    private static final String DESTINATION = "/opt/ans-hl7v2-simulators/templates/CDA/";

    public void insertTemplate(TemplateHl7v2 template) {
        try {
            this.service.createTemplate(template);

            fileService.upload(this.savedFile);

        } catch (DuplicationException e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error Duplicate", "Invalid Template. The name " + template.getTitle() + " or the selectedTemplate file may already exist");
            PrimeFaces.current().dialog().showMessageDynamic(message);
        } catch (SaveFileException e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error Can't save File", "Error when uploading the File");
            PrimeFaces.current().dialog().showMessageDynamic(message);
        } catch (AccesDeniedException e) {
            FacesMessage msg = new FacesMessage("Error! ", "Not allowed to write in the " + DESTINATION + " directory. You should change permissions to access the directory.");
            PrimeFaces.current().dialog().showMessageDynamic(msg);
        }
    }

    public void updateTemplate(TemplateHl7v2 template) {
        TemplateHl7v2 templateOld= this.service.getOneTemplate(template.getId());
        try {
            this.service.updateTemplate(template);
            if (getSavedFile() != null) {
                fileService.delete(templateOld.getFileName());
                fileService.upload(this.getSavedFile());
            }
        } catch (DuplicationException e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error Duplicate", "Invalid Template. The title " + template.getTitle() + " or the selectedTemplate file may already exist!");
            PrimeFaces.current().dialog().showMessageDynamic(message);
        } catch (NotFoundException e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error not Found", "Template not found. The template does not exist!");
            PrimeFaces.current().dialog().showMessageDynamic(message);
        }
    }


    public TemplateHl7v2 getSelectedTemplate() {
        return selectedTemplate;
    }


    public void setSelectedTemplate(TemplateHl7v2 template) {
        this.selectedTemplate = template;
        this.enable = template.isEnable();
        this.title = template.getTitle();
        this.description = template.getDescription();
        this.fileName = template.getFileName();
    }

    public String getContent() {
        return fileService.read(this.fileName);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Part getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(Part uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public UploadedFile getSavedFile() {
        return savedFile;
    }

    public void setSavedFile(UploadedFile savedFile) {
        this.savedFile = savedFile;
    }

    public String getDestination() {
        return DESTINATION;
    }
}
