package net.ihe.gazelle.ans.hl7v2.simulators.ui.interlay;


import jakarta.enterprise.context.ApplicationScoped;
import jakarta.faces.application.FacesMessage;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.Hl7ConnexionException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.NotFoundException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.ConfigurationService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.CreatorService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.TemplateHl7v2Service;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.TemplateHl7v2;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.model.MessageData;
import net.ihe.gazelle.ans.hl7v2.simulators.ui.interlay.model.GetPrincipal;
import org.primefaces.PrimeFaces;
import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 20/06/2023
 */
@Named
@ApplicationScoped
public class Hl7CreatorBean {

    private ConfigurationSUT configuration;
    private TemplateHl7v2 templateHl7v2;
    private String messageSend;
    private MessageData selectedMessageData;
    private final ConfigurationService configurationService;
    private final TemplateHl7v2Service templateHl7v2Service;
    private final CreatorService<MessageData> creatorService;

    private final GetPrincipal getPrincipal;


    @Inject
    public Hl7CreatorBean(ConfigurationService service, TemplateHl7v2Service templateHl7v2Service, CreatorService<MessageData> creatorService, GetPrincipal getPrincipal) {
        this.configurationService = service;
        this.templateHl7v2Service = templateHl7v2Service;
        this.creatorService = creatorService;
        this.getPrincipal = getPrincipal;
    }


    public void send() throws InterruptedException {
        try {
            this.creatorService.sendMessage(this.configuration, this.templateHl7v2, configurationService.getLocalCreatorConfiguration());
        } catch (NotFoundException e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error No Configuration", "There is no configuration define for the manager. You should create one with local_creator_configuration as application name.");
            PrimeFaces.current().dialog().showMessageDynamic(message);
        } catch (Hl7ConnexionException e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Connexion Failed", "Cannot connect to the manager please verify that your manager is started!");
            PrimeFaces.current().dialog().showMessageDynamic(message);
        }
    }

    public String getPortSupplier() {
        try {
            return configurationService.getLocalCreatorConfiguration().getPort();
        } catch (NotFoundException e) {
            return "";
        }

    }

    void resetCreatorServer() {
        this.creatorService.resetCreatorServer();
    }


    public List<ConfigurationSUT> getConfigurations() {
        if (getPrincipal == null || !getPrincipal.isLoggedIn()) {
            return this.configurationService.getAvailableConfigurations("", "");
        } else if (getPrincipal.isAdmin()) {

            return this.configurationService.getAllConfiguration();
        } else {

            return this.configurationService.getAvailableConfigurations(getPrincipal.getUsername(), getPrincipal.getOrganizationKeyword());
        }
    }


    public List<TemplateHl7v2> getTemplatesHl7v2() {
        return templateHl7v2Service.getEnableTemplates();
    }


    public ConfigurationSUT getConfiguration() {
        return configuration;
    }

    public void setConfiguration(ConfigurationSUT configuration) {
        this.configuration = configuration;
    }

    public TemplateHl7v2 getTemplateHl7v2() {
        return templateHl7v2;
    }

    public void setTemplateHl7v2(TemplateHl7v2 templateHl7v2) {
        this.templateHl7v2 = templateHl7v2;
    }

    public String getMessageSend() {
        return messageSend;
    }

    public void setMessageSend(String messageSend) {
        this.messageSend = messageSend;
    }

    public List<MessageData> getHistoryMessage() {
        return this.creatorService.getHistoryMessage();
    }


    public MessageData getSelectedMessageData() {
        return selectedMessageData;
    }

    public void setSelectedMessageData(MessageData selectedMessageData) {
        this.selectedMessageData = selectedMessageData;
    }
}
