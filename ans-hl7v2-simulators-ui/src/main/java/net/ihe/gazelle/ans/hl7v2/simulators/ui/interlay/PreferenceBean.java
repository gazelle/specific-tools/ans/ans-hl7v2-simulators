package net.ihe.gazelle.ans.hl7v2.simulators.ui.interlay;

import jakarta.faces.application.FacesMessage;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.DuplicationException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.NotFoundException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.PreferenceService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.Preference;
import org.primefaces.PrimeFaces;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 20/06/2023
 */
@Named
@ViewScoped
public class PreferenceBean {

    private Preference selectedPreference;
    private String preferenceName;
    private String className;
    private String preferenceValue;
    private String description;
    @Inject
    PreferenceService preferenceService;

    public PreferenceBean(PreferenceService preferenceService) {
        this();
        this.preferenceService = preferenceService;
    }

    public PreferenceBean() {
    }

    public List<Preference> getAll() {
        return this.preferenceService.getAllPreference();
    }

    public Preference getOne(int id) {
        return this.preferenceService.getOnePreference(id);
    }

    public void deletePreference(Preference preference) {
        this.preferenceService.deletePreference(preference);
    }

    public void openNew() {
        this.setSelectedPreference(Preference.builder()
                .withPreferenceName("myPref")
                .withClassName("java.lang.String")
                .withPreferenceValue("value")
                .withDescription("My value")
                .build());

    }

    public Preference getSelectedPreference() {
        return this.selectedPreference;
    }

    public void setSelectedPreference(Preference selectedPreference) {
        this.selectedPreference = selectedPreference;
        this.preferenceName = selectedPreference.getPreferenceName();
        this.className = selectedPreference.getClassName();
        this.preferenceValue = selectedPreference.getPreferenceValue();
        this.description = selectedPreference.getDescription();
    }

    public void deleteSelectedPreference() {
        deletePreference(this.getSelectedPreference());
        PrimeFaces.current().ajax().update("form:messages", "form:dt-preferences");
    }

    public void insertUpdateSelectedPreference() {
        Preference pref = Preference.builder()
                .withId(this.selectedPreference.getId())
                .withPreferenceName(this.preferenceName)
                .withClassName(this.className)
                .withPreferenceValue(this.preferenceValue)
                .withDescription(this.description)
                .build();

        if (pref.getId() != 0)
            updatePreference(pref);
        else insertPreference(pref);

        PrimeFaces.current().executeScript("PF('managePreferenceDialog').hide()");
        PrimeFaces.current().ajax().update("form:messages", "form:dt-preferences");
    }

    public void insertPreference(Preference preference) {
        try {
            this.preferenceService.insertPreference(preference);
        } catch (DuplicationException e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error Duplicate", "Invalid Preference. The name " + preference.getPreferenceName() + " may already exist!");
            PrimeFaces.current().dialog().showMessageDynamic(message);
        }
    }

    public void updatePreference(Preference preference) {
        try {
            this.preferenceService.updatePreference(preference);
        } catch (DuplicationException e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error Duplicate", "Invalid Preference. The name " + preference.getPreferenceName() + " may already exist!");
            PrimeFaces.current().dialog().showMessageDynamic(message);
        } catch (NotFoundException e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error not Found", "Configuration not found. The configuration does not exist!");
            PrimeFaces.current().dialog().showMessageDynamic(message);
        }
    }


    public String getPreferenceName() {
        return preferenceName;
    }

    public void setPreferenceName(String preferenceName) {
        this.preferenceName = preferenceName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getPreferenceValue() {
        return preferenceValue;
    }

    public void setPreferenceValue(String preferenceValue) {
        this.preferenceValue = preferenceValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getDocumentationUrl() {
        Preference documentationUrl = this.preferenceService.getPreferenceByName("documentation_url");
        if (documentationUrl != null)
            return documentationUrl.getPreferenceValue();
         else return "";
    }


    public String version() {
        String name = System.getenv("APP_NAME");
        String version = System.getenv("APP_VERSION");
        return name + " :: " + version;
    }

    public String getContact() {
        Preference pref = this.preferenceService.getPreferenceByName("contact_and_support");
        if (pref != null)
            return pref.getPreferenceValue();
        else return "";
    }

    public String getTermAndCondition() {
        Preference pref = this.preferenceService.getPreferenceByName("privacy_policy");
        if (pref != null)
            return pref.getPreferenceValue();
        else return "";
    }

    public String getCurrentYear() {
        Date currentDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        return formatter.format(currentDate);
    }

}