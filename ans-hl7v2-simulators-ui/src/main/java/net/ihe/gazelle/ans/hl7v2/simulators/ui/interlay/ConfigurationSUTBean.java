package net.ihe.gazelle.ans.hl7v2.simulators.ui.interlay;

import jakarta.faces.application.FacesMessage;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.DuplicationException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.NotFoundException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.ConfigurationService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;
import net.ihe.gazelle.ans.hl7v2.simulators.ui.interlay.model.GetPrincipal;
import org.primefaces.PrimeFaces;

import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 20/06/2023
 */
@Named
@ViewScoped
public class ConfigurationSUTBean {

    private final String[] charsets = {"UNICODE UTF-8","8859/15"};
    @Inject
    ConfigurationService configurationService;
    @Inject
    GetPrincipal getPrincipal;
    private ConfigurationSUT selectedConfigurationSUT;
    private String name;
    private String host;
    private String port;
    private String application;
    private String facility;
    private String owner;
    private String companyOwner;
    private boolean share;
    private String charset;

    public ConfigurationSUTBean(ConfigurationService configurationService, GetPrincipal getPrincipal) {
        this();
        this.configurationService = configurationService;
        this.getPrincipal = getPrincipal;
    }

    public ConfigurationSUTBean() {
    }



    public List<ConfigurationSUT> getAll() {
        if (getPrincipal.isAdmin()) return this.configurationService.getAllConfiguration();
        else
            return this.configurationService.getAvailableConfigurations(getPrincipal.getUsername(), getPrincipal.getOrganizationKeyword());


    }


    public ConfigurationSUT getOne(int id) {
        return this.configurationService.getOneConfiguration(id);
    }

    public void insertConfiguration(ConfigurationSUT configurationSUT) {
        try {
            this.configurationService.insertConfiguration(configurationSUT);
        } catch (DuplicationException e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error Duplicate", "Invalid Configuration. The application " + application + " may already exist!");
            PrimeFaces.current().dialog().showMessageDynamic(message);
        }
    }

    public void updateConfiguration(ConfigurationSUT configurationSUT) {
        try {
            this.configurationService.updateConfiguration(configurationSUT);
        } catch (DuplicationException e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error Duplicate", "Invalid Configuration. The application " + application + " may already exist!");
            PrimeFaces.current().dialog().showMessageDynamic(message);
        } catch (NotFoundException e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error Not Found", "Configuration not found. The configuration does not exist!");
            PrimeFaces.current().dialog().showMessageDynamic(message);
        }
    }


    public void delete(ConfigurationSUT configurationSUT) {
        try {
            this.configurationService.deleteConfiguration(configurationSUT);
        } catch (NotFoundException e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error Not Found", "Configuration not found. The configuration does not exist!");
            PrimeFaces.current().dialog().showMessageDynamic(message);
        }
    }

    public void openNew() {
        this.setSelectedConfigurationSUT(ConfigurationSUT.builder().withName("MyConf").withHost("127.0.0.1").withPort("8080").withApplication("myApp").withFacility("myFacility").withOwner(getPrincipal.getUsername()).withCompanyOwner(getPrincipal.getOrganizationKeyword()).withShare(false).withCharset("UNICODE UTF-8")
                .build());

    }

    public ConfigurationSUT getSelectedConfigurationSUT() {
        return this.selectedConfigurationSUT;
    }

    public void setSelectedConfigurationSUT(ConfigurationSUT selectedConfigurationSUT) {
        this.selectedConfigurationSUT = selectedConfigurationSUT;
        this.host = selectedConfigurationSUT.getHost();
        this.name = selectedConfigurationSUT.getName();
        this.port = selectedConfigurationSUT.getPort();
        this.application = selectedConfigurationSUT.getApplication();
        this.facility = selectedConfigurationSUT.getFacility();
        this.owner = selectedConfigurationSUT.getOwner();
        this.companyOwner = selectedConfigurationSUT.getCompanyOwner();
        this.share = selectedConfigurationSUT.isShare();
        this.charset = selectedConfigurationSUT.getCharset();
    }

    public void deleteSelectedConfigurationSUT() {
        this.configurationService.deleteConfiguration(this.getSelectedConfigurationSUT());
        PrimeFaces.current().ajax().update("form:messages", "form:dt-configurations");
    }

    public void insertUpdateSelectedConfigurationSUT() {
        ConfigurationSUT updatedConf = ConfigurationSUT.builder()
                .withId(this.selectedConfigurationSUT.getId())
                .withHost(getHost())
                .withName(getName())
                .withPort(getPort())
                .withApplication(getApplication())
                .withFacility(getFacility())
                .withOwner(this.selectedConfigurationSUT.getOwner())
                .withCompanyOwner(this.selectedConfigurationSUT.getCompanyOwner())
                .withShare(isShare())
                .withCharset(getCharset())
                .build();

        if (updatedConf.getId() != 0) {

            updateConfiguration(updatedConf);

        } else {

            insertConfiguration(updatedConf);


        }
        PrimeFaces.current().executeScript("PF('manageConfigurationDialog').hide()");
        PrimeFaces.current().ajax().update("form:messages", "form:dt-configurations");

    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getCompanyOwner() {
        return companyOwner;
    }

    public void setCompanyOwner(String companyOwner) {
        this.companyOwner = companyOwner;
    }

    public boolean isShare() {
        return share;
    }

    public void setShare(boolean share) {
        this.share = share;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }


    public String[] getCharsets() {
        return charsets;
    }
}