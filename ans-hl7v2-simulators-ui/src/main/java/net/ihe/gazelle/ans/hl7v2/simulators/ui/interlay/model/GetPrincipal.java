package net.ihe.gazelle.ans.hl7v2.simulators.ui.interlay.model;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.RequestScoped;
import jakarta.faces.context.ExternalContext;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Named;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apereo.cas.client.authentication.AttributePrincipal;
import org.apereo.cas.client.util.AbstractCasFilter;
import org.apereo.cas.client.validation.Assertion;

import javax.security.auth.Subject;
import java.io.IOException;
import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 20/06/2023
 */
@Named
@RequestScoped
public class GetPrincipal implements Serializable {

    @Serial
    private static final long serialVersionUID = 4334713859397066659L;
    private static final String ROLE_NAME = "role_name";
    private boolean loggedIn = false;
    private boolean casEnabled = false;
    private String username;
    private String firstname;
    private String lastname;
    private String organizationKeyword; // institution_keyword
    private List<String> roles = new ArrayList<>(); // role_name
    private final Subject subject = new Subject();
    private AttributePrincipal principal;



    @PostConstruct
    public void init() {
        principal = getPrincipal();
        if (principal != null) {
            loggedIn = true;
            username = principal.getName();
            firstname = (String) principal.getAttributes().get("firstname");
            lastname = (String) principal.getAttributes().get("lastname");
            organizationKeyword = (String) principal.getAttributes().get("institution_keyword");
            roles = parseRoles((String) principal.getAttributes().get(ROLE_NAME));
        }
    }


    public boolean checkAlreadyLogged() throws IOException {
        principal = getPrincipal();
        if (principal != null) {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/token.xhtml");
            return true;
        }
        return false;
    }

    public boolean isAdmin() {
        roles = parseRoles((String) principal.getAttributes().get(ROLE_NAME));
        for (String role : roles) {
            if (role.trim().equals("admin_role")) {
                return true;
            }
        }
        return false;
    }


    public boolean checkIsAdmin() throws IOException {
        if (!isAdmin()) {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/index.xhtml");
            return true;
        }
        return false;
    }

    private AttributePrincipal getPrincipal() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context == null) {
            return null;
        }
        final HttpServletRequest request = (HttpServletRequest) context.getExternalContext()
                .getRequest();
        final HttpSession session = request.getSession(false);
        Assertion assertion = (Assertion) (session == null ? request
                .getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION) : session
                .getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION));
        return assertion != null ? assertion.getPrincipal() : null;
    }

    private List<String> parseRoles(String roles) {
        List<String> rolesList = new ArrayList<>();
        final StringTokenizer st = new StringTokenizer(roles, "[,]");
        String role;
        while (st.hasMoreElements()) {
            role = (String) st.nextElement();
            role = StringUtils.trimToNull(role);
            if (role != null) {
                rolesList.add(role);
            }
        }
        return rolesList;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public String getUsername() {
        return this.username;
    }

    public String getOrganizationKeyword() {
        return organizationKeyword;
    }

    public List<String> getRoles() {
        return roles;
    }


    public Subject getSubject() {
        return subject;
    }


    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public boolean isCasEnabled() {
        return casEnabled;
    }

    public void setCasEnabled(boolean casEnabled) {
        this.casEnabled = casEnabled;
    }




}