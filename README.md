# ans-hl7v2-simulators



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.inria.fr/gazelle/specific-tools/ans/ans-hl7v2-simulators.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.inria.fr/gazelle/specific-tools/ans/ans-hl7v2-simulators/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***


## Name
hl7v2-simulators
## Description
This application contains two simulators that simulate the transmission of HL7v2 messages.
The first simulator mimics the transmission of a Clinical Document Architecture (CDA) to a receiving application. The creator of the simulator expects to receive as many acknowledgments as requested by the message.
The second simulator acts as a server, receiving any CDA message sent to it. It then sends back as many acknowledgments as requested in the message to its sender.



## Installation
First you should pull the project from:
https://gitlab.inria.fr/gazelle/specific-tools/ans/ans-hl7v2-simulators.git
then you can use quarkus with :
Be careful you must have a gazelle-database started with an ans-hl7v2-database  to use it this way
``
mvn clean package
cd /ans-hl7v2-simulators-ui/
mvn quarkus:dev
``
Or
you can use the application  with docker with
``
mvn clean install
``
or even use the industrialization project that you can pull there
https://gitlab.inria.fr/gazelle/private/industrialization/docker/gazelle-hl7v2-simulators.git


## Usage

CONFIGURATION  
Before using the application there must be configuration define matching with the system you want to test.
Configuration Sut don't configure servers used by the application, they are configure by preferences within the application. 


TEMPLATE  
Furthermore, you must also have template define to be able to send any message with the creator. However, business acknowledgment(ZAM) should be on defined with template on the storage disk, they
are stock in the /opt/ans-hl7v2-simulators/template/ZAM.  
There are 3 different ZAM that cover 3 various cases.  
ZAM_LCT_OK.mustache cover the case where the creator expect a lecture acknowledgment.  
ZAM_RCV_KO.mustache cover the case where the creator expect a reception acknowledgment with an error.  
-> to ask this zam your sent message should have a segment like this one:
OBX|12|CE|ACK_RECEPTION^Accusé de réception^MetaDMPMSS||Y^ERROR^HL70136||||||F|

The important values are ACK_RECEPTION  and Y^ERROR in a nutshell this segment ask an ACK_RECEPTION to be sent with ACK_RECEPTION and Y and in error with ERROR. 

ZAM_RCV_OK.mustache cover the case where the creator expect a valid acknowledgment.  
In any case if there are missing ZAM a message should be print in the manager UI.

In the message send by the creator there are two segments that define how many ZAM should be replied :  
ACK_RECEPTION define if the creator expect reception acknowledgment.  
ACK_LECTURE define if the creator expect
If you are using the industrialization project you can set the environment variable DEFAULT_ANS_TEMPLATES to true to have defaults templates CDA and ZAM provides by the application.
An administrator can also add custom Configuration SUT and template.
Custom template can be uploaded from local disk as long as the file imported is a mustache Template (.mustache) and no file with this name already exist.
The header of the template should be of shape of
MSH|^˜\&|{{applicationSupplier}}|{{facilitySupplier}}|{{applicationReceiver}}|{{facilityReceiver}}|{{idMessage}}||ORU^R01^ORU_R01|{{idMessage}}|P|2.5|||||FRA|UNICODE UTF-8|||2.0^CISIS_CDA_HL7_V2 .
Fields in the header Application Supplier, Facility Supplier, Application Receiver and Facility Receiver are set by the information of Configuration SUT.
idMessage is set by a timestamp and is not mandatory but it can avoid duplicate  or incoherent id.

MANAGER

Before using the manager be sure that the SUT Configuration name local_manager_configuration is correctly define.  
For replying ZAM the manager must have all required ZAM templates. See TEMPLATE section for details.  
For replying ZAM the manger should also have the configuration SUT defined of the sender. Else the manager cannot know where to send ZAM ( because ACK with HAPI is automatic).  
For starting the server manager just go to the Manger GUI and press the start button.  
Once the server is started it will try parsing every incoming messages and reply ACK/NACK AND corresponding ZAM.
The validation process is made by HL7validator with is call by a webservice. Note that the endpoint of the webservice should be set in preference with the preference name Endpoint_Validator.
Matching profil will be used for each kind of message.


![Validation_Profil_Used.png](Validation_Profil_Used.png)

CREATOR  
Before using the creator be sure that the SUT Configuration name local_creator_configuration is correctly define.    
For sending the receiving application Configuration must have been insert in the configuration list.  
On the Creator GUI before sending, a Configuration Receiver and a Template must have been selected.  
Once the message is sent the message history print messages who have been sent and receive including ACK/NACK AND ZAM.  1

PREFERENCES

Preferences enable to custom the working of the application there are 3 things that can be custom.
    Endpoint_Validator: The validation Endpoint to define which HL7 validator should be used.
    Manager_Name, Manager_Host, Manager_Application, Manager_Facility, Manager_Port	:The configuration of the manager
    Creator_Name, Creator_Host ,Creator_Application , Creator_Port:The configuration of the creator

## Support
There is a documentation link who are initialize at the start of the application. You should see the documentation section on the footer of the application.


## Authors and acknowledgment
Made by Claude LUSSEAU and Romuald DUBOURG.  
