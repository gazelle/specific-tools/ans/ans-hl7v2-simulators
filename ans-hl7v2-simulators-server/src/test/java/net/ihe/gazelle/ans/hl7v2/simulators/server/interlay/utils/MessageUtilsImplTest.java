package net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.utils;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v26.segment.MSH;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 10/07/2023
 */
class MessageUtilsImplTest {

    MessageUtilsImpl utils = new MessageUtilsImpl();

    @BeforeEach
    void init() {
        utils = new MessageUtilsImpl();
    }

    @Test
    void readMessage_OK() throws HL7Exception {
        File file = new File("src/test/resources/messageTest1.hl7").getAbsoluteFile();
        Message message = utils.readMessage(file);
        Assertions.assertFalse(message.isEmpty());
    }

    @Test
    void messageToString_OK() {
        File file = new File("src/test/resources/messageTest3.hl7").getAbsoluteFile();
        Message message = utils.readMessage(file);
        String valueString = utils.messageToString(message);
        assertEquals("MSH|^~\\&|Ntierprise|Ntierprise Clinic|Healthmatics EHR|Healthmatics Clinic|20190423113910||ADT^A08|8899-39|P|2.6|||NE|NE\n", valueString);
    }

    @Test
    void getSplitMessageString_OK() {
        File file = new File("src/test/resources/messageTest1.hl7").getAbsoluteFile();
        Message message = utils.readMessage(file);
        String valueString = utils.messageToString(message);
        List<String> messages = utils.getSplitMessageString(valueString);
        assertEquals(6, messages.size());
    }

    @Test
    void getOxbList_OK() {
        File file = new File("src/test/resources/messageTest4.hl7").getAbsoluteFile();
        Message message = utils.readMessage(file);
        List<String> obxList = utils.getOxbList(message);
        assertEquals(7, obxList.size());
    }

    @Test
    void getPrtList_OK() {
        File file = new File("src/test/resources/messageTest4.hl7").getAbsoluteFile();
        Message message = utils.readMessage(file);
        List<String> obxList = utils.getPRTList(message);
        assertEquals(4, obxList.size());
    }

    @Test
    void getReadACK_OK() {
        File file = new File("src/test/resources/messageTest5.hl7").getAbsoluteFile();
        Message message = utils.readMessage(file);
        assertTrue(utils.shouldSendZ01(message));
    }

    @Test
    void getReceivedACK_OK() {
        File file = new File("src/test/resources/messageTest5.hl7").getAbsoluteFile();
        Message message = utils.readMessage(file);
        assertTrue(utils.shouldSendZ02(message));
    }

    @Test
    void getReceivedACK2_OK() {
        File file = new File("src/test/resources/messageTest5.hl7").getAbsoluteFile();
        Message message = utils.readMessage(file);
        assertTrue(utils.shouldSendZ03(message));
    }

    @Test
    void getReceivedACK3_OK() {
      File file = new File("src/test/resources/messageTest6.hl7").getAbsoluteFile();
       Message message = utils.readMessage(file);
      assertTrue(utils.shouldSendZ01(message));
       assertTrue(utils.shouldSendZ02(message));
       assertTrue(utils.shouldSendZ03(message));


    }

    @Test
    void getMSH_OK() {
        File file = new File("src/test/resources/messageTest3.hl7").getAbsoluteFile();
        Message message = utils.readMessage(file);
        MSH msh = utils.getMSH(message);
        assertNotNull(msh);
    }

    @Test
    void getIdMSH_OK() {
        File file = new File("src/test/resources/messageTest3.hl7").getAbsoluteFile();
        Message message = utils.readMessage(file);
        String id =utils.getIdMessage(message);
        Assertions.assertEquals("8899-39",id);
    }

    @Test
    void getTransactionMSH_OK() {
        File file = new File("src/test/resources/messageTest4.hl7").getAbsoluteFile();
        Message message = utils.readMessage(file);
        String transaction =utils.getTransaction(message);
        Assertions.assertEquals("[EI[1.0^CISIS_CDA_HL7_LPS\n" +
                "EVN]]",transaction);
    }


    @Test
    void getTypeMessageMSH_OK() {
        File file = new File("src/test/resources/messageTest3.hl7").getAbsoluteFile();
        Message message = utils.readMessage(file);
        String typeMessage =utils.getTypeMessage(message);
        Assertions.assertEquals("ADT^A08",typeMessage);
    }


    @Test
    void shouldSendError(){
        File file = new File("src/test/resources/messageTest3.hl7").getAbsoluteFile();
        Message message = utils.readMessage(file);
        assertFalse(utils.shouldSendError(message));
    }

    @Test
    void matchingJDV_OK() {
        assertTrue(utils.matchingJDV("Autorisation de soins et actes non usuels sanitaires","AUTORIS-SOINS","1.2.250.1.213.1.1.4.12","src/test/resources/JDV_J66-TypeCode-DMP.csv"));
    }

    @Test
    void matchingJDV_KO() {
        assertFalse(utils.matchingJDV("Autorisation de sains et actes non usuels sanitaires","AUTORIS-SOINS","1.2.250.1.213.1.1.4.12","src/test/resources/JDV_J66-TypeCode-DMP.csv"));
    }

    @Test
    void checkMSSante_OK() {
        File file = new File("src/test/resources/messageTest4.hl7").getAbsoluteFile();
        Message message = utils.readMessage(file);
        assertTrue(utils.checkMSSante(message));
    }

    @Test
    void getIDFormatted_OK() {
        String idFormatted = utils.getIdFormatted();
        assertEquals(18, idFormatted.length());
    }

    @Test
    void getDateFormatted_OK() {
        String dateFormatted = utils.getDateFormatted();
        assertEquals(19, dateFormatted.length());
    }

    @Test
    void isValidEmailDomain_OK() {
        assertTrue(MessageUtilsImpl.isValidEmailDomain("test@mssante.fr", "mssante.fr"));
        assertFalse(MessageUtilsImpl.isValidEmailDomain("test@test.fr", "mssante.fr"));
    }

    @Test
    void generatedACK() throws HL7Exception, IOException {
        File file = new File("src/test/resources/messageTest3.hl7").getAbsoluteFile();
        Message message = utils.readMessage(file);
        Message ack = utils.generateAck(message);
        assertNotNull(ack);
    }
}