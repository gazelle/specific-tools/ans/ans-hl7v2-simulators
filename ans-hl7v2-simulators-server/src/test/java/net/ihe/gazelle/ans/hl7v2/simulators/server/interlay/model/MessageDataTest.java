package net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.model;

import ca.uhn.hl7v2.model.Message;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.utils.MessageUtilsImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;
/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
 class MessageDataTest {

    private static MessageData messageData;
    MessageUtilsImpl utils = new MessageUtilsImpl();


    @BeforeAll
    static void init() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy[MM[dd[HH[mm[ss]]]]][XXX]");
        ZoneId zone = ZoneId.of(System.getenv("TIME_ZONE")!=null? System.getenv("TIME_ZONE") : "UTC");
        ZonedDateTime now = ZonedDateTime.now(zone);
        String formattedTimestamp = now.format(formatter);

        messageData = MessageData.builder()
                .withData("Message")
                .withApplicationReceiver("AppReceiver")
                .withFacilityReceiver("FacReceiver")
                .withApplicationSupplier("AppSupplier")
                .withFacilitySupplier("FacSupplier")
                .withTypeMessage("MSH")
                .withIdMessage(formattedTimestamp)
                .withIdMessageOrigin("001").build();
    }

    @Test
    void getter_OK() {
        assertEquals("Message", messageData.getData());
        assertEquals("MSH", messageData.getTypeMessage());
        assertEquals("AppReceiver", messageData.getApplicationReceiver());
        assertEquals("FacReceiver", messageData.getFacilityReceiver());
        assertEquals("AppSupplier", messageData.getApplicationSupplier());
        assertEquals("FacSupplier", messageData.getFacilitySupplier());
        assertEquals("001", messageData.getIdMessageOrigin());

    }

    @Test
    void messageToMessageToMessageData_OK() {
        File file = new File("src/test/resources/messageTest1.hl7").getAbsoluteFile();
        Message message = utils.readMessage(file);
        MessageData messageData1 = MessageData.messageToMessageData(message);
        assertEquals("Ntierprise", messageData1.getApplicationSupplier());
        assertEquals("Ntierprise Clinic", messageData1.getFacilitySupplier());
        assertEquals("Healthmatics EHR", messageData1.getApplicationReceiver());
        assertEquals("Healthmatics Clinic", messageData1.getFacilityReceiver());
    }
}
