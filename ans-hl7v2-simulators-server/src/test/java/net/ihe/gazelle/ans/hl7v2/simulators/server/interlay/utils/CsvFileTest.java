package net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.utils;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.List;

import org.junit.jupiter.api.Assertions;

/**
 * @author Romuald DUBOURG
 * @company KEREVAL
 * @project wado-errors-manager
 * @date 06/09/2023
 */


@QuarkusTest
 class CsvFileTest {
    private static final String FILE_PATH =  "src/test/resources/JDV_J66-TypeCode-DMP.csv";
    static File file;
    static CsvFile csvFile;

    @BeforeEach
    public  void beforeClass() {
        file = new File(FILE_PATH);
         csvFile = new CsvFile(file);
    }

    @Test
     void testFile() {
        // Test
        Assertions.assertEquals(file, csvFile.getFile());
    }

    @Test
     void testCsvFile() {

        // Result
        final int nombreLigne = 91;

        // Appel
        final List<String> lines = csvFile.getLines();

        // Test
        Assertions.assertEquals(nombreLigne, lines.size());
    }

    @Test
     void testData() {

        // Result
        final int nombreLigne =91 ;
        final int nombreColonnes =3 ;

        // Appel
        final List<String[]> data = csvFile.getData();

        // Test
        Assertions.assertEquals(nombreLigne, data.size());

        for (String[] oneData : data) {
            Assertions.assertEquals(nombreColonnes, oneData.length);
        }

    }
}
