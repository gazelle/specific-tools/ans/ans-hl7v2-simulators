package net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.utils;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v26.message.ACK;
import ca.uhn.hl7v2.model.v26.segment.MSH;
import jakarta.enterprise.context.ApplicationScoped;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.CustomHl7Exception;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.HapiContextClosingException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.utils.MessageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 07/07/2023
 */
@ApplicationScoped
public class MessageUtilsImpl implements MessageUtils<Message, MSH> {

    public static final String ACK_RECEPTION = "ACK_RECEPTION";

    public static final String DEST_MS_SANTE_PAT = "DESTMSSANTEPAT";
    public static final String DEST_MS_SANTE_PS = "DESTMSSANTEPS";
    public static final String TIME_ZONE = "TIME_ZONE";
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageUtilsImpl.class);

    static boolean isValidEmailDomain(String email, String domainToCheck) {
        // Expression régulière pour vérifier le format de l'adresse e-mail
        String emailRegex = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}\\b";
        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(email);

        // Vérifier si le format de l'adresse e-mail est valide
        if (matcher.find()) {
            String[] parts = email.split("@");
            String domain = parts[1];
            return domain.endsWith(domainToCheck);
        }

        // Vérifier si le domaine correspond au domaine spécifié
        return true;

    }

    @Override
    public Message readMessage(File hl7File) {
        Message hl7Message = null;
        try (FileInputStream fis = new FileInputStream(hl7File);
             HapiContext hapiContext = new CustomHapiContext("");) {

            byte[] buffer = new byte[(int) hl7File.length()];
            if (fis.read(buffer) == 0) throw new CustomHl7Exception("File cannot be read.");
            String hl7MessageString = new String(buffer);

            hl7Message = hapiContext.getGenericParser().parse(hl7MessageString);
            LOGGER.info("HL7 message transformation successful!");
        } catch (Exception e) {
            LOGGER.error("Error Read message: ", e);
        }
        return hl7Message;
    }

    @Override
    public ACK generateAck(Message message) throws HL7Exception, IOException {
        MSH mshOrigin = getMSH(message);


        String idFormattedTimestamp = getIdFormatted();
        String dateFormattedTimestamp = getDateFormatted();

        ACK ackMessage = (ACK) message.generateACK();
        // Modify MSH-17 and MSH-18 in the ACK message
        MSH msh = ackMessage.getMSH();
        msh.getCountryCode().setValue("FRA");  // Setting MSH-17
        msh.getCharacterSet(0).setValue(mshOrigin.getCharacterSet(0).getValue()); // Setting MSH-18
        msh.getMsh7_DateTimeOfMessage().setValue(dateFormattedTimestamp);
        msh.getMsh10_MessageControlID().setValue(idFormattedTimestamp);
        return ackMessage;
    }
    @Override
    public String getIdFormatted(){
        DateTimeFormatter idFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss.SSS");
        ZoneId zone = ZoneId.of(System.getenv(TIME_ZONE) != null ? System.getenv(TIME_ZONE) : "UTC");
        ZonedDateTime now = ZonedDateTime.now(zone);
        return now.format(idFormatter);
    }
    @Override
    public String getDateFormatted(){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssZ");
        ZoneId zone = ZoneId.of(System.getenv(TIME_ZONE) != null ? System.getenv(TIME_ZONE) : "UTC");
        ZonedDateTime now = ZonedDateTime.now(zone);
        return now.format(dateTimeFormatter);
    }

    @Override
    public MSH getMSH(Message message) {
        MSH segment;
        try {
            segment = (MSH) message.get("MSH");
        } catch (HL7Exception e) {
            throw new CustomHl7Exception("Can't find MSH segment", e);
        }
        return segment;
    }

    @Override
    public String getIdMessage(Message message) {
        MSH segment;
        try {
            segment = (MSH) message.get("MSH");

        } catch (HL7Exception e) {
            throw new CustomHl7Exception("Can't find MSH segment for ID message", e);
        }
        return segment.getMessageControlID().getValue();
    }

    @Override
    public String getTransaction(Message message) {
        MSH segment;
        try {
            segment = (MSH) message.get("MSH");

        } catch (HL7Exception e) {
            throw new CustomHl7Exception("Can't find MSH segment for Transaction", e);
        }
        return Arrays.toString(segment.getMsh21_MessageProfileIdentifier());
    }

    @Override
    public String getTypeMessage(Message message) {
        MSH segment;
        try {
            segment = (MSH) message.get("MSH");

        } catch (HL7Exception e) {
            throw new CustomHl7Exception("Can't find MSH segment for type Message", e);
        }
        try {
            return segment.getMessageType().encode();
        } catch (HL7Exception e) {
            throw new CustomHl7Exception(e);
        }
    }

    @Override
    public List<String> getOxbList(Message message) {
        List<String> obxList = new ArrayList<>();
        List<String> messages = getSplitMessageString(messageToString(message));
        for (String mes : messages) {
            String name = getSegmentName(mes);
            if (Objects.equals(name, "OBX")) {
                obxList.add(mes);
            }
        }
        if (obxList.isEmpty()) {
            return Collections.emptyList();
        }
        return obxList;
    }

    @Override
    public List<String> getPRTList(Message message) {
        List<String> prtList = new ArrayList<>();
        List<String> messages = getSplitMessageString(messageToString(message));
        for (String mes : messages) {
            String name = getSegmentName(mes);
            if (name.startsWith("PRT")) {
                prtList.add(mes);
            }
        }
        if (prtList.isEmpty()) {
            return Collections.emptyList();
        }
        return prtList;
    }

    @Override
    public String messageToString(Message message) {

        String parsed;
        try (HapiContext context = new CustomHapiContext("");) {
            parsed = context.getPipeParser().encode(message);

            if (parsed.contains("\r")) {
                return parsed.replace("\r", "\n");
            } else if (parsed.contains("\r\n")) {
                return parsed.replace("\r\n", "\n");
            } else {
                return parsed;
            }
        } catch (HL7Exception e) {
            throw new CustomHl7Exception("Can't parse Message to String", e);
        } catch (IOException e) {
            throw new HapiContextClosingException();
        }
    }

    @Override
    public List<String> getSplitMessageString(String message) {
        List<String> messages = new ArrayList<>();
        String[] values = message.split(System.lineSeparator());
        Collections.addAll(messages, values);
        return messages;
    }

    @Override
    public String getSegmentName(String segment) {
        String[] tabs = segment.split("\\|");
        return tabs[0];
    }

    public String[] getTerserSegment(String value) {
        return value.split("\\|");
    }

    public String[] getTabsTerserSegment(String value) {
        return value.split("\\^");
    }

    @Override
    public boolean shouldSendZ01(Message message) {

        List<String> obsList = getOxbList(message);
        List<String> results = obsList
                .stream()
                .filter(x -> getTabsTerserSegment(getTerserSegment(x)[3])[0].equals("DESTDMP"))
                .toList();

        List<String> results2 = obsList
                .stream()
                .filter(x -> getTabsTerserSegment(getTerserSegment(x)[3])[0].equals(ACK_RECEPTION))
                .toList();
        if (results.isEmpty() || results2.isEmpty()) {
            return false;
        }

        return isSetToY(results) && (isSetToY(results2));
    }

    @Override
    public boolean shouldSendZ02(Message message) {
        List<String> obsList = getOxbList(message);
        List<String> results = obsList
                .stream()
                .filter(x -> getTabsTerserSegment(getTerserSegment(x)[3])[0].equals(DEST_MS_SANTE_PAT) || getTabsTerserSegment(getTerserSegment(x)[3])[0].equals(DEST_MS_SANTE_PS))
                .toList();
        List<String> results2 = obsList
                .stream()
                .filter(x -> getTabsTerserSegment(getTerserSegment(x)[3])[0].equals(ACK_RECEPTION))
                .toList();


        return isSetToY(results) && (isSetToY(results2));
    }

    @Override
    public boolean shouldSendZ03(Message message) {

        List<String> obsList2 = getOxbList(message);
        List<String> results2 = obsList2
                .stream()
                .filter(x -> getTabsTerserSegment(getTerserSegment(x)[3])[0].equals("ACK_LECTURE_MSS"))
                .toList();

        for (String result2 : results2) {

            if (getTabsTerserSegment(getTerserSegment(result2)[5])[0].startsWith("Y") || getTabsTerserSegment(getTerserSegment(result2)[5])[0].equals("Y")) {
                if (this.getTransaction(message).contains("CISIS_CDA_HL7_LPS")) return true;

                List<String> obsList = getOxbList(message);
                List<String> results = obsList
                        .stream()
                        .filter(x -> getTabsTerserSegment(getTerserSegment(x)[3])[0].equals(DEST_MS_SANTE_PAT) || getTabsTerserSegment(getTerserSegment(x)[3])[0].equals(DEST_MS_SANTE_PS))
                        .toList();

                if (extracted(results)) return true;


            }
        }
        return false;
    }

    private boolean extracted(List<String> results) {
        for (String result : results) {
            if (getTabsTerserSegment(getTerserSegment(result)[5])[0].startsWith("Y") || getTabsTerserSegment(getTerserSegment(result)[5])[0].equals("Y")) {
                return true;

            }
        }
        return false;
    }

    public boolean shouldSendError(Message message) {
        List<String> obsList = getOxbList(message);
        List<String> results = obsList
                .stream()
                .filter(x -> getTabsTerserSegment(getTerserSegment(x)[3])[0].equals(ACK_RECEPTION))
                .toList();

        if (results.isEmpty()) {
            return false;
        }
        for (String result : results) {
            if ((getTabsTerserSegment(getTerserSegment(result)[5])[0].startsWith("Y") && getTabsTerserSegment(getTerserSegment(result)[5])[1].equals("ERROR"))) {
                return true;
            }
        }
        return false;
    }

    public boolean isSetToY(List<String> results) {
        for (String result : results) {
            if (getTabsTerserSegment(getTerserSegment(result)[5])[0].startsWith("Y") || getTabsTerserSegment(getTerserSegment(result)[5])[0].equals("Y"))
                return true;
        }
        return false;
    }

    @Override
    public boolean checkDMPDest(Message message) {

        String obx = getOxbList(message).get(0);
        String code = getTabsTerserSegment(getTerserSegment(obx)[3])[0];
        String name = (getTabsTerserSegment(getTerserSegment(obx)[3])[1]);
        String oidRaw = (getTabsTerserSegment(getTerserSegment(obx)[3])[2]);
        String oid;


        if (oidRaw.equals("LN")) oid = "2.16.840.1.113883.6.1";
        else if (oidRaw.equals("TREA05")) oid = "1.2.250.1.213.1.1.4.12";
        else oid = "ASTM";

        return matchingJDV(name, code, oid, "/opt/ans-hl7v2-simulators/JDV/JDV_J66-TypeCode-DMP.csv");
    }

    public boolean matchingJDV(String name, String code, String oid, String filePath) {
        try {
            File file = new File(filePath);

            final CsvFile csvFile = new CsvFile(file);
            final List<String[]> data = csvFile.getData();

            for (String[] oneData : data) {

                if (oneData[2].equals(name) && (oneData[1].equals(code) && oneData[0].equals(oid))) {
                    return true;

                }
            }
            return false;

        } catch (Exception e) {
            return false;
        }

    }

    public boolean checkMSSante(Message message) {
        List<String> obsList = getOxbList(message);
        List<String> results = obsList
                .stream()
                .filter(x -> getTabsTerserSegment(getTerserSegment(x)[3])[0].equals(DEST_MS_SANTE_PAT) || getTabsTerserSegment(getTerserSegment(x)[3])[0].equals(DEST_MS_SANTE_PS))
                .toList();

        List<String> prtList = getPRTList(message);
        if (isSetToY(results)) {
            for (String prt : prtList) {
                if (!isValidEmailDomain(prt, ".mssante.fr") && !isValidEmailDomain(prt, "interop-mssante.apicrypt.org"))
                    return false;
            }
        }
        return true;
    }

}

