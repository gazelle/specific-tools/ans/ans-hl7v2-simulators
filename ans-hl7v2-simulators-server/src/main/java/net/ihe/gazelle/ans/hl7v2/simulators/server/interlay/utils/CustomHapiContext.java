package net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.utils;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.llp.MinLowerLayerProtocol;
import ca.uhn.hl7v2.parser.CanonicalModelClassFactory;
import ca.uhn.hl7v2.parser.GenericModelClassFactory;
import ca.uhn.hl7v2.validation.builder.support.NoValidationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public class CustomHapiContext extends DefaultHapiContext {

    private static final Logger LOG = LoggerFactory.getLogger(CustomHapiContext.class);
    public CustomHapiContext(String charset) {
        super(new GenericModelClassFactory());
        this.setValidationRuleBuilder(new NoValidationBuilder());
        MinLowerLayerProtocol mllp = new MinLowerLayerProtocol();
        if (charset.equals("UNICODE UTF-8")) {
            LOG.error("UTF-8");
            mllp.setCharset("UTF-8");
        }

        if (charset.equals("8859/15")){
            LOG.error("ISO-8859-15");

            mllp.setCharset("ISO-8859-15");}
        this.setLowerLayerProtocol(mllp);
        this.getParserConfiguration().setAllowUnknownVersions(true);
        CanonicalModelClassFactory mcf = new CanonicalModelClassFactory("2.6");
        this.setModelClassFactory(mcf);
    }

}
