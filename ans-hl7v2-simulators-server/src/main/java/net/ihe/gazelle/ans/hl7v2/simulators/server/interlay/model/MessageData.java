package net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.model;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v26.segment.MSH;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.CustomHl7Exception;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.HapiContextClosingException;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.utils.CustomHapiContext;

import java.io.IOException;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public class MessageData {


    private final String applicationSupplier;
    private final String facilitySupplier;
    private final String applicationReceiver;
    private final String facilityReceiver;
    private final String typeMessage;
    private final String dateMessage;
    private final String idMessage;
    private final String idMessageOrigin;
    private final String charset;
    private final String data;

    public MessageData(MessageDataBuilder builder) {
        this.applicationSupplier = builder.applicationSupplier;
        this.facilitySupplier = builder.facilitySupplier;
        this.applicationReceiver = builder.applicationReceiver;
        this.facilityReceiver = builder.facilityReceiver;
        this.typeMessage = builder.typeMessage;
        this.dateMessage = builder.dateMessage;
        this.idMessage = builder.idMessage;
        this.idMessageOrigin = builder.idMessageOrigin;
        this.charset = builder.charset;
        this.data = builder.data;

    }

    public static MessageDataBuilder builder() {
        return new MessageDataBuilder();
    }

    public static MessageData messageToMessageData(Message message) {
        MSH msh = null;
        String rawMessage = "";
        try {

            msh = (MSH) message.get("MSH");
            rawMessage = message.encode();
            return messageToMessageData(msh, rawMessage);

        } catch (HL7Exception e) {
            throw new CustomHl7Exception("Can't build MessageData", e);
        }

    }

    public static MessageData messageToMessageData(MSH msh, String rawMessage) {

        try (HapiContext context = new CustomHapiContext(msh.getField(18, 0).encode())) {
            return builder()
                    .withApplicationSupplier(msh.getSendingApplication().encode())
                    .withFacilitySupplier(msh.getSendingFacility().encode())
                    .withApplicationReceiver(msh.getReceivingApplication().encode())
                    .withFacilityReceiver(msh.getReceivingFacility().encode())
                    .withTypeMessage(msh.getMsh9_MessageType().encode())
                    .withDateMessage(msh.getMsh7_DateTimeOfMessage().encode())
                    .withIdMessage(msh.getMessageControlID().encode())
                    .withCharset(msh.getField(18, 0).encode())
                    .withData(rawMessage)
                    .build();
        } catch (HL7Exception | IOException e) {
            throw new HapiContextClosingException();
        }

    }

    public String getApplicationSupplier() {
        return applicationSupplier;
    }

    public String getFacilitySupplier() {
        return facilitySupplier;
    }

    public String getApplicationReceiver() {
        return applicationReceiver;
    }

    public String getFacilityReceiver() {
        return facilityReceiver;
    }

    public String getTypeMessage() {
        return typeMessage;
    }

    public String getDateMessage() {
        return dateMessage;
    }

    public String getIdMessage() {
        return idMessage;
    }

    public String getIdMessageOrigin() {
        return idMessageOrigin;
    }

    public String getData() {
        return data;
    }

    public String getCharset() {
        return charset;
    }

    public static class MessageDataBuilder {
        private String applicationSupplier;
        private String facilitySupplier;
        private String applicationReceiver;
        private String facilityReceiver;
        private String typeMessage;
        private String dateMessage;
        private String idMessage;
        private String idMessageOrigin;
        private String charset;
        private String data;

        public MessageDataBuilder withApplicationSupplier(String applicationSupplier) {
            this.applicationSupplier = applicationSupplier;
            return this;

        }

        public MessageDataBuilder withFacilitySupplier(String facilitySupplier) {
            this.facilitySupplier = facilitySupplier;
            return this;

        }

        public MessageDataBuilder withApplicationReceiver(String applicationReceiver) {
            this.applicationReceiver = applicationReceiver;
            return this;

        }

        public MessageDataBuilder withFacilityReceiver(String facilityReceiver) {
            this.facilityReceiver = facilityReceiver;
            return this;

        }

        public MessageDataBuilder withTypeMessage(String typeMessage) {
            this.typeMessage = typeMessage;
            return this;

        }

        public MessageDataBuilder withDateMessage(String dateMessage) {
            this.dateMessage = dateMessage;
            return this;

        }


        public MessageDataBuilder withIdMessage(String idMessage) {
            this.idMessage = idMessage;
            return this;

        }

        public MessageDataBuilder withIdMessageOrigin(String idMessageOrigin) {
            this.idMessageOrigin = idMessageOrigin;
            return this;

        }

        public MessageDataBuilder withCharset(String charset) {
            this.charset = charset;
            return this;
        }


        public MessageDataBuilder withData(String data) {
            this.data = data;
            return this;
        }


        public MessageData build() {
            return new MessageData(this);
        }
    }

}

