package net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.exception;
/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public class InvalideMessageException extends RuntimeException{

    public InvalideMessageException(String message) {
        super(message);
    }
}
