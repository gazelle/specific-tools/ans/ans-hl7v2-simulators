package net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.server;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.app.Connection;
import ca.uhn.hl7v2.app.HL7Service;
import ca.uhn.hl7v2.app.Initiator;
import ca.uhn.hl7v2.llp.LLPException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v26.segment.MSH;
import ca.uhn.hl7v2.protocol.ReceivingApplication;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import jakarta.enterprise.context.ApplicationScoped;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.CustomHl7Exception;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.Hl7ConnexionException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.server.CreatorServer;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.utils.MessageUtils;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.TemplateHl7v2;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.model.MessageData;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.protocol.CustomReceiverClientApplication;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.utils.CustomHapiContext;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.utils.MessageUtilsImpl;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
@ApplicationScoped
public class CreatorServerImpl implements CreatorServer<MessageData> {


    List<MessageData> messageHistory = new ArrayList<>();
    private HL7Service server;

    MessageUtils<Message, MSH> utils = new MessageUtilsImpl();

    public CreatorServerImpl() {
        this.messageHistory = new ArrayList<>();
    }

    @Override
    public void sendMessage(ConfigurationSUT configuration, TemplateHl7v2 templateHl7v2, ConfigurationSUT configurationSUTCreator) throws InterruptedException {
        HapiContext context = new CustomHapiContext(configuration.getCharset());
        startCreatorServer(configurationSUTCreator, context);

        Message cda = buildMessage(configuration, configurationSUTCreator, templateHl7v2, context);
        sendAndReceive(cda, configuration, context);


    }

    public void resetCreatorServer() {
        if (server != null && server.isRunning()) {
            server.stopAndWait();
            server = null;
        }
        messageHistory = new ArrayList<>();
    }


    public Message buildMessage(ConfigurationSUT configuration, ConfigurationSUT configurationSUTCreator, TemplateHl7v2 templateHl7v2, HapiContext context) {
        Writer writer = new StringWriter();

        String idFormattedTimestamp = utils.getIdFormatted();
        String dateFormattedTimestamp = utils.getDateFormatted();
        String destination = "/opt/ans-hl7v2-simulators/templates/CDA/";
        File file = new File(destination + templateHl7v2.getFileName());
        MustacheFactory mustacheFactory = new DefaultMustacheFactory();

        MessageData messageData = MessageData.builder()
                .withApplicationSupplier(configurationSUTCreator.getApplication())
                .withFacilitySupplier(configurationSUTCreator.getFacility())
                .withApplicationReceiver(configuration.getApplication())
                .withFacilityReceiver(configuration.getFacility())
                .withDateMessage(dateFormattedTimestamp)
                .withIdMessage(idFormattedTimestamp)
                .withCharset(configuration.getCharset())
                .build();

        try {
            Mustache mustache = mustacheFactory.compile(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8), file.getName());

            mustache.execute(writer, messageData).flush();

            String result = writer.toString();

            return context.getPipeParser().parse(result);
        } catch (HL7Exception | IOException e) {
            throw new CustomHl7Exception("Send Message Error!", e);
        }
    }

    public void startCreatorServer(ConfigurationSUT configurationSUTCreator, HapiContext context) throws InterruptedException {
        if (server == null) {
            server = context.newServer(Integer.parseInt(configurationSUTCreator.getPort()), false);
            ReceivingApplication<Message> handler = new CustomReceiverClientApplication(this);

            server.registerApplication("*", "*", handler);
            server.startAndWait();
        }
    }

    public void sendAndReceive(Message cda, ConfigurationSUT configuration, HapiContext context) {
        try {
            Connection connection = context.newClient(configuration.getHost(), Integer.parseInt(configuration.getPort()), false);
            Initiator initiator = connection.getInitiator();
            Message response = initiator.sendAndReceive(cda);
            this.messageHistory.add(MessageData.messageToMessageData(cda));
            this.messageHistory.add(MessageData.messageToMessageData(response));
        } catch (HL7Exception | LLPException | IOException e) {
            throw new Hl7ConnexionException();
        }
    }

    public List<MessageData> getMessageHistory() {
        return messageHistory;
    }


}