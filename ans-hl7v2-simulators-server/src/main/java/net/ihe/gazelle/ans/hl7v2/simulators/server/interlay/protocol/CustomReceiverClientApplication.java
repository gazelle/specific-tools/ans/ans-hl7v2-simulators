package net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.protocol;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v26.segment.MSH;
import ca.uhn.hl7v2.protocol.ReceivingApplication;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.CustomHl7Exception;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.server.CreatorServer;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.utils.MessageUtils;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.model.MessageData;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.server.CreatorServerImpl;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.utils.MessageUtilsImpl;

import java.io.IOException;
import java.util.Map;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public class CustomReceiverClientApplication implements ReceivingApplication<Message> {

    CreatorServer<MessageData> creatorServer;
    MessageUtils<Message, MSH> utils = new MessageUtilsImpl();


    public CustomReceiverClientApplication(CreatorServer<MessageData> creatorServer) {
        this.creatorServer = creatorServer;
    }

    @Override
    public Message processMessage(Message message, Map metadata) throws HL7Exception {
        Message ack;
        try {
            ack = utils.generateAck(message);
        } catch (IOException e) {
            throw new CustomHl7Exception("Can't generate ACK!", e);
        }
        this.creatorServer.getMessageHistory().add(MessageData.messageToMessageData(message));
        this.creatorServer.getMessageHistory().add(MessageData.messageToMessageData(ack));
        return ack;
    }

    @Override
    public boolean canProcess(Message message) {
        return true;
    }
}