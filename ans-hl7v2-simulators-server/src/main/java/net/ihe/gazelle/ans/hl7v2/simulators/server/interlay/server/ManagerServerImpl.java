package net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.server;

import ca.uhn.hl7v2.app.HL7Service;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.protocol.ReceivingApplication;
import jakarta.enterprise.context.ApplicationScoped;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.server.CheckMessageService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.server.ManagerServer;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.ConfigurationService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.TemplateHl7v2Service;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.protocol.CustomReceiverServerApplication;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.utils.CustomHapiContext;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 03/07/2023
 */
@ApplicationScoped
public class ManagerServerImpl implements ManagerServer {

    private HL7Service serverUTF8;


    private HL7Service serverI885915;


    @Override
    public void startListenMessages(ConfigurationService configurationService, TemplateHl7v2Service templateHl7v2Service, CheckMessageService<Message> checkMessageService) {
        boolean useTls = false;
        try {

            CustomHapiContext context = new CustomHapiContext("UNICODE UTF-8");
            ConfigurationSUT configurationUTF8 = configurationService.getLocalUTF8ManagerConfiguration();
            serverUTF8 = context.newServer(Integer.parseInt(configurationUTF8.getPort()), useTls);
            ReceivingApplication<Message> handler = new CustomReceiverServerApplication(configurationUTF8, configurationService, templateHl7v2Service, checkMessageService, context);
            serverUTF8.registerApplication("*", "*", handler);
            serverUTF8.startAndWait();



            context = new CustomHapiContext("8859/15");
            ConfigurationSUT configurationI885915 = configurationService.getLocalI885915ManagerConfiguration();
            serverI885915 = context.newServer(Integer.parseInt(configurationI885915.getPort()), useTls);
            handler = new CustomReceiverServerApplication(configurationI885915, configurationService, templateHl7v2Service, checkMessageService, context);
            serverI885915.registerApplication("*", "*", handler);
            serverI885915.startAndWait();

        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void stopListenMessages() {
        if (serverUTF8.isRunning()) {
            serverUTF8.stopAndWait();
        }

        if (serverI885915.isRunning()) {
            serverI885915.stopAndWait();
        }
    }

    public void switchServerStateMessages(ConfigurationService configurationService, TemplateHl7v2Service templateHl7v2Service, CheckMessageService<Message> checkMessageService) {

        if (isServerRunning()) {
            stopListenMessages();
        } else {
            startListenMessages(configurationService, templateHl7v2Service, checkMessageService);
        }
    }

    @Override
    public boolean isServerRunning() {
        if (serverUTF8 == null  || serverI885915 == null) return false;
        else return serverUTF8.isRunning()  && serverI885915.isRunning();
    }
}