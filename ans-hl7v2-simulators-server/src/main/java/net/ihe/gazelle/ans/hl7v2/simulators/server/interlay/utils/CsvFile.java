package net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.utils;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.valueOf;

/**
 * @author Romuald DUBOURG
 * @company KEREVAL
 * @project wado-errors-manager
 * @date 06/09/2023
 */
public class CsvFile {
    public static final char SEPARATOR = ';';

    private File file;
    private List<String> lines;
    private List<String[]> data;

    private CsvFile() {
    }

    public File getFile() {
        return file;
    }

    public List<String> getLines() {
        return lines;
    }

    public List<String[]> getData() {
        return data;
    }

    public CsvFile(File file) {
        this.file = file;

        // Init
        init();
    }

    private void init() {
        lines = readFile(file);

        data = new ArrayList<>(lines.size());
        String sep = valueOf(SEPARATOR);
        for (String line : lines) {
            String[] oneData = line.split(sep);
            data.add(oneData);
        }
    }

    public List<String> readFile(File file) {
        List<String> result = new ArrayList<>();

        try (FileReader fr = new FileReader(file)) {
            try (BufferedReader br = new BufferedReader(fr)) {
                for (String line = br.readLine(); line != null; line = br.readLine()) {
                    result.add(line);
                }
                return result;
            }
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

}
