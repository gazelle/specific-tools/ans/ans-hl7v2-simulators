package net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.protocol;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.app.Connection;
import ca.uhn.hl7v2.app.Initiator;
import ca.uhn.hl7v2.llp.LLPException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v26.segment.MSH;
import ca.uhn.hl7v2.protocol.ReceivingApplication;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.CustomHl7Exception;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.exception.NotFoundException;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.server.CheckMessageService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.ConfigurationService;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.service.TemplateHl7v2Service;
import net.ihe.gazelle.ans.hl7v2.simulators.api.application.utils.MessageUtils;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.ConfigurationSUT;
import net.ihe.gazelle.ans.hl7v2.simulators.api.domain.model.TemplateHl7v2;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.exception.InvalideMessageException;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.model.MessageData;
import net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.utils.MessageUtilsImpl;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.*;


/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 15/06/2023
 */
public class CustomReceiverServerApplication implements ReceivingApplication<Message> {

    ConfigurationService configurationService;
    ConfigurationSUT configurationSUTManager;
    TemplateHl7v2Service templateHl7v2Service;

    HapiContext context;
    MessageUtils<Message, MSH> utils = new MessageUtilsImpl();


    CheckMessageService<Message> checkMessageService;

    public CustomReceiverServerApplication(ConfigurationSUT configurationSUManager, ConfigurationService configurationService, TemplateHl7v2Service templateHl7v2Service, CheckMessageService<Message> checkMessageService, HapiContext context) {
        this.configurationSUTManager = configurationSUManager;
        this.configurationService = configurationService;
        this.templateHl7v2Service = templateHl7v2Service;
        this.checkMessageService = checkMessageService;
        this.context = context;
    }

    public boolean canProcess(Message theIn) {
        return true;
    }

    @Override
    public Message processMessage(Message message, Map<String, Object> metadata) {
        try {
            if (!checkMessageService.checkMessage(message)) {
                throw new InvalideMessageException("Refuse by Validation Profile");
            }

            if (!utils.checkMSSante(message)) {
                throw new InvalideMessageException("Email domain should be .mssante.fr or interop-mssante.apicrypt.org for MSSante");
            }
            if (!utils.checkDMPDest(message)) {
                throw new InvalideMessageException("Message information's was not matching set of values DMP");
            }

            if (isMockErrorMessage(message)) throw new CustomHl7Exception();
            String messageIDOrigin = utils.getIdMessage(message);
            ConfigurationSUT configurationSUT = findSupplier(message);
            List<TemplateHl7v2> listMessages = templatesToReply(message);
            sendZAM(listMessages, configurationSUT, messageIDOrigin);
            return utils.generateAck(message);
        } catch (IOException | HL7Exception e) {
            throw new CustomHl7Exception("Process Message error: ", e);
        }
    }

    public void sendZAM(List<TemplateHl7v2> listTemplates, ConfigurationSUT configurationSUT, String messageIdOrigin) {


        TimerTask task = new TimerTask() {
            public void run() {

                for (TemplateHl7v2 templateHl7v2 : listTemplates) {
                    Connection connection = null;
                    try (Writer writer = new StringWriter()) {
                        connection = context.newClient(configurationSUT.getHost(), Integer.parseInt(configurationSUT.getPort()), false);

                        Initiator initiator = connection.getInitiator();

                        String idFormattedTimestamp = utils.getIdFormatted();
                        String dateFormattedTimestamp = utils.getDateFormatted();
                        MessageData messageData = MessageData.builder()
                                .withApplicationSupplier(configurationSUTManager.getApplication())
                                .withFacilitySupplier(configurationSUTManager.getFacility())
                                .withApplicationReceiver(configurationSUT.getApplication())
                                .withFacilityReceiver(configurationSUT.getFacility())
                                .withIdMessageOrigin(messageIdOrigin)
                                .withDateMessage(dateFormattedTimestamp)
                                .withIdMessage(idFormattedTimestamp)
                                .withCharset(configurationSUTManager.getCharset())
                                .build();


                        templateHl7v2.getTemplateMustache().execute(writer, messageData).flush();

                        String result = writer.toString();
                        Message zam = context.getPipeParser().parse(result);

                        initiator.sendAndReceive(zam);
                    } catch (HL7Exception | LLPException | IOException e) {
                        throw new CustomHl7Exception(e);
                    } finally {
                        assert connection != null;
                        if (connection.isOpen()) {
                            connection.close();
                        }
                    }

                }

            }
        };
        Timer timer = new Timer("Timer");
        long delay = 1000L;
        timer.schedule(task, delay);
    }

    public boolean isMockErrorMessage(Message message) {
        try {
            return utils.getMSH(message).getMessageControlID().encode().equals("000");
        } catch (HL7Exception e) {
            throw new CustomHl7Exception(e);
        }
    }

    public ConfigurationSUT findSupplier(Message message) {
        MSH msh = utils.getMSH(message);
        try {
            return configurationService.findSupplier(msh.getSendingApplication().encode(), msh.getSendingFacility().encode());
        } catch (HL7Exception e) {
            throw new NotFoundException();
        }
    }

    public List<TemplateHl7v2> templatesToReply(Message message) {

        List<TemplateHl7v2> templateHl7v2s = new ArrayList<>();
        try {


            if (utils.shouldSendZ01(message)) {
                templateHl7v2s.add(templateHl7v2Service.getZAMZ01RCVOK());
            }
            if (utils.shouldSendZ02(message)) {
                templateHl7v2s.add(templateHl7v2Service.getZAMZ02RCVOK());
            }
            if (utils.shouldSendZ03(message)) {
                if (utils.shouldSendError(message)) templateHl7v2s.add(templateHl7v2Service.getZAMZ03RCVKO());
                else templateHl7v2s.add(templateHl7v2Service.getZAMZ03LCTOK());
            }
        } catch (Exception e) {
            return templateHl7v2s;
        }
        return templateHl7v2s;
    }


}
