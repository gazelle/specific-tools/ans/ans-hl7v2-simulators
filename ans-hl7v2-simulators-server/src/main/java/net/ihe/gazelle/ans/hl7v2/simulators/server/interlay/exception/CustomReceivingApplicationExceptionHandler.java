package net.ihe.gazelle.ans.hl7v2.simulators.server.interlay.exception;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.protocol.ReceivingApplicationExceptionHandler;

import java.util.Map;

/**
 * @author Romuald DUBOURG, Claude LUSSEAU
 * @company KEREVAL
 * @project ans-hl7v2-simulators
 * @date 10/07/2023
 */
public class CustomReceivingApplicationExceptionHandler implements ReceivingApplicationExceptionHandler {


    @Override
    public String processException(String incomingMessage, Map<String, Object> incomingMetadata, String outgoingNegativeAcknowledgementMessage, Exception exception)
            throws HL7Exception {
        return outgoingNegativeAcknowledgementMessage;
    }

}